//
//  FFTControl.h
//  Science of Music
//
//  Created by Kevin Murphy on 6/7/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <Accelerate/Accelerate.h>
#include <math.h>
#import <Foundation/Foundation.h>

typedef enum {
    FFTControlMode_Linear,
    FFTControlMode_Logarithmic
} FFTControlMode;



@interface FFTControl : NSObject
{
    int len;
    FFTSetup fftSetup;
    vDSP_Length log2n;
    COMPLEX_SPLIT A;
}
@property (readwrite) int lengthFFT;
@property (nonatomic, readwrite) FFTControlMode scaleMode;

- (id) initWithLength: (int) length;
- (void) doFFTReal:(SInt16*) samples amplitude: (float*) amps phase: (float*) fase length: (int) numSamples;
@end
