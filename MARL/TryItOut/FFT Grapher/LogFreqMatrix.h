//
//  LogFreqMatrix.h
//  MARL
//
//  Created by Kevin Murphy on 10/6/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogFreqMatrix : NSObject
+ (float*) matrixWithHeight: (int) height sampleRate: (float) sampleRate lowMidi: (int) lowerMidi higherMidi: (int) upperMidi binsPerMidi: (int) binsPerMidi;
@end
