//
//  WaveTableCell.m
//  MARL
//
//  Created by Kevin Murphy on 9/10/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "WaveTableCell.h"

@implementation WaveTableCell
@synthesize waveView, frequency, amplitude, calibrationFrequency, dimmed;

- (id) initWithFrequency:(float) freq andAmplitude:(float)amp {
    self = [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WaveCell"];
    self.frequency = freq;
    self.amplitude = amp;
    self.waveView = [[SineWaveView alloc] initWithFrame:self.contentView.bounds andFrequeny:freq];
    waveView.frame = CGRectOffset(waveView.frame, 0, 10);
    waveView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.contentView addSubview:waveView];
    self.backgroundColor = [UIColor whiteColor];

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    //NSLog(@"selected");
    // Configure the view for the selected state
}

- (void) setDimmed:(BOOL)dim {
    if(!dimmed && dim) {
        //NSLog(@"DIMMED");
        if(dimmedView==nil) {
            dimmedView = [[UIView alloc] initWithFrame:self.contentView.bounds];
            dimmedView.backgroundColor = RGBA(0, 0, 0, 0.5);
            
            UILabel *frequencyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)];
            frequencyLabel.text = [NSString stringWithFormat:@"Frequency: %f", frequency];
            frequencyLabel.font = [UIFont systemFontOfSize:14];
            frequencyLabel.textColor = [UIColor whiteColor];
            frequencyLabel.backgroundColor = [UIColor clearColor];
            frequencyLabel.textAlignment = NSTextAlignmentCenter;
            
            UILabel *amplitudeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)];
            amplitudeLabel.text = [NSString stringWithFormat:@"Amplitude: %f", amplitude];
            amplitudeLabel.font = [UIFont systemFontOfSize:14];
            amplitudeLabel.textColor = [UIColor whiteColor];
            amplitudeLabel.backgroundColor = [UIColor clearColor];
            amplitudeLabel.textAlignment = NSTextAlignmentCenter;
            
            frequencyLabel.center = CGPointMake(self.contentView.bounds.size.width/2, self.contentView.bounds.size.height/2-16);
            amplitudeLabel.center = CGPointMake(self.contentView.bounds.size.width/2, self.contentView.bounds.size.height/2+16);
            
            [dimmedView addSubview:frequencyLabel];
            [dimmedView addSubview:amplitudeLabel];
            
        }
        [self.contentView addSubview:dimmedView];
        [UIView animateWithDuration:0.5 animations:^{dimmedView.alpha =1;} completion:^(BOOL finished) {
            dimmed = YES;
        }];
    } else {
        //NSLog(@"NOT DIMMED");
        [UIView animateWithDuration:0.5 animations:^{dimmedView.alpha = 0;} completion:^(BOOL finished) {
            dimmed = NO;
        }];
    }

}

- (void) setCalibrationFrequency:(float)calibration {
    calibrationFrequency = calibration;
    
    self.waveView.periodOfFrame = 1/calibrationFrequency;
}

- (NSString*) description {
    return [NSString stringWithFormat:@"Frequency: %f", frequency];
}

@end
