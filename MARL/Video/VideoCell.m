//
//  VideoCell.m
//  MARL
//
//  Created by Kevin Patrick Murphy on 7/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "VideoCell.h"
#import <AVFoundation/AVFoundation.h>

@implementation VideoCell
@synthesize thisCellsVideo, titleLabel, descriptionLabel, thumb, durationLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(132, 7, 450, 20)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:26];
        
        descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(135, 40, 470, 80)];
        descriptionLabel.numberOfLines = 0;
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.font =  [UIFont fontWithName:@"TrebuchetMS" size:13];
        descriptionLabel.textColor = RGBA(0.4, 0.4, 0.4, 0.7);
        
        durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(578, 110, 50, 20)];
        durationLabel.numberOfLines = 0;
        durationLabel.backgroundColor = [UIColor clearColor];
        durationLabel.font = [UIFont fontWithName:@"TrebuchetMS" size:13];
        
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:descriptionLabel];
        [self.contentView addSubview:durationLabel];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setThisCellsVideo:(Video *)thisCellsVideoo {
    thisCellsVideo = thisCellsVideoo;
    titleLabel.text = thisCellsVideo.title;
    [titleLabel sizeToFit];
    descriptionLabel.text = thisCellsVideo.deck;
    [descriptionLabel sizeToFit];
    if(thisCellsVideoo.path!=nil) {
        NSURL *testUrl = [[NSURL alloc] initFileURLWithPath:thisCellsVideoo.path];
        UIImageView *image = [[UIImageView alloc] initWithImage:[self thumbnailFromVideoAtURL:testUrl atTime:23.4]];
        image.frame = CGRectMake(10, 10, 80+30, 80+30);
        image.backgroundColor = [UIColor blackColor];
        image.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:image];
    } else {
        UIImageView *image = [[UIImageView alloc] initWithImage:thisCellsVideoo.thumbImage];
        image.frame = CGRectMake(10, 10, 80+30, 80+30);
        image.backgroundColor = [UIColor blackColor];
        image.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:image];
    }
    
    int seconds = thisCellsVideoo.duration%60;
    int mins = thisCellsVideoo.duration/60;
    NSString *stringOfTiem = [NSString stringWithFormat:@"%02d:%02d", mins, seconds];
    durationLabel.text = stringOfTiem;
}

- (UIImage *)thumbnailFromVideoAtURL:(NSURL *)contentURL atTime: (float) timeSec {
    UIImage *theImage = nil;
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:contentURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(timeSec*100, 100);
    CGImageRef imgRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
    
    theImage = [[UIImage alloc] initWithCGImage:imgRef];
    
    CGImageRelease(imgRef);
    
    return theImage;
}

@end
