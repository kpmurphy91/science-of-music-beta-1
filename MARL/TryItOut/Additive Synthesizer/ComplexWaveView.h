//
//  ComplexWaveView.h
//  MARL
//
//  Created by Kevin Murphy on 9/10/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComplexWaveView : UIView
{
    CGPathRef complexWavePath;
}
@property (nonatomic, readwrite) NSArray *arrayOfFrequencies;
@end
