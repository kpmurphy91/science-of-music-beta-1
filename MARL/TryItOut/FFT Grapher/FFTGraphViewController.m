//
//  FFTGraphViewController.m
//  MARL
//
//  Created by Kevin Murphy on 7/5/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "FFTGraphViewController.h"
#import "LogFreqMatrix.h"
#include <Accelerate/Accelerate.h>

@interface FFTGraphViewController ()

@end

@implementation FFTGraphViewController
@synthesize logFreqMatrix, mode;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        spectrumAnalyzer = [[FFTControl alloc] initWithLength:512];
        spectrumGraphView = [[Grapher alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 500)];
        spectrumGraphView = [[Grapher alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        spectrumGraphView.lineColor = RGBA(33, 45, 50, 1);
        spectrumGraphView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        spectrumGraphView.shouldFill = YES;
        
        [self.view addSubview:spectrumGraphView];
        
        windowSize = 1024;
        
        TPCircularBufferInit(&buffer, windowSize*4);
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    UIButton* myInfoButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [myInfoButton addTarget:self action:@selector(infoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:myInfoButton];
    
    [AudioController sharedAudioManager].delegate = self;
}


- (void) infoButtonClicked:(id) sender {
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.navigationController.view cache:NO];
    
    NSData *htmlData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"FFTHelp" ofType:@"html"]];
    HelpViewController *helpController = [HelpViewController helpControllerWithHTML:htmlData];
    
    [self.navigationController pushViewController:helpController animated:YES];
    [UIView commitAnimations];
}

- (void) viewDidAppear:(BOOL)animated {
    self.navigationController.toolbar.tintColor = RGBA(165, 159, 221, 1);    
    
    [[AudioController sharedAudioManager] startAudio];
}

- (void) viewWillDisappear:(BOOL)animated {
    self.navigationController.toolbarHidden = YES;
}

- (void) viewDidDisappear:(BOOL)animated {
    [[AudioController sharedAudioManager] stopAudio];
    self.navigationController.toolbarHidden = YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (YES);
}

- (void) dismiss {
    [self dismissViewControllerAnimated:YES
                             completion:^{}];
}



bool isPerforming = NO;
SInt16 *numbers;
float *amp, *phase;
float *updateAmp;
int updateLength;
int length = 0;
- (void) performReal {
    if(!isPerforming) {
        isPerforming = YES;
        [spectrumAnalyzer doFFTReal:numbers amplitude:amp phase:phase length:length];
        
        updateLength = length/2;
        updateAmp = (float*) malloc(sizeof(float)*updateLength);
        memcpy(updateAmp, amp, updateLength*sizeof(float));
        [self performSelectorOnMainThread:@selector(updateViewForFFT) withObject:nil waitUntilDone:YES];
        
        free(amp);
        free(phase);
        isPerforming = NO;
    }

}

- (void) updateViewForFFT {

    [spectrumGraphView setPoints:updateAmp withXorNil:nil numPuntos:updateLength/4];

}

- (void) receivedAudioSamples:(SInt16 *)samples length:(int)len {
    if(isPerforming) {
        //NSLog(@"busy...");
        return;
    }
    numbers = (SInt16*) malloc(len*sizeof(SInt16));
    memcpy(numbers, samples, len*sizeof(SInt16));
    length = len;
    amp = (float*) malloc(sizeof(float)*length);
    phase = (float*) malloc(sizeof(float)*length);
    
    [self performSelectorInBackground:@selector(performReal) withObject:nil];

}



@end
