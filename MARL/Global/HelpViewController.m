//
//  HelpViewController.m
//  MARL
//
//  Created by Kevin Murphy on 10/24/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

+ (HelpViewController*) helpControllerWithHTML:(NSData*) htmlData;
{
    HelpViewController *aController = [[HelpViewController alloc] initWithNibName:nil bundle:nil];
    [aController loadWebViewWithData:htmlData];
    return aController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated {
    
    self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height+2);
}

- (void) loadWebViewWithData:(NSData*) someNiceData {
    if(contentView != nil) {
        [contentView removeFromSuperview];
        contentView = nil;
    }
    
    
    NSString *htmlString = [[NSString alloc] initWithData:someNiceData encoding:NSASCIIStringEncoding];
    //htmlString = @"<body style='width:100%;background-color:#000;'><h1 style=\"color:#FFF;width:100%;text-align:center;\"><b>hey there</b></h1></body>";
    contentView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    contentView.scrollView.bounces = NO;
    [contentView loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    self.view = contentView;
    
}

- (void) viewWillDisappear:(BOOL)animated {
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
    
    
    //[self.navigationController popViewControllerAnimated:NO];
    [UIView commitAnimations];
    
    
    self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height-2);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
