//
//  ComplexWaveView.m
//  MARL
//
//  Created by Kevin Murphy on 9/10/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "ComplexWaveView.h"

@implementation ComplexWaveView
@synthesize arrayOfFrequencies;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        // Initialization code
    }
    return self;
}

- (void) setArrayOfFrequencies:(NSArray *)array {
    if(array.count==0) {
        NSLog(@"ARRAY WAS EMPTY");
        return;
    }
    
    NSArray *sortedArray;
    sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = a;
        NSNumber *second = b;
        return [first compare:second];
    }];

    arrayOfFrequencies = sortedArray;
    
    
    CGMutablePathRef mutablePath = CGPathCreateMutable();
    float H = self.frame.size.height;
    float W = self.frame.size.width;
    
    float largestPeriod = 1/[[sortedArray objectAtIndex:0] floatValue];
    float singlePixelScale = largestPeriod/W;
    
    float fundamental = [[sortedArray objectAtIndex:0] floatValue];
    
    CGPathMoveToPoint(mutablePath, nil, 0, H/2);
    
    for(int i = 0; i<W; i++) {
        float thisX = ((float) i);
        float thisY = H/2;
        for(int i = 0; i<arrayOfFrequencies.count; i++) {
            float thisFreq = [[arrayOfFrequencies objectAtIndex:i] floatValue];
            float thisScale = thisFreq/fundamental;
            
            thisY -= (H/(6+arrayOfFrequencies.count))*sinf(((2*M_PI)/W)*thisScale*thisX);
        }
        //NSLog(@"Drawing point (%f, %f)", thisX, thisY);
        CGPathAddLineToPoint(mutablePath, nil, thisX, thisY);
        
    }
    
    complexWavePath = CGPathCreateCopy(mutablePath);
    CGPathRelease(mutablePath);
    
    [self setNeedsDisplay];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextClearRect(UIGraphicsGetCurrentContext(), self.bounds);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    
    CGContextSetLineWidth(context, 2);
    CGContextAddPath(context, complexWavePath);
    CGContextDrawPath(context, kCGPathStroke);
    // Drawing code
}

- (NSString*) description {
    return [NSString stringWithFormat:@"%@", arrayOfFrequencies];
}

@end
