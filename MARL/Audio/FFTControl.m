//
//  FFTControl.m
//  Science of Music
//
//  Created by Kevin Murphy on 6/7/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "FFTControl.h"

@implementation FFTControl
@synthesize lengthFFT, scaleMode;


float freqFromMidi(float midi) {
    return 440.0*pow(2.0, ((double)midi-57)/12.0);
}


- (id) initWithLength: (int) length {
    len = length*2;
    log2n = log2f(length);
	fftSetup = vDSP_create_fftsetup(log2n, FFT_RADIX2);
    self.lengthFFT = length;
    return self;
}

- (void) doFFTReal:(SInt16*) samples amplitude: (float*) amps phase: (float*) fase length: (int) numSamples {
    log2n = log2f(numSamples);
	fftSetup = vDSP_create_fftsetup(log2n, FFT_RADIX2);

    
    float *hann = (float *) malloc(sizeof(float)*numSamples);
    float *fsamples = (float*) malloc(sizeof(float)*numSamples);

    for(int i = 0; i<numSamples; i++) {
        fsamples[i] = ((float)samples[i]);
    }
    
    vDSP_hann_window(hann, numSamples, vDSP_HANN_NORM);
    vDSP_vmul(fsamples, 1, hann, 1, fsamples, 1, numSamples);
    
	A.realp = (float *) malloc(numSamples/2*sizeof(float));
	A.imagp = (float *) malloc(numSamples/2*sizeof(float));

    //Convert float array of reals samples to COMPLEX_SPLIT array A
	vDSP_ctoz((COMPLEX*)fsamples,2,&A,1,numSamples/2);

    //Perform FFT using fftSetup and A
    //Results are returned in A
	vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_FORWARD);

    
    //Convert COMPLEX_SPLIT A result to float array to be returned
    amps[0] = A.realp[0]/(numSamples*2);
    fase[0] = atan2(A.imagp[0], A.realp[0]);
    
	for(int i =1;i<numSamples;i++) {
		amps[i]=(sqrt(A.realp[i]*A.realp[i]+A.imagp[i]*A.imagp[i])/numSamples);
        fase[i] = atan(A.imagp[i]/A.realp[i]);
    }
    
    
    free(fsamples);
    free(hann);
    free(A.imagp);
    free(A.realp);

    
    
}

@end
