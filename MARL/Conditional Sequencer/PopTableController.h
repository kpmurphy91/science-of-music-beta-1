//
//  PopTableController.h
//  MARL
//
//  Created by Kevin Murphy on 8/28/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SequencerSoundFont.h"

@protocol SoundFontChooserDelegate <NSObject>
- (void) soundFontChooserChoseSoundFont:(SequencerSoundFont*) soundFont;
@optional
- (void) soundFontChooserDismissedWithFont:(SequencerSoundFont*) soundFont;
@end

@interface PopTableController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *tabView;
    NSIndexPath *checkedIndexPath;
}
@property (nonatomic, readwrite) NSArray *arrayOfSoundFonts;
@property (nonatomic, readwrite) id<SoundFontChooserDelegate> delegate;
@property (nonatomic, readwrite) UITableView *tabView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andArray: (NSArray*) array andDefaultObject:(SequencerSoundFont*) defaul;

@end
