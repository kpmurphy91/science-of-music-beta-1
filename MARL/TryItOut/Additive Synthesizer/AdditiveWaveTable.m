//
//  AdditiveWaveTable.m
//  MARL
//
//  Created by Kevin Murphy on 9/9/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "AdditiveWaveTable.h"

@implementation AdditiveWaveTable
@synthesize values, length;
- (id) init {
    self = [super init];
    if(self) {
        self.length = 0;
        self.values = NULL;
        index = 0;
    }
    return self;
}

- (SInt16) getNextSample {
    if(values!=NULL) {
        index++;
        if(index>=self.length) index -= self.length;
        return self.values[index];
    }
    return 0;
}

- (void) regenerateWithArrayOfNumbers:(NSArray *)array {
    
    if(array.count==0){
        self.values = NULL;
        self.length = 0;
        return;
    }
    
    NSArray *sortedArray = array;
    sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = a;
        NSNumber *second = b;
        return [first compare:second];
    }];
    
    
    float fundamental = [[sortedArray objectAtIndex:0] floatValue];
    PADsynth *thisSynth = new PADsynth(8192, 44100, sortedArray.count);
    thisSynth->setharmonic(0, 1, 1);
    
    
    for (int i = 1; i<array.count; i++) {
        float thisWaveFreq = [[sortedArray objectAtIndex:i] floatValue];
        float thisWaveAmp = 1;
        thisSynth->setharmonic(i, thisWaveAmp, thisWaveFreq/fundamental);
    }
    
    float *samples = (float*) malloc(sizeof(float)*8192);
    thisSynth->synth(fundamental, 2, 1, samples);
    
    SInt16 *samplesInt = (SInt16*) malloc(sizeof(SInt16)*8192);
    
    for (int i = 0; i<8192; i++) {
        samplesInt[i] = samples[i]*SHRT_MAX;
    }
    
    self.values = samplesInt;
    self.length = 8192;

}


- (void) regenerateWithArrayOfWaves:(NSArray *)array {
   
    if(array.count==0){
        self.values = NULL;
        self.length = 0;
        return;
    }
    
    NSArray *sortedArray = array;
    sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = [NSNumber numberWithFloat:[a frequency]];
        NSNumber *second = [NSNumber numberWithFloat:[b frequency]];
        return [first compare:second];
    }];
    
    
    float fundamental = [[sortedArray objectAtIndex:0] frequency];
    PADsynth *thisSynth = new PADsynth(8192, 44100, sortedArray.count);
    thisSynth->setharmonic(0, 1, 1);


    for (int i = 1; i<array.count; i++) {
        float thisWaveFreq = [[sortedArray objectAtIndex:i] frequency];
        float thisWaveAmp = [[sortedArray objectAtIndex:i] amplitude];
        thisSynth->setharmonic(i, thisWaveAmp, thisWaveFreq/fundamental);
        
    }
    float *samples = (float*) malloc(sizeof(float)*8192);
    thisSynth->synth(fundamental, 2, 1, samples);
    
    
    SInt16 *samplesInt = (SInt16*) malloc(sizeof(SInt16)*8192);
    
    for (int i = 0; i<8192; i++) {
        samplesInt[i] = samples[i]*SHRT_MAX;
    }
    
    self.values = samplesInt;
    self.length = 8192;
}

@end
