//
//  Grapher.h
//  Science of Music
//
//  Created by Kevin Murphy on 6/7/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface Grapher : UIView
{
    CGMutablePathRef line;
    UIColor *lineColor;
}
@property (nonatomic) UIColor *lineColor;
@property (nonatomic) BOOL shouldFill;
- (void) setPoints: (float*) puntos withXorNil: (float*) xPuntos numPuntos: (int) numOfPoints;

@end
