//
//  Sequence.h
//  MARL
//
//  Created by Kevin Murphy on 9/11/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoundFontArray.h"

@protocol SequenceRepeatDelegate <NSObject>
- (void) sequenceReachedEndOfSequence;
- (void) sequencePlayedNote;
@end

@interface Sequence : NSObject <NSCoding>
{
    NSTimer *sequenceTimer;
    
}
@property (nonatomic, readwrite) NSString *title;
@property (nonatomic, readwrite) BOOL isplaying;
@property (nonatomic, readwrite) int currentColumn;
@property (nonatomic, readwrite) SequencerSoundFont *sound;
@property (nonatomic, readwrite) NSArray *sequenceArray, *frequencyArray;
@property (nonatomic, readwrite) int tempo;
@property (nonatomic, readwrite) id<SequenceRepeatDelegate> delegate;
- (void) play;
- (void) stop;
@end
