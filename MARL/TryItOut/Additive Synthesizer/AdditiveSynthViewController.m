//
//  AdditiveSynthViewController.m
//  MARL
//
//  Created by Kevin Murphy on 8/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "AdditiveSynthViewController.h"
#import "PADsynth.h"
#import "AudioController.h"

@interface AdditiveSynthViewController ()

@end

@implementation AdditiveSynthViewController
@synthesize arrayOfWaves, arrayOfCells, popNotePicker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        arrayOfWaves = [[NSMutableArray alloc] initWithCapacity:4];
        arrayOfCells = [[NSMutableArray alloc] initWithCapacity:4];
        
        
        addWaveCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"addAWaveCell"];
        addWaveCell.selectionStyle = UITableViewCellSelectionStyleNone;

        UIButton *addCell = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        addCell.frame = CGRectMake(303, 0, 80, 55);
        addCell.center = CGPointMake(640-65/2+19, 75/2);
        [addCell setTitle:@"Add Wave" forState:UIControlStateNormal];
        [addCell addTarget:self action:@selector(addWaveFromCell) forControlEvents:UIControlEventTouchUpInside];
        [addWaveCell.contentView addSubview:addCell];
        
        
        
        addFreqField = [[UITextField alloc] initWithFrame:CGRectMake(60, 15, 80, 30)];
        addFreqField.delegate = self;
        addFreqField.center = CGPointMake(180+300, 75/3-5);
        addFreqField.borderStyle = UITextBorderStyleRoundedRect;
        addFreqField.backgroundColor = [UIColor whiteColor];
        addFreqField.text = @"440.0";
        [addWaveCell.contentView addSubview:addFreqField];
        
        
        UIButton *noteButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        noteButton.frame = CGRectMake(addFreqField.frame.origin.x+addFreqField.frame.size.width+6, addFreqField.frame.origin.y, 30, 30);
        UIImage *imageForBut = [UIImage imageNamed:@"quarter.png"];
        
        [noteButton setImage:[self imageWithImage:imageForBut scaledToWidth:8] forState:UIControlStateNormal];
        [noteButton addTarget:self action:@selector(chooseNote:) forControlEvents:UIControlEventTouchUpInside];
        [addWaveCell.contentView addSubview:noteButton];
        
        frequencySlider = [[UISlider alloc] initWithFrame:CGRectMake(60, 15, 300, 30)];
        [frequencySlider addTarget:self action:@selector(sliderUpdate:) forControlEvents:UIControlEventValueChanged];
        frequencySlider.center = CGPointMake(270, 75/3-5);
        frequencySlider.minimumValue = 20.0;
        frequencySlider.maximumValue = 20000.0;
        frequencySlider.value = pow((440.0-20.0)/(20000-20),1.0/3.0)*(20000-20)+20;
        [addWaveCell.contentView addSubview:frequencySlider];
        
        
        UILabel *freqFeildLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 20)];
        freqFeildLabel.center = CGPointMake(80, 75/4);
        freqFeildLabel.text = @"Frequency:";
        freqFeildLabel.backgroundColor = [UIColor clearColor];
        [addWaveCell.contentView addSubview:freqFeildLabel];
        
        addAmpField = [[UITextField alloc] initWithFrame:CGRectMake(addFreqField.frame.origin.x, addFreqField.frame.origin.y+addFreqField.frame.size.height+25, addFreqField.frame.size.width, addFreqField.frame.size.height)];
        addAmpField.borderStyle = UITextBorderStyleRoundedRect;
        addAmpField.center = CGPointMake(180+300, 75*2/3+3);
        addAmpField.delegate = self;
        addAmpField.backgroundColor = [UIColor whiteColor];
        addAmpField.text = @"0.5";
        [addWaveCell.contentView addSubview:addAmpField];
        
        amplitudeSlider = [[UISlider alloc] initWithFrame:CGRectMake(60, 15, 300, 30)];
        [amplitudeSlider addTarget:self action:@selector(sliderUpdate:) forControlEvents:UIControlEventValueChanged];
        amplitudeSlider.center = CGPointMake(270, 75*2/3+3);
        amplitudeSlider.minimumValue = 0;
        amplitudeSlider.value = 0.5;
        amplitudeSlider.maximumValue = 1.0;
        [addWaveCell.contentView addSubview:amplitudeSlider];
        
        addCell.frame = CGRectMake(addCell.frame.origin.x+6, noteButton.frame.origin.y, addCell.frame.size.width, addAmpField.frame.origin.y+addAmpField.frame.size.height-3);
        
        UILabel *amplitudeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 20)];
        amplitudeLabel.center = CGPointMake(80, 75*2/3);
        amplitudeLabel.text = @"Amplitude:";
        amplitudeLabel.backgroundColor = [UIColor clearColor];
        [addWaveCell.contentView addSubview:amplitudeLabel];

        clearCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"clear"];
        clearCell.textLabel.text = @"Clear Waves";
        
    }
    return self;
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton* myInfoButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [myInfoButton addTarget:self action:@selector(infoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:myInfoButton];
    self.navigationItem.title = @"Additive Synthesizer";
    
    thisTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStyleGrouped];
    thisTable.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    thisTable.dataSource = self;
    thisTable.delegate = self;
    thisTable.backgroundView = nil;
    thisTable.editing = YES;
    thisTable.allowsSelectionDuringEditing = YES;
    [self.view addSubview:thisTable];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UIImageView *im = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"additiveGraph.png"]];
    im.frame = CGRectMake(0, 0, 300, 300);
    [self.view addSubview:im];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void) viewWillDisappear:(BOOL)animated {
    [self clearAllWaves];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (YES);
}




#pragma mark Create A Sinusoid
#pragma mark -

- (void) addWaveFromCell {
    if(arrayOfCells.count>9) return;
    
    [self.view endEditing:YES];
    
    float freq = addFreqField.text.floatValue;
    float amp = addAmpField.text.floatValue;

    [thisTable beginUpdates];

    
    WaveTableCell *aCell = [[WaveTableCell alloc] initWithFrequency:freq andAmplitude:amp];
    [arrayOfCells addObject:aCell];
    
    
    
    
    if(arrayOfCells.count==1) {
        [thisTable insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:2]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    
    NSIndexPath *newIndex = [NSIndexPath indexPathForRow:arrayOfCells.count-1 inSection:1];
    
    //search order rows
    NSArray *array = arrayOfCells;
    array = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = [NSNumber numberWithFloat:[(WaveTableCell*)a frequency]];
        NSNumber *second = [NSNumber numberWithFloat:[(WaveTableCell*)b frequency]];
        return [first compare:second];
    }];
    arrayOfCells = [NSMutableArray arrayWithArray:array];
    
    //NSLog(@"%@", array.description);
    
    NSInteger test = [arrayOfCells indexOfObject:aCell];
    //NSLog(@"NEW INDEX: %d - %@\n", test, newIndex);
    
    
    [thisTable insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndex] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [thisTable endUpdates];
    
    [thisTable reloadData];
    
    [self createWaveTable];
    
}


- (void) createWaveTable {
    AdditiveWaveTable *aTable = [[AdditiveWaveTable alloc] init];
    [aTable regenerateWithArrayOfWaves:arrayOfCells];
    
    [self updateComplexWave];
    [AudioController sharedAudioManager].synthWaveTable = aTable;
}

- (void) clearAllWaves {
    if(arrayOfCells.count == 0) return;
    
    NSMutableArray *rows = [[NSMutableArray alloc] initWithCapacity:4];
    for(int i = 0; i<arrayOfCells.count; i++) {
        [rows addObject:[NSIndexPath indexPathForRow:i inSection:1]];
    }
    [rows addObject:[NSIndexPath indexPathForRow:1 inSection:2]];
    
    [arrayOfCells removeAllObjects];
    
    [thisTable beginUpdates];
    [thisTable deleteRowsAtIndexPaths:rows withRowAnimation:UITableViewRowAnimationMiddle];
    [thisTable endUpdates];
    [thisTable reloadData];
    
    [self createWaveTable];

}

- (void) updateComplexWave {
    NSMutableArray *arrayOfFreqs = [[NSMutableArray alloc] initWithCapacity:arrayOfCells.count];
    for(int i =0; i<arrayOfCells.count;i++) {
        [arrayOfFreqs addObject:[NSNumber numberWithFloat:[[arrayOfCells objectAtIndex:i] frequency]]];
    }
    complexWave.arrayOfFrequencies = arrayOfFreqs;
}


#pragma mark UITableView Delegate
#pragma mark -

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"Clear chosen");

    if(indexPath.section == 2 && indexPath.row == 1) {
        [self clearAllWaves];

    }
    
    if([[tableView cellForRowAtIndexPath:indexPath] isKindOfClass:[WaveTableCell class]]) {
        [(WaveTableCell*)[tableView cellForRowAtIndexPath:indexPath] setDimmed: YES];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [arrayOfCells removeObjectAtIndex:indexPath.row];
        
        [thisTable beginUpdates];
        
        NSMutableArray *deleteArray = [[NSMutableArray alloc] initWithCapacity:1];
        if(arrayOfCells.count==0) {
            [deleteArray addObject:[NSIndexPath indexPathForRow:1 inSection:2]];
        }
        [deleteArray addObject:indexPath];
        [thisTable deleteRowsAtIndexPaths:deleteArray  withRowAnimation:UITableViewRowAnimationRight];
        
        [thisTable endUpdates];
        
        [thisTable reloadData];
        [self createWaveTable];

    }
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger numSections = 3;
    return numSections;
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section==0) {
        return 1;
    } else if(section==1) {
        return arrayOfCells.count;
    } else {
        if(arrayOfCells.count>=1)
            return 2;
        else 
            return 1;
    }
}


- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *thisCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"addWave"];
    
    
    if(indexPath.section==0) {
        complexWave = [[ComplexWaveView alloc] initWithFrame:CGRectMake(thisCell.contentView.frame.origin.x, thisCell.contentView.frame.origin.y+4, thisCell.contentView.bounds.size.width*2+38, thisCell.contentView.bounds.size.height+7)];
        UIImageView *axis = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Complex Wave Background.png"]];
        axis.frame = complexWave.frame;
        axis.contentMode = UIViewContentModeScaleToFill;
        
        [self updateComplexWave];
        [thisCell.contentView addSubview:axis];
        [thisCell.contentView addSubview:complexWave];
        thisCell.backgroundColor = [UIColor blackColor];
        //thisCell.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Complex Wave Background.png"]];
    } else if(indexPath.section ==2) {
        
        if(indexPath.row ==0) 
            return addWaveCell;
        else 
            return clearCell;
    } else {
        thisCell = [arrayOfCells objectAtIndex:indexPath.row];
    }
    
    //NSLog(@"row %d - %@", indexPath.row,NSStringFromCGRect(thisCell.contentView.frame));//thisCell.description);

    
    return thisCell;
}


- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section ==0) {
        return 60;
    }
    
    if(indexPath.section ==2 && indexPath.row == 0) {
        return 75;
    }
    return  65;
}


- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL returnbool = NO;
    
    if(indexPath.section ==1) {
        returnbool = YES;
    }
    
    return returnbool;
}




#pragma mark UITextView Delegate
#pragma mark -

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==addFreqField) {
        [addAmpField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self addWaveFromCell];
    }
    return NO;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [minimumTimer invalidate];
    minimumTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(checkMinimum:) userInfo:nil repeats:NO];
    
    if([string length]==0){
        return YES;
    }
    
    //Validate Character Entry
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789,."];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        
        if ([myCharSet characterIsMember:c]) {
            
            //now check if string already has 1 decimal mark
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            if([sep count]>2) return NO;
            
            if(textField==addFreqField && string.floatValue>20000) {
                addFreqField.text = @"20000.0";
                return NO;
            } else if(textField==addAmpField && string.floatValue>1) {
                addAmpField.text = @"1.0";
                return NO;
            }
            return YES;
            
        }
        
    }
    
    return NO;
}

- (void) checkMinimum:(NSTimer*) aTimer {
    if(addFreqField.text.floatValue<20) {
        addFreqField.text = @"20.0";
    } else if(addAmpField.text.floatValue<0) {
        addAmpField.text = @"0.0";
    }
}

- (BOOL)isNumeric:(NSString *)input {
    for (int i = 0; i < [input length]; i++) {
        char c = [input characterAtIndex:i];
        // Allow a leading '-' for negative integers
        if (!((c == '-' && i == 0) || (c >= '0' && c <= '9'))) {
            return NO;
        }
    }
    return YES;
}


#pragma mark Keyboard Notifications
#pragma mark -

- (void) keyboardWillHide: (NSNotification*)aNotification  {
    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    // NSLog(@"End Frame: %@", NSStringFromCGRect(keyboardEndFrame));
    // NSLog(@"Self Frame: %@", NSStringFromCGRect(self.navigationController.view.frame));
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect newFr;
    
    
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)) {
        newFr = CGRectMake(thisTable.frame.origin.x, 0, thisTable.frame.size.width, thisTable.frame.size.height+keyboardEndFrame.size.width);
        
    } else {
        newFr = CGRectMake(thisTable.frame.origin.x, 0, thisTable.frame.size.width, thisTable.frame.size.height+keyboardEndFrame.size.height);
        
    }
    thisTable.frame = newFr;
    
    
    [UIView commitAnimations];
    
    [thisTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}


- (void) keyboardWillShow: (NSNotification*)aNotification  {
    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    // NSLog(@"End Frame: %@", NSStringFromCGRect(keyboardEndFrame));
    // NSLog(@"Self Frame: %@", NSStringFromCGRect(self.navigationController.view.frame));
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect newFr;
    
    
    
    if(UIDeviceOrientationIsLandscape(self.interfaceOrientation)) {
        newFr = CGRectMake(thisTable.frame.origin.x, 0, thisTable.frame.size.width, thisTable.frame.size.height-keyboardEndFrame.size.width);
        
    } else {
        newFr = CGRectMake(thisTable.frame.origin.x, 0, thisTable.frame.size.width, thisTable.frame.size.height-keyboardEndFrame.size.height);
        
    }
    thisTable.frame = newFr;
    
    
    [UIView commitAnimations];
    
    [thisTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

- (void) infoButtonClicked:(id) sender {
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.navigationController.view cache:NO];
    
    NSData *htmlData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"AdditiveSynthHelp" ofType:@"html"]];
    
    HelpViewController *helpController = [HelpViewController helpControllerWithHTML:htmlData];
       
    [self.navigationController pushViewController:helpController animated:YES];
    [UIView commitAnimations];
}

- (void) sliderUpdate:(id) sender {
    if((UISlider*) sender == frequencySlider) {
        float delta = 20000-20;
        float prop = (frequencySlider.value-20)/delta;
        prop = prop*prop*prop;
        float newval = prop*delta+20;
        addFreqField.text = [NSString stringWithFormat:@"%3.1f", newval];
    } else if((UISlider*) sender == amplitudeSlider) {
        addAmpField.text = [NSString stringWithFormat:@"%4.2f",[(UISlider*) sender value]];
    }
}

- (void) chooseNote:(id) sender {
    NoteChooserViewController *notePicker = [[NoteChooserViewController alloc] initWithDelegate:self];
    popNotePicker = [[UIPopoverController alloc] initWithContentViewController:notePicker];
    UIView *sent = (UIView*) sender;
    [popNotePicker setPopoverContentSize:notePicker.view.frame.size];
    [popNotePicker presentPopoverFromRect:sent.frame inView:addWaveCell.contentView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void) notePickerCanceled {
    [popNotePicker dismissPopoverAnimated:YES];
}


- (void) notePickerChoseNote:(int)note {
    printf("NEW NOTE: %d\n", note);
    [popNotePicker dismissPopoverAnimated:YES];
    
    addFreqField.text = [NSString stringWithFormat:@"%3.1f", [self mToF:note]];
}


- (double) mToF: (int) MIDINote {
    if (MIDINote>0 && MIDINote<=119) {
        return 440.0*pow(2, ((double)MIDINote-57)/12);
    }
    return 0;
}
@end
