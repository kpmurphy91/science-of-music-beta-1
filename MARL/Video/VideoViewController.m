//
//  VideoViewController.m
//  MARL
//
//  Created by Kevin Patrick Murphy on 7/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "VideoViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface VideoViewController ()

@end

@implementation VideoViewController
@synthesize thisVideo;

+ (VideoViewController*) videoAndExplanationFromVideoObject:(Video *)videoObject {
    //set up a view for this Video Object and spit it back out!
    VideoViewController *thisVidView = [[VideoViewController alloc] init];
    thisVidView.thisVideo = videoObject;
    return thisVidView;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)playMovie
{
    [videoPlayer play];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 15, 600, 40)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:24];
    titleLabel.text = thisVideo.title;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:titleLabel];
    
    //set up the controller view container
    playerWindow = [[UIView alloc] initWithFrame:CGRectMake(50, 50, self.view.bounds.size.width-100, (self.view.bounds.size.width-100)*4/7 + 80)];
    playerWindow.layer.cornerRadius = 15;
    playerWindow.layer.masksToBounds = YES;
    playerWindow.layer.borderColor = RGBA(0x55, 0x55, 0x55, 1).CGColor;
    playerWindow.backgroundColor = RGBA(200, 200, 200, 0.3);
    playerWindow.layer.borderWidth = 2;
    
    
    //and then set that up if it's not youtube...
    if(![thisVideo isYoutube]) {
        videoPlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:thisVideo.path]];
        videoPlayer.view.frame = playerWindow.bounds;
        //and put 'em on the screen, show 'em to the world
        [playerWindow addSubview:videoPlayer.view];
    } else {
        NSString *embedHTML = @"\
        <html><head>\
        <style type=\"text/css\">\
        body {\
        background-color: black;\
        color: black;\
        }\
        </style>\
        </head><body style=\"margin:0\">\
        <div style=\"margin:12 auto;\">\
        <iframe style=\"display: block; margin: 0 auto;\" width=\"640\" height=\"360\" src=\"http://www.youtube.com/embed/%@?rel=0\" frameborder=\"0\" allowfullscreen></iframe>\
        </div></body></html>";
        NSString *html = [NSString stringWithFormat:embedHTML, thisVideo.youtubeID];
        UIWebView *videoView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, playerWindow.bounds.size.width, 390)];
        [videoView loadHTMLString:html baseURL:nil];
        videoView.backgroundColor = RGBA(0, 0, 0, 0.6);
        videoView.center = CGPointMake(playerWindow.bounds.size.width/2, videoView.bounds.size.height/2+1);
        videoView.scrollView.scrollEnabled = NO;
        [playerWindow addSubview:videoView];
    }
    
    shortDescription = [[UILabel alloc] initWithFrame:CGRectMake(10, 360+25, playerWindow.bounds.size.width-20, 60)];
    shortDescription.font = [UIFont fontWithName:@"TrebuchetMS" size:13];
    shortDescription.textAlignment = NSTextAlignmentLeft;
    shortDescription.text = thisVideo.deck;
    shortDescription.backgroundColor = [UIColor clearColor];
    [playerWindow addSubview:shortDescription];
    
    [self.view addSubview: playerWindow];

    //Set the title of the navigation controller accordingly
    self.navigationItem.title = thisVideo.title;
    
    
    //and add it to the screen
    UIWebView *infoView = [[UIWebView alloc] initWithFrame:CGRectMake(playerWindow.frame.origin.x, playerWindow.frame.origin.y+playerWindow.frame.size.height+40, 600, 370)];
    [infoView loadHTMLString:thisVideo.description baseURL:nil];    
    CGSize size = infoView.scrollView.contentSize;
    size.width = CGRectGetWidth(infoView.scrollView.frame);
    infoView.scrollView.contentSize = size;
    infoView.scrollView.alwaysBounceHorizontal = NO;
    infoView.scrollView.alwaysBounceVertical = NO;
    infoView.scrollView.bounces = NO;
    [self.view addSubview:infoView];
    
}

- (void) viewDidAppear:(BOOL)animated {
    [self playMovie];
}

- (void) viewWillDisappear:(BOOL)animated {
    [videoPlayer stop];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}





@end
