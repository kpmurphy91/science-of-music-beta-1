//
//  VideoViewController.m
//  MARL
//
//  Created by Kevin Patrick Murphy on 7/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "VideoPickViewController.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#include <netinet/in.h>
#import <CFNetwork/CFNetwork.h>
#import "VideoViewController.h"
#import "VideoCell.h"

@implementation VideoPickViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        listOfVideos = [NSMutableArray arrayWithCapacity:3];
    }
    
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Videos";
    
    [self retreiveVideos];
    
    tableOfVideos = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width-70, self.view.bounds.size.height-100) style:UITableViewStyleGrouped];
    tableOfVideos.backgroundColor = [UIColor clearColor];
    tableOfVideos.backgroundView = nil;
    tableOfVideos.delegate = self;
    tableOfVideos.dataSource = self;
    tableOfVideos.center = CGPointMake(self.view.bounds.size.width/2, tableOfVideos.center.y);
    tableOfVideos.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;

    [self.view addSubview:tableOfVideos];
    
    UIBarButtonItem *refresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(retreiveVideos)];
    self.navigationItem.rightBarButtonItem = refresh;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}



#pragma mark TableView Methods


- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoCell *cell = [[VideoCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Choice"];
    //cell.textLabel.textAlignment = UITextAlignmentCenter;
    
    Video *thisVideoInCell = [listOfVideos objectAtIndex:indexPath.row];
    cell.thisCellsVideo = thisVideoInCell;
    
    
    return  cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VideoViewController *newVideo = [VideoViewController videoAndExplanationFromVideoObject:[listOfVideos objectAtIndex:indexPath.row]];
     [self.navigationController pushViewController:newVideo animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [listOfVideos count];
}

- (int) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return  0;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  130;
}


- (void) infoButtonClicked:(id) sender {
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.navigationController.view cache:NO];
    
    UIViewController *helpController = [[UIViewController alloc] init];
    UIView *help = [[UIView alloc] initWithFrame:self.view.bounds];
    help.backgroundColor=[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1];
    helpController.view = help;
    
    [self.navigationController pushViewController:helpController animated:YES];
    [UIView commitAnimations];
}

- (void) retreiveVideos {
    [self throwActivityView];
    
    if([self connectedToNetwork]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSError* error = nil;
            NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://scienceofmusic.kevmdev.org/video-plist-api.php?api=1&auToken=cY8zir59dkl"] options:NSDataReadingUncached error:&error];
            if (error) {
                [self alertViewCancel:nil];
                return;
            } else {
                [self performSelectorOnMainThread:@selector(fetchedData:)
                                       withObject:data waitUntilDone:YES];
            }
            
        });
    } else {
        UIAlertView *aView = [[UIAlertView alloc] initWithTitle:@"Cannot connect to Youtube" message:@"It looks like your device isn't connected to the internet..." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [aView show];
    }
    
}

- (void) alertViewCancel:(UIAlertView *)alertView {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL) connectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!didRetrieveFlags)
    {
        printf("Error. Could not recover network reachability flags\n");
        return 0;
    }
    
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    return (isReachable && !needsConnection) ? YES : NO;
}

- (void) fetchedData:(NSData*)newData {
    NSError* error;
    plistModel = [NSJSONSerialization
                          JSONObjectWithData:newData //1
                          options:kNilOptions
                          error:&error];
    
    //NSArray* latestLoans = [json objectForKey:@"videoIds"]; //2
    [listOfVideos removeAllObjects];
    for(int i = 0; i<plistModel.count; i++) {
        NSDictionary *thisVideoDict = [plistModel objectAtIndex:i];
        Video *thisVideo = [Video videoObjectWithYoutubeID:[thisVideoDict objectForKey:@"youtubeID"] title:[thisVideoDict objectForKey:@"title"] andDescription:[thisVideoDict objectForKey:@"longDescription"]];
        thisVideo.deck = [thisVideoDict objectForKey:@"shortDescription"];
        thisVideo.duration = [[thisVideoDict objectForKey:@"runtime"] intValue];
        [listOfVideos addObject:thisVideo];
    }
    
    
    NSLog(@"%@", plistModel);
    [tableOfVideos reloadData];
    [self activityEnded];
}

- (void) throwActivityView {
    dispatch_async(dispatch_get_main_queue(), ^{
        activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 130, 130)];
        activityView.layer.cornerRadius = 20;
        activityView.backgroundColor = RGBA(0.8, 0.8, 0.8, 0.6);
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activity.center = CGPointMake(activityView.frame.size.width/2, activityView.frame.size.height/2);
        activityView.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
        [activity startAnimating];
        [activityView addSubview:activity];
        UILabel *loading = [[UILabel alloc] initWithFrame:CGRectMake(0, 95, 130, 30)];
        loading.text = @"Loading";
        loading.textAlignment = NSTextAlignmentCenter;
        loading.backgroundColor = [UIColor clearColor];
        loading.textColor = [UIColor whiteColor];
        loading.font = [UIFont boldSystemFontOfSize:15];
        [activityView addSubview:loading];
        [self.view addSubview:activityView];
    });
}

- (void) activityEnded {
    dispatch_async(dispatch_get_main_queue(), ^{
        [activityView removeFromSuperview];
    });
}

@end
