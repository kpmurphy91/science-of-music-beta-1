//
//  OverlayMessage.m
//  MARL
//
//  Created by Kevin Murphy on 10/21/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "OverlayMessage.h"
#import <QuartzCore/QuartzCore.h>

@implementation OverlayMessage

+ (OverlayMessage*) messageWithTitle:(NSString*) title andDuration:(float) duration {
    OverlayMessage *thisMessage = [[OverlayMessage alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    thisMessage->message.text = title;
    thisMessage->duration = duration;
    thisMessage.alpha = 0;
    if(UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        thisMessage.center = CGPointMake(1024.0/2, 768.0/2);
    } else {
        thisMessage.center = CGPointMake(768/2, 1024/2);
    }
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{thisMessage.alpha = 1;} completion:^(BOOL fin) {
        [UIView animateWithDuration:0.4 delay:duration options:UIViewAnimationCurveLinear animations:^{thisMessage.alpha=0;} completion:^(BOOL fin){[thisMessage removeFromSuperview];}];
    }];
    
    return thisMessage;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = RGBA(0, 0, 0, 0.6);
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = YES;
        
        
        message = [[UILabel alloc] initWithFrame:self.bounds];
        message.backgroundColor = [UIColor clearColor];
        message.textColor = [UIColor whiteColor];
        message.textAlignment = NSTextAlignmentCenter;
        [self addSubview:message];
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
