//
//  PatcherView.m
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "PatcherView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PatcherView
@synthesize blocksOnScreen, delegate, isPlaying, title, isInEditMode;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.title = @"Sequencer";
        
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"PatcherBackground.png"]];
        blocksOnScreen = [[NSMutableArray alloc] initWithCapacity:1];
        
        placeholder = [[UIView alloc] initWithFrame:CGRectZero];
        placeholder.backgroundColor = [UIColor clearColor];
        [self addSubview:placeholder];
        
        isInEditMode = NO;
    }
    return self;
}

#pragma mark -
#pragma mark NSCoding Protocol

- (id) initWithCoder:(NSCoder *)aDecoder {

    self = [self initWithFrame:[aDecoder decodeCGRectForKey:@"frame"]];
    if(self) {
        NSArray *blocks = [aDecoder decodeObjectForKey:@"blocksOnScreen"];
        self.blocksOnScreen = [blocks mutableCopy];
        self.title = [aDecoder decodeObjectForKey:@"title"];
    }
    
    for(int i = 0; i<blocksOnScreen.count; i++) {
        UIView *thisBlock = [blocksOnScreen objectAtIndex:i];
        if([thisBlock isKindOfClass:[PatcherConditionalBlock class]]) {
            PatcherConditionalBlock *thisCond = (PatcherConditionalBlock*) thisBlock;
            thisCond.delegate = self;
            [thisCond animateIn];
            
        } else {
            PatcherSequenceBlock *thisSequence = (PatcherSequenceBlock*) thisBlock;
            thisSequence.delegate = self;
            [thisSequence animateIn];
        }
    }
    
    for(int i = 0; i<blocksOnScreen.count; i++) {
        UIView *thisBlock = [blocksOnScreen objectAtIndex:i];
        if([thisBlock isKindOfClass:[PatcherConditionalBlock class]]) {
            [(PatcherConditionalBlock*) thisBlock resolvePortsAndConditionals];
        }
    }
    
    for(int i = 0; i<blocksOnScreen.count; i++) {
        [self addSubview:[blocksOnScreen objectAtIndex:i]];
    }
    
    
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeCGRect:self.frame forKey:@"frame"];
    [aCoder encodeObject:self.blocksOnScreen forKey:@"blocksOnScreen"];
    [aCoder encodeObject:self.title forKey:@"title"];
}


- (PatcherSequenceBlock*) sequenceForTagNumber:(int) num {
    for(int i = 0; i<blocksOnScreen.count; i++) {
        id thisObject = [blocksOnScreen objectAtIndex:i];
        if([thisObject isKindOfClass:[PatcherSequenceBlock class]]) {
            if([(PatcherSequenceBlock*)thisObject sequenceTag]==num) {
                return (PatcherSequenceBlock*) thisObject;
            }
        }
    }
    
    return nil;
}

- (PatcherConditionalBlock*) conditionalForTagNumber:(int) num {
    for(int i = 0; i<blocksOnScreen.count; i++) {
        id thisObject = [blocksOnScreen objectAtIndex:i];
        if([thisObject isKindOfClass:[PatcherConditionalBlock class]]) {
            if([(PatcherConditionalBlock*)thisObject conditionalTag]==num) {
                return (PatcherConditionalBlock*) thisObject;
            }
        }
    }
    
    return nil;
}

#pragma mark -
#pragma mark Block Life Cycle

- (void) createNewConditional: (PatcherConditionalBlock*) aBlock {
    //Adds the new conditional to the screen
    PatcherConditionalBlock *newConditional = aBlock;
    [self addSubview:newConditional];
    newConditional.delegate = self;
    [newConditional animateIn];
    [blocksOnScreen addObject:newConditional];
}

- (void) deleteConditional:(PatcherConditionalBlock *)aConditional
{
    //prepare to delete (remove cords, adjacent conditionals)
    [aConditional.topPort.connectedCord selfDestruct];
    [aConditional.bottomPort.connectedCord selfDestruct];
    
    aConditional.leftConditional.rightConditional = nil;
    aConditional.topConditional.bottomConditional = nil;
    aConditional.rightConditional.leftConditional = nil;
    aConditional.bottomConditional.topConditional = nil;
    
    //remove from array
    [blocksOnScreen removeObject:aConditional];
    [aConditional.layer removeAllAnimations];
    
    [UIView animateWithDuration:0.6 delay:0 options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState animations:^{aConditional.center = CGPointMake(950, 750);aConditional.alpha = 0.3; aConditional.transform = CGAffineTransformMakeScale(0.3, 0.3);} completion:^(BOOL finished){[aConditional removeFromSuperview];}];
    
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    UIView *deleteThis = [theAnimation valueForKey:@"DeleteThisView"];
    [deleteThis removeFromSuperview];
}

- (void) createNewSequence: (Sequence*) aseq {
    //The sequence builder created a sequence
    PatcherSequenceBlock *newSequence = [[PatcherSequenceBlock alloc] initWithCenter:CGPointMake(self.frame.size.width-90, 90)];
    newSequence.thisSequence = aseq;
    [self addSubview:newSequence];
    newSequence.delegate = self;
    [newSequence animateIn];
    [blocksOnScreen addObject:newSequence];
}

- (void) deleteSequence: (PatcherSequenceBlock*) aSequenceBlock
{
    
}

- (void) clearBlocks {
    
    if(self.isPlaying) {
        [self stopAllSequences];
        NSLog(@"is playing");
    }
    
    for(int i = 0; i<blocksOnScreen.count; i++) {
        UIView *temp = [blocksOnScreen objectAtIndex:i];
        [temp removeFromSuperview];
    }
    
    [blocksOnScreen removeAllObjects];
}



- (void) setTitle:(NSString*) titl {
    title = titl;
    [[delegate navigationItem] setTitle:title];
}

#pragma mark -
#pragma mark Patcher Port Protocol

- (void) patcherPortWasSelected:(PatcherPortView *)port {
    //Check to see if there is a Cord connected already, if so update the newPatcherCord iVar (see patcherPortISSelected:). Otherwise create a patcherCord.
    if(port.connectedCord==nil) {
        if([port.parent isKindOfClass:[PatcherConditionalBlock class]] && (port == [(PatcherConditionalBlock*)port.parent leftPort] || port == [(PatcherConditionalBlock*)port.parent rightPort])) {
            //FIXME: Do not draw cords from the left and right ports. To any future developers - I originally had 4 ports per block, regardless if it's a sequence or conditional. I eliminated the left and right ports for sequences, and I did the same to Conditional blocks except I didn't remove them from the block UIView because I use the frame to detect when adjacent blocks should snap together. (If you're reading this, I didn't have time to fix it)
        } else {
            newPatcherCord = [[PatcherCord alloc] initWithFrame:self.bounds fromPort:port];
            newPatcherCord.fromPort = port;
        }
    } else {
        newPatcherCord = port.connectedCord;
    }
    
    //FIXME:Delete if no problems with Patcher Cord: newPatcherCord.backgroundColor = [UIColor clearColor];
    [self insertSubview:newPatcherCord aboveSubview:placeholder];
}

- (void) patcherPortIsSelected:(PatcherPortView *)port andCordMovedToPoint:(CGPoint)newPoint {
    //TODO: Instead of a "newpatchercord" iVar, the newPatcherCord should be accessed via the port argument. Since the port argument has a cord connected to it, you can get the same pointer through port.connectedCord where port is connectedCord.fromPort

    
    //Update the patcher cords location
    [newPatcherCord moveToPoint:newPoint];

    //Check to see if the new location of the cord is inside of any port on screen. Then check to see if a connection should be made
    PatcherPortView *destPort = [self isPointInAPort:newPoint originatingFromPort:newPatcherCord.fromPort];

    PatcherConditionalBlock *thisConditional = (PatcherConditionalBlock*) destPort.parent;


    if(destPort != nil) {
        if(newPatcherCord.toPort!=nil && destPort!=newPatcherCord.toPort) {
            newPatcherCord.toPort.connectedCord = nil;
            newPatcherCord.toPort = nil;
        } else {
            if([thisConditional isKindOfClass:[PatcherConditionalBlock class]] && (thisConditional.leftConditional.topConditional!=nil || thisConditional.rightConditional.topConditional != nil)) {

            } else {
                newPatcherCord.toPort = destPort;
            }
        }
    } else {
        newPatcherCord.toPort.connectedCord = nil;
        newPatcherCord.toPort = nil;
    }
    
}

- (void) patcherPortWasDropped:(PatcherPortView *)port andCordIsAt:(CGPoint)newPoint {
    
    //Check to see if the cord was connected to anything. If its disconnected, drop it and reset the to and from ports, otherwise connect the ports via the dropped cord.
    if(newPatcherCord.toPort==nil) {
        [newPatcherCord removeFromSuperview];
        newPatcherCord.toPort.connectedCord = nil;
        newPatcherCord.fromPort.connectedCord = nil;
        newPatcherCord = nil;
        //NSLog(@"Dropped");
    } else {
        newPatcherCord.toPort.connectedCord = newPatcherCord;
        newPatcherCord.fromPort.connectedCord = newPatcherCord;
    }
    
    
    //Orient existing cord correctly (otherPoint is stationary point)
    CGPoint otherPoint;
    
    if(CGPointEqualToPoint(newPoint, newPatcherCord.pointA)) {
        otherPoint = newPatcherCord.pointB;
    } else if(CGPointEqualToPoint(newPoint, newPatcherCord.pointB)) {
        otherPoint = newPatcherCord.pointA;
    }
}

#pragma mark -
#pragma mark Conditional Block Protocol

- (void) conditional:(PatcherConditionalBlock*) patcherConditional WasDroppedAtPoint:(CGPoint)pointInFrame {

}

- (void) conditional:(PatcherConditionalBlock*) patcherConditional WasMovedToPoint:(CGPoint)pointInFrame {

    //Check to see if any ports are overlapping and other ports
    
    CGRect leftFrame = [self getFrameInSuperview:patcherConditional.leftPort];
    CGRect rightFrame = [self getFrameInSuperview:patcherConditional.rightPort];
    CGRect topFrame = [self getFrameInSuperview:patcherConditional.topPort];
    CGRect bottomFrame = [self getFrameInSuperview:patcherConditional.bottomPort];
    
    leftFrame = CGRectMake(leftFrame.origin.x-20, leftFrame.origin.y-20, leftFrame.size.width+40, leftFrame.size.height+40);
    rightFrame = CGRectMake(rightFrame.origin.x-20, rightFrame.origin.y-20, rightFrame.size.width+40, rightFrame.size.height+40);
    topFrame = CGRectMake(topFrame.origin.x-20, topFrame.origin.y-20, topFrame.size.width+40, topFrame.size.height+40);
    bottomFrame = CGRectMake(bottomFrame.origin.x-20, bottomFrame.origin.y-20, bottomFrame.size.width+40, bottomFrame.size.height+40);
    
    //Loop through all the blocks on screen and see if any ports are aligned
    for(int i = 0; i<self.blocksOnScreen.count; i++) {
        id thisObject = [self.blocksOnScreen objectAtIndex:i];
        if([thisObject isKindOfClass:[PatcherConditionalBlock class]]) {
            
            PatcherConditionalBlock *thisConditional = (PatcherConditionalBlock*) thisObject;
            
            /*  So I'll explain the next conditional pattern:
             
             if(the moving conditionals port is in the complementary stationary port) {
                if(there's already a left conditional OR (there's a right conditional AND it's not me) OR theres any configuration in which there will be 3 columns or rows of conditionals (restricted to 2 columns and rows max)) {
                } else {
                    Snap the conditional to where it needs to be and connect the two
                }
             } else {
                if(they're currently connected) {
                    Break the two conditionals apart
                }
             }
             */
            
            
            if(CGRectIntersectsRect(leftFrame, [self getFrameInSuperview:thisConditional.rightPort])) {
                //Moving conditional inside stationary port
                if(thisConditional.leftConditional != nil || thisConditional.topConditional.rightConditional != nil || thisConditional.topConditional.leftConditional != nil || thisConditional.bottomConditional.rightConditional != nil || thisConditional.bottomConditional.leftConditional != nil || (thisConditional.rightConditional != nil && thisConditional.rightConditional!=patcherConditional)) {
                    //This here is occupied, now move along, sir!
                    
                } else {
                    //Connect!
                    patcherConditional.center = CGPointMake(thisConditional.center.x+patcherConditional.frame.size.width, thisConditional.center.y);
                    thisConditional.rightConditional = patcherConditional;
                    patcherConditional.leftConditional = thisConditional;
                }
                                    
            } else {
                if(thisConditional.rightConditional == patcherConditional) {
                    //Reset connection
                    thisConditional.rightConditional = nil;
                    [patcherConditional setLeftConditional:nil];
                }
            }
                
            if(CGRectIntersectsRect(rightFrame, [self getFrameInSuperview:thisConditional.leftPort])) {
                
                if(thisConditional.rightConditional != nil || thisConditional.topConditional.leftConditional != nil || thisConditional.topConditional.rightConditional != nil || thisConditional.bottomConditional.leftConditional != nil || thisConditional.bottomConditional.rightConditional != nil || (thisConditional.leftConditional != nil && thisConditional.leftConditional != patcherConditional)) {
                    
                } else {
                    
                    thisConditional.leftConditional = patcherConditional;
                    patcherConditional.rightConditional = thisConditional;
                    patcherConditional.center = CGPointMake(thisConditional.center.x-patcherConditional.frame.size.width, thisConditional.center.y);
                }
                                
            } else {
                if(thisConditional.leftConditional == patcherConditional) {
                    thisConditional.leftConditional = nil;
                    [patcherConditional setRightConditional:nil];
                }
            }
            
            if(CGRectIntersectsRect(topFrame, [self getFrameInSuperview:thisConditional.bottomPort])) {
                if(thisConditional.topConditional != nil || thisConditional.leftConditional.bottomConditional!=nil || thisConditional.leftConditional.topConditional != nil || thisConditional.rightConditional.bottomConditional != nil || thisConditional.rightConditional.topConditional != nil || (thisConditional.bottomConditional != nil && thisConditional.bottomConditional != patcherConditional)) {
                    
                } else {
                    
                    thisConditional.bottomConditional = patcherConditional;
                    patcherConditional.topConditional = thisConditional;
                    patcherConditional.center = CGPointMake(thisConditional.center.x, thisConditional.center.y+thisConditional.frame.size.height);
                }
            } else {
                if(thisConditional.bottomConditional == patcherConditional) {
                    thisConditional.bottomConditional = nil;
                    [patcherConditional setTopConditional:nil];
                }
            }
            
            if(CGRectIntersectsRect(bottomFrame, [self getFrameInSuperview:thisConditional.topPort])) {
                if(thisConditional.bottomConditional != nil || thisConditional.leftConditional.topConditional != nil || thisConditional.leftConditional.bottomConditional != nil || thisConditional.rightConditional.topConditional != nil || thisConditional.rightConditional.bottomConditional != nil || (thisConditional.topConditional != nil && thisConditional.topConditional != patcherConditional)) {
                    
                } else {
                    
                    thisConditional.topConditional = patcherConditional;
                    patcherConditional.bottomConditional = thisConditional;
                    patcherConditional.center = CGPointMake(thisConditional.center.x, thisConditional.center.y- thisConditional.frame.size.height);
                }
            } else {
                if(thisConditional.topConditional == patcherConditional) {
                    thisConditional.topConditional = nil;
                    [patcherConditional setBottomConditional:nil];
                }
            }
        }
    }
}

- (void) conditional:(PatcherConditionalBlock*) patcherConditional WasSelectedAtPoint:(CGPoint)pointInFrame {
    if(self.isInEditMode) {
        //DELETE THIS CONDITIONAL
        [self deleteConditional:patcherConditional];
    }
}


#pragma mark -
#pragma mark Sequence Block Protocol
//TODO: Check other files and/or methods and protocol, and if fitting delete alltogether

- (void) sequenceWasTouched:(PatcherSequenceBlock*) aSequenceBlock {
    if(self.isInEditMode) {
        //DELETE THIS SEQUENCE
    }
}

- (void) sequenceWantsToEdit:(PatcherSequenceBlock *)aSequenceBlock {
    [delegate sequenceWantsToEdit:aSequenceBlock];
}

#pragma mark -
#pragma mark Block Moving Geometry Methods

- (PatcherPortView*) isPointInAPort:(CGPoint) point originatingFromPort:(PatcherPortView*) port {
    //This method will return a PatcherPortView if point is inside of any port frame on screen (except for the block that it's coming from, hence the "originating from Port" argument
    
    PatcherPortView *returnPort = nil;
    
    
    BOOL isASequence = NO;
    
    if([port.parent isKindOfClass:[PatcherConditionalBlock class]]) {
        isASequence = NO;
    } else if([port.parent isKindOfClass:[PatcherSequenceBlock class]]) {
        isASequence = YES;
    }

    for(int i = 0; i<[blocksOnScreen count]; i++) {
        
        id thisBlock = [blocksOnScreen objectAtIndex:i];
        
        BOOL otherIsASequence = NO;

        if([thisBlock isKindOfClass:[PatcherConditionalBlock class]]) {
            otherIsASequence = NO;
        } else if([thisBlock isKindOfClass:[PatcherSequenceBlock class]]) {
            otherIsASequence = YES;
        }

        if(!isASequence && otherIsASequence && thisBlock != port.parent) {
            PatcherSequenceBlock *thisClassedBlock = thisBlock;
            
            CGRect topFr = [self translateFrame:thisClassedBlock.topPort.frame intoParentFrame:thisClassedBlock.frame];
            CGRect btmFr = [self translateFrame:thisClassedBlock.bottomPort.frame intoParentFrame:thisClassedBlock.frame];
            
            topFr = CGRectMake(topFr.origin.x-20, topFr.origin.y-20, topFr.size.width+40, topFr.size.height+40);
            btmFr = CGRectMake(btmFr.origin.x-20, btmFr.origin.y-20, btmFr.size.width+40, btmFr.size.height+40);

            if(port.portLocation == KMDPatcherPort_BottomPort) {
                if(CGRectContainsPoint(topFr, point)) {
                    returnPort = thisClassedBlock.topPort;
                }
            }
            
            if(port.portLocation == KMDPatcherPort_TopPort) {
                if(CGRectContainsPoint(btmFr, point)) {
                    returnPort = thisClassedBlock.bottomPort;
                }
            }
        } else if(isASequence && !otherIsASequence && thisBlock != port.parent) {
            PatcherConditionalBlock *thisClassedBlock = thisBlock;
            
            CGRect topFr = [self translateFrame:thisClassedBlock.topPort.frame intoParentFrame:thisClassedBlock.frame];
            CGRect btmFr = [self translateFrame:thisClassedBlock.bottomPort.frame intoParentFrame:thisClassedBlock.frame];
            
            topFr = CGRectMake(topFr.origin.x-30, topFr.origin.y-30, topFr.size.width+60, topFr.size.height+60);
            btmFr = CGRectMake(btmFr.origin.x-30, btmFr.origin.y-30, btmFr.size.width+60, btmFr.size.height+60);

            if(port.portLocation == KMDPatcherPort_BottomPort && thisClassedBlock.topConditional == nil) {
                if(CGRectContainsPoint(topFr, point)) {
                    returnPort = thisClassedBlock.topPort;
                }
            }
            
            if(port.portLocation == KMDPatcherPort_LeftPort && thisClassedBlock.rightConditional == nil) {
                if(CGRectContainsPoint([self translateFrame:thisClassedBlock.rightPort.frame intoParentFrame:thisClassedBlock.frame], point)) {
                    returnPort = thisClassedBlock.rightPort;
                }
            }
            
            if(port.portLocation == KMDPatcherPort_RightPort && thisClassedBlock.leftConditional == nil) {
                if(CGRectContainsPoint([self translateFrame:thisClassedBlock.leftPort.frame intoParentFrame:thisClassedBlock.frame], point)) {
                    returnPort = thisClassedBlock.leftPort;
                }
            }
            
            if(port.portLocation == KMDPatcherPort_TopPort && thisClassedBlock.bottomConditional == nil) {
                if(CGRectContainsPoint(btmFr, point)) {
                    returnPort = thisClassedBlock.bottomPort;
                }
            }

        }
        
        if(thisBlock == port.parent) {
            
        } else {
            
                    }
    }
    return returnPort;
}



- (CGFloat) distanceFrom:(CGPoint) a to:(CGPoint) b {
    return sqrtf((b.x-a.x)*(b.x-a.x) + (b.y-a.y)*(b.y-a.y));
}

- (CGRect) translateFrame:(CGRect) childFrame intoParentFrame:(CGRect) parentFrame {
    CGRect result = CGRectMake(parentFrame.origin.x+childFrame.origin.x, parentFrame.origin.y+childFrame.origin.y, childFrame.size.width, childFrame.size.height);
    return result;
}

- (CGRect) getFrameInSuperview: (UIView*) childView {
    
    CGRect result = CGRectMake(childView.superview.frame.origin.x+childView.frame.origin.x, childView.superview.frame.origin.y+childView.frame.origin.y, childView.frame.size.width, childView.frame.size.height);
    return result;
}


#pragma mark -
#pragma mark Transport Control Methods

- (void) stop {
    for (int i = 0; i<blocksOnScreen.count; i++) {
        id thisObject = [blocksOnScreen objectAtIndex:i];
        if([thisObject isKindOfClass:[PatcherSequenceBlock class]]) {
            [thisObject stop];
        }
    }
}

- (void) stopAllSequences {
    [self stop];
}

- (BOOL) isPlaying {
    BOOL blah = NO;
    
    for (int i = 0; i<blocksOnScreen.count; i++) {
        id thisObject = [blocksOnScreen objectAtIndex:i];
        if([thisObject isKindOfClass:[PatcherSequenceBlock class]]) {
            PatcherSequenceBlock *thisSeq = thisObject;
            if(thisSeq.thisSequence.isplaying) {
                blah = YES;
            }
        }
    }
    
    return blah;
}

- (void) resetAllConditionals {
    for (int i = 0; i<blocksOnScreen.count; i++) {
        id thisObject = [blocksOnScreen objectAtIndex:i];
        if([thisObject isKindOfClass:[PatcherConditionalBlock class]]) {
            [thisObject reset];
        }
    }
}



- (void) setIsInEditMode:(BOOL)isInEdit {
    isInEditMode = isInEdit;
    
    if(isInEditMode) {
        for(int i = 0; i<blocksOnScreen.count; i++) {
            UIView *thisBlock = [blocksOnScreen objectAtIndex:i];

            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse | UIViewAnimationCurveLinear animations:^{thisBlock.transform = CGAffineTransformMakeRotation(69); thisBlock.transform = CGAffineTransformMakeRotation(-69);} completion:^(BOOL finished){}];
        }
    } else {
        for(int i = 0; i<blocksOnScreen.count; i++) {
            UIView *thisBlock = [blocksOnScreen objectAtIndex:i];
            [thisBlock.layer removeAllAnimations];
            thisBlock.transform = CGAffineTransformMakeRotation(0);
        }
    }
}



@end
