//
//  SineWaveView.m
//  MARL
//
//  Created by Kevin Murphy on 9/9/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "SineWaveView.h"

@implementation SineWaveView
@synthesize frequency, periodOfFrame;

- (id)initWithFrame:(CGRect)frame andFrequeny:(float) freq
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.frequency = freq;

        self.periodOfFrame = 1/150.0;
        // Initialization code
    }
    return self;
}


- (void) setPeriodOfFrame:(float)period {
    periodOfFrame = period;
    
    [self setNeedsDisplay];
}


- (void)drawRect:(CGRect)rect
{
    
    float periodOfMe = 1.0/self.frequency;
    int numberOfCycles = ceil(periodOfFrame/periodOfMe);
    printf("FREQUENCY: %f and periodOfFrame: %f numOfCycles: %d\n", self.frequency, self.periodOfFrame, numberOfCycles);
    float fullWidth = ((periodOfMe*numberOfCycles)/periodOfFrame)*self.frame.size.width;

    
    CGContextClearRect(UIGraphicsGetCurrentContext(), self.bounds);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    
    CGContextSetLineWidth(context, 2);
    CGMutablePathRef path = CGPathCreateMutable();
    
    float y = self.frame.size.height;
    
    float pixelsPerCycle = fullWidth/numberOfCycles;
    float beginPixel = 0;
    for(int i = 0; i<numberOfCycles; i++) {
        CGPathMoveToPoint(path, nil, beginPixel, y/2);
        CGPathAddQuadCurveToPoint(path, nil, beginPixel+pixelsPerCycle/4, 0, beginPixel+pixelsPerCycle/2, y/2);
        CGPathMoveToPoint(path, nil, beginPixel+pixelsPerCycle/2, y/2);
        CGPathAddQuadCurveToPoint(path, nil, beginPixel+pixelsPerCycle*0.75, y, beginPixel+pixelsPerCycle, y/2);
        CGContextAddPath(context, path);
        CGContextDrawPath(context, kCGPathStroke);
        beginPixel+=pixelsPerCycle;
    }
    
    
    CGPathRelease(path);

}


@end
