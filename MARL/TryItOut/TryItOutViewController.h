//
//  TryItOutViewController.h
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADSRViewController.h"
#import "FFTGraphViewController.h"
#import "AdditiveSynthViewController.h"

@interface TryItOutViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *tableViewOfChoices;
}
@end
