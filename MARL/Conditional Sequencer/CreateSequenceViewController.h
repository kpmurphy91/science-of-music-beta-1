//
//  CreateSequenceViewController.h
//  MARL
//
//  Created by Kevin Murphy on 9/13/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sequence.h"
#import "PopTableController.h"
#import "SequencerSoundFont.h"

@protocol CreateSequenceDelegate <NSObject>
- (void) popoverCreatedSequence: (Sequence*) aSequence;
@end

@interface CreateSequenceViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SoundFontChooserDelegate, UITextFieldDelegate>
{
    UITableView *thisTable;
    UITextField *tempoField, *titleField;
    int currentTempo;
    UILabel *soundChoice;
    SequencerSoundFont *chosenSoundFont;
    NSString *currentTitle;
}
@property (nonatomic, readwrite) BOOL editing;
@property (nonatomic, readwrite) id<CreateSequenceDelegate> delegate;
@property (nonatomic, readwrite) NSMutableArray *arrayOfSoundFonts;
@property (nonatomic, readwrite) int currentTempo;
- (id) initWithSequence: (Sequence*) aSequence;

@end
