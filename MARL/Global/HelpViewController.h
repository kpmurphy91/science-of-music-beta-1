//
//  HelpViewController.h
//  MARL
//
//  Created by Kevin Murphy on 10/24/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
{
    //UIScrollView *scrollContainer;
    UIWebView *contentView;
}
+ (HelpViewController*) helpControllerWithHTML:(NSData*) htmlData;
@end
