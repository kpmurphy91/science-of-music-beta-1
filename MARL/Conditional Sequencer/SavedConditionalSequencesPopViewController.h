//
//  SavedConditionalSequencesPopViewController.h
//  MARL
//
//  Created by Kevin Murphy on 9/17/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OpenSequencePopOver <NSObject>
- (void) choseSequence:(NSString*) fileName;
- (void) makeNewPatch;
@end

@interface SavedConditionalSequencesPopViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *choices;
    NSArray *paths;
}
@property (nonatomic, readwrite) id<OpenSequencePopOver> delegate;
@end
