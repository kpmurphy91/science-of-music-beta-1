//
//  SequenceTouchInterceptor.m
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "SequenceTouchInterceptor.h"

@implementation SequenceTouchInterceptor
@synthesize rows, columns, delegate, ooooThatTickles;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.rows = 8;
        self.columns = 12;
        self.ooooThatTickles = NO;
        
        currentCol = -1;
        currentRow = -1;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (float) getButtonHeight {
    return self.bounds.size.height/self.rows;
}

- (float) getButtonWidth {
    return self.bounds.size.width/self.columns;
}

- (int) whatColAreWeIn:(float) xCoord {
    float stepSize = self.bounds.size.width/self.columns;
    
    int colWeAreIn = xCoord/stepSize;
    return colWeAreIn;
}

- (int) whatRowAreWeIn:(float) yCoord {
    float stepSize = self.bounds.size.height/self.rows;
    
    //yCoord = self.bounds.size.height - yCoord;
    int rowWeAreIn = yCoord/stepSize;
    return rowWeAreIn;
}

- (void) generateTouchGridWithRows:(int) rowCount andColumns: (int) columnCount {
     //NSNumber NSArray? or object?
    self.rows = rowCount;
    self.columns = columnCount;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    self.ooooThatTickles = YES;
    
    UITouch *touch = [touches anyObject];
    CGPoint coord = [touch locationInView:self];
    int col = [self whatColAreWeIn:coord.x];
    int row  = [self whatRowAreWeIn:coord.y];
    //NSLog(@"%d, %d", col, row);

    currentRow = row;
    currentCol = col;
    
    [delegate startedInterceptingTouchInRow:row andColumn:col];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint coord = [touch locationInView:self];
    int col = [self whatColAreWeIn:coord.x];
    int row  = [self whatRowAreWeIn:coord.y];
    
    if((row != currentRow || col != currentCol) && (col>=0 && col<self.columns && row>=0 && row<self.rows)) {
        //NSLog(@"%d, %d", col, row);
        [delegate interceptedTouchMovedInRow:row andColumn:col];
        
        currentRow = row;
        currentCol = col;
    }
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    self.ooooThatTickles = NO;
    [delegate droppedTouch];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    self.ooooThatTickles = NO;
    [delegate droppedTouch];
}

@end
