//
//  EditSequencerView.m
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "EditSequencerView.h"
#import "AudioController.h"
#import "SoundFontArray.h"
#import "CreateSequenceViewController.h"

@implementation EditSequencerView
@synthesize columns, rows, soundPopoverController, delegate, thisSequence, editingBlock;
- (id)initWithFrame:(CGRect)frame andSequence:(Sequence*) thisSeq orBlock:(PatcherSequenceBlock *)block
{
    self = [super initWithFrame:frame];
    if (self) {
        isplaying = NO;
        sequenceButtonGridView = [[TouchGrid alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        [self addSubview:sequenceButtonGridView];
        
        self.thisSequence = thisSeq;
        if(self.thisSequence == nil) {
            self.thisSequence = block.thisSequence;
            self.editingBlock = block;
            NSLog(@"%@", thisSequence.sequenceArray);
            [sequenceButtonGridView reloadButtonsWithArray:thisSequence.sequenceArray];
        }
        
        [self.thisSequence.sound load];
        
        sequenceButtonGridView.highLightDecayInterval = self.thisSequence.sound.noteOnTimeout;
        columns = 16;
        rows = 12;
        
        NSLog(@"this tempo %d", thisSequence.tempo);
        // Initialization code
    }
    return self;
}



- (void) clear {
    [sequenceButtonGridView clearSelection];
    
}

- (void) play {
    if(!isplaying) {
        isplaying = YES;
        [self playNextColumn];
    } else {
        [self stop];
    }
}

- (void) stop {
    isplaying = NO;
}

int currentCol = 0;
- (void) playNextColumn {
    
    //NSLog(@"Play Column: %d", currentCol);
    int transpose = 5;
    
    [self glowColumn:currentCol];
    NSMutableArray *buttonsForThisCol = [sequenceButtonGridView.beatContentArray objectAtIndex:currentCol];
    for(int i = 0; i< self.rows; i++) {
        UIButton *thisButton = [buttonsForThisCol objectAtIndex:i];
        //NSLog(@"%@", thisButton);
        
        if(thisButton.selected) {
            int thisNoteNumber = [[sequenceButtonGridView.rowToFrequencyArray objectAtIndex:((self.rows-1)- i)] intValue] + transpose;
           // NSLog(@"Play note: %d - MIDI NUMBER: %d", i, thisNoteNumber);
            [[AudioController sharedAudioManager] playNote: thisNoteNumber];
            
        }
    }

    currentCol++;
    if(currentCol >= self.columns) {
        currentCol=0;
    }
    
    if(isplaying) {
        [self performSelector:@selector(playNextColumn) withObject:nil afterDelay:60.0f/thisSequence.tempo];
    }
}

- (void) glowColumn:(int) columnNumber {
    sequenceButtonGridView.highLightDecayInterval = self.thisSequence.sound.noteOnTimeout;
    [sequenceButtonGridView animateColumn:columnNumber];
    
}

- (void) removeFromSuperview {
    if([sequenceTimer isValid]) [sequenceTimer invalidate];
    [super removeFromSuperview];
}

- (Sequence*) closeSequence {
    [self stop];
    
    Sequence *newSequence = thisSequence;
    newSequence.sequenceArray = sequenceButtonGridView.beatContentArray;
    newSequence.frequencyArray = sequenceButtonGridView.rowToFrequencyArray;
    
    
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:5];
    
    for(int col = 0; col<sequenceButtonGridView.beatContentArray.count; col++) {
        NSMutableArray *buttonsForThisCol = [sequenceButtonGridView.beatContentArray objectAtIndex:col];
        NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:5];
        for(int i = 0; i< buttonsForThisCol.count; i++) {
            UIButton *thisButton = [buttonsForThisCol objectAtIndex:i];
            //NSLog(@"%@", thisButton);
            
            if(thisButton.highlighted) {
                [newArray addObject:[NSNumber numberWithBool:YES]];
                
            } else {
                [newArray addObject:[NSNumber numberWithBool:NO]];
            }
        }
        
        [arr addObject:newArray];

    }
        
    
    newSequence.sound = thisSequence.sound;
    newSequence.sequenceArray = arr;
    newSequence.tempo = thisSequence.tempo;
    newSequence.title = thisSequence.title;
    return newSequence;
}

- (void) popoverCreatedSequence:(Sequence *)aSequence {
    self.thisSequence.tempo = aSequence.tempo;
    self.thisSequence.sound = aSequence.sound;
    self.thisSequence.title = aSequence.title;
    
    UIViewController *thisController = (UIViewController*)self.delegate;
    
    
    
    thisController.navigationItem.title = thisSequence.title;
    
    if(soundPopoverController.isPopoverVisible) {
        [soundPopoverController dismissPopoverAnimated:YES];
    }
    sequenceButtonGridView.highLightDecayInterval = self.thisSequence.sound.noteOnTimeout;

}

- (void) presentEditor: (id) sender {
    if(soundPopoverController.isPopoverVisible) return;
    
    CreateSequenceViewController *present = [[CreateSequenceViewController alloc] initWithSequence:self.thisSequence];
    present.delegate = self;
    present.currentTempo = thisSequence.tempo;
    
    UINavigationController *navContol = [[UINavigationController alloc] initWithRootViewController:present];
    
    soundPopoverController = [[UIPopoverController alloc] initWithContentViewController:navContol];
    
    [soundPopoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (void) presentSoundSelection:(id)sender {
    PopTableController *present = [[PopTableController alloc] initWithNibName:nil bundle:nil andArray:[SoundFontArray globalSoundFontArray] andDefaultObject:thisSequence.sound];
    present.delegate = self;
    
    
    soundPopoverController = [[UIPopoverController alloc] initWithContentViewController:present];
    
    [soundPopoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    
}

- (void) soundFontChooserChoseSoundFont:(SequencerSoundFont *)soundFont {
    [soundFont load];
    thisSequence.sound = soundFont;
    [soundPopoverController dismissPopoverAnimated:YES];
    sequenceButtonGridView.highLightDecayInterval = self.thisSequence.sound.noteOnTimeout;
}

- (void) soundFontChooserDismissedWithFont:(SequencerSoundFont*) soundFont {
    thisSequence.sound = soundFont;
    
    [soundFont load];
    [soundPopoverController dismissPopoverAnimated:YES];
    sequenceButtonGridView.highLightDecayInterval = self.thisSequence.sound.noteOnTimeout;
}

@end

