//
//  NavigationControlForAddBlock.h
//  MARL
//
//  Created by Kevin Murphy on 10/8/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationControlForAddBlock : UINavigationController

@end
