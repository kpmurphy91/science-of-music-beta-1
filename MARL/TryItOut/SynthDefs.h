//
//  SynthDefs.h
//  Science of Music
//
//  Created by Kevin Murphy on 6/11/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

typedef enum {
    waveSine,
    waveTriangle,
    waveSawTooth,
    waveSquare
} WaveFormType;

typedef enum {
    synthEnvelopeStateStartDelay,
    synthEnvelopeStateAttack,
    synthEnvelopeStateDecay,
    synthEnvelopeStateSustain,
    synthEnvelopeStateRelease,
    synthEnvelopeOff
} synthEnvelopeState;

typedef enum {
    synthKeyStateTouchBegan,
    synthKeyStateTouchContinue,
    synthKeyStateTouchLifted,
    synthKeyStateNoTouch
} SynthKeyState;

struct EnvelopeValues
{
    CGPoint startDelay;
    CGPoint attack;
    CGPoint decay;
    CGPoint sustain;
    CGPoint release;
    CGFloat duration;
};

struct SynthNoteDescription {
    struct EnvelopeValues adsr;
    float frequency;
    //SynthKeyState keyState;
};




