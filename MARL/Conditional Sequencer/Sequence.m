//
//  Sequence.m
//  MARL
//
//  Created by Kevin Murphy on 9/11/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "Sequence.h"
#import "AudioController.h"

@implementation Sequence
@synthesize sequenceArray, frequencyArray, tempo, sound, isplaying, currentColumn, delegate, title;

- (id) init {
    self = [super init];
    if(self) {
        isplaying = NO;
    }
    
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [self init];
    
    if(self) {
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.sequenceArray = [aDecoder decodeObjectForKey:@"sequenceArray"];
        self.frequencyArray = [aDecoder decodeObjectForKey:@"frequencyArray"];
        self.tempo = [aDecoder decodeIntForKey:@"tempo"];
        
        
        NSString *soundTitle = [aDecoder decodeObjectForKey:@"soundFontTitle"];
        
        NSArray *sounds = [SoundFontArray globalSoundFontArray];
        
        for(int i = 0; i<sounds.count; i++) {
            SequencerSoundFont *thisOne = [sounds objectAtIndex:i];
            if([thisOne.title isEqualToString:soundTitle]) {
                self.sound = thisOne;
            }
        }
        
        if(sound == nil) {
            sound = [sounds objectAtIndex:0];
        }
    }
    
    
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:sequenceArray forKey:@"sequenceArray"];
    [aCoder encodeObject:frequencyArray forKey:@"frequencyArray"];
    [aCoder encodeObject:title forKey:@"title"];
    [aCoder encodeInt:tempo forKey:@"tempo"];
    [aCoder encodeObject:sound.title forKey:@"soundFontTitle"];
}

- (void) play {
    
    isplaying = YES;
    [self.sound load];
    [self playNextColumn];
    
}

- (void) stop {
    isplaying = NO;
}

- (void) playNextColumn {
    if(!isplaying) return;
    int transpose = 5;
    bool playedSomething = NO;
    
    NSMutableArray *buttonsForThisCol = [sequenceArray objectAtIndex:currentColumn];
    for(int i = 0; i< buttonsForThisCol.count; i++) {
        NSNumber *thisNumb = [buttonsForThisCol objectAtIndex:i];
        //NSLog(@"%@", thisButton);
        
        if(thisNumb.boolValue) {
            int thisNoteNumber = [[frequencyArray objectAtIndex:((buttonsForThisCol.count-1)- i)] intValue] + transpose;
            //NSLog(@"Play note: %d - MIDI NUMBER: %d", i, thisNoteNumber);
            [[AudioController sharedAudioManager] playNote: thisNoteNumber];
            playedSomething = YES;
        }
    }
   
    int stopColumn = currentColumn-1;
    if(stopColumn<0) stopColumn+=sequenceArray.count;
    //[self stopColumn:stopColumn];
    //NSLog(@"Play Column: %d", currentColumn);
    //NSLog(@"Stop Column: %d", stopColumn);

    currentColumn++;
    if(currentColumn >= sequenceArray.count) {
        currentColumn=0;
        [delegate sequenceReachedEndOfSequence];
    }
    
    if(playedSomething) {
        [self.delegate sequencePlayedNote];
    }
    
    //NSLog(@"Play Again Before");
    
    if(isplaying) {
        [self performSelector:@selector(playNextColumn) withObject:nil afterDelay:60.0f/tempo];
        //NSLog(@"play Again %d", tempo);
    }
    
}

- (void) stopColumn:(int) col {
    int transpose = 5;
    
    NSMutableArray *buttonsForThisCol = [sequenceArray objectAtIndex:col];
    for(int i = 0; i< buttonsForThisCol.count; i++) {
        NSNumber *thisNumb = [buttonsForThisCol objectAtIndex:i];
        //NSLog(@"%@", thisButton);
        
        if(thisNumb.boolValue) {
            int thisNoteNumber = [[frequencyArray objectAtIndex:((buttonsForThisCol.count-1)- i)] intValue] + transpose;
            //NSLog(@"Play note: %d - MIDI NUMBER: %d", i, thisNoteNumber);
            [[AudioController sharedAudioManager] stopNote: thisNoteNumber];
        }
    }
    
}


@end
