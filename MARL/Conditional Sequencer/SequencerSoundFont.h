//
//  SequencerSoundFont.h
//  MARL
//
//  Created by Kevin Murphy on 9/13/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioController.h"

@interface SequencerSoundFont : NSObject
@property (nonatomic, readwrite) NSTimeInterval noteOnTimeout;
@property (nonatomic, readwrite) NSString *sf2Name, *title;
@property (nonatomic, readwrite) NSURL *presetURL;
- (void) load;

@end
