//
//  Video.h
//  MARL
//
//  Created by Kevin Patrick Murphy on 7/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface Video : NSObject
{
    MPMoviePlayerController *videoPlayer;
    
}
@property (readwrite) NSString *title, *deck, *description, *path, *youtubeID;
@property (readwrite) NSURL *urlToVideo;
@property (readwrite) UIImage *thumbImage;
@property (readwrite) int duration;
+ (Video*) videoObjectWithFileName:(NSString*) inPath title:(NSString*) inTitle andDescription:(NSString*) inDescription;
+ (Video*) videoObjectWithYoutubeID:(NSString*) idd title:(NSString*) inTitle andDescription:(NSString*) inDescription;
- (void) finishThumbnail;
- (BOOL) isYoutube;
@end
