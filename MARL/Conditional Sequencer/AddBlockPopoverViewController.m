//
//  AddBlockPopoverViewController.m
//  MARL
//
//  Created by Kevin Murphy on 10/8/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "AddBlockPopoverViewController.h"

@interface AddBlockPopoverViewController ()

@end

@implementation AddBlockPopoverViewController
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [self forcePopoverSize];

}

- (void) viewDidAppear:(BOOL)animated {
    thisTable = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    thisTable.delegate = self;
    thisTable.dataSource = self;
    [self.view addSubview:thisTable];
    
    
    self.contentSizeForViewInPopover = CGSizeMake(300, 280);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Choice"];
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    
    if(indexPath.row == 0) {
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.text = @"Create New Sequence";
        
    } else {
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.text = @"Create New Conditional";
    }
    
    return  cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == 0) {
        
        CreateSequenceViewController *seqController = [[CreateSequenceViewController alloc] initWithNibName:nil bundle:nil];
        seqController.delegate = delegate;
        
        [self.navigationController pushViewController:seqController animated:YES];
    } else if(indexPath.row == 1) {
        CreateConditionalViewController *condController = [[CreateConditionalViewController alloc] initWithNibName:nil bundle:nil];
        condController.delegate = delegate;
        
        [self.navigationController pushViewController:condController animated:YES];
    }
    
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (int) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return  0;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  60;
}

- (void) forcePopoverSize {
    CGSize currentSetSizeForPopover = self.contentSizeForViewInPopover;
    CGSize fakeMomentarySize = CGSizeMake(currentSetSizeForPopover.width - 1.0f, currentSetSizeForPopover.height - 1.0f);
    self.contentSizeForViewInPopover = fakeMomentarySize;
}



@end
