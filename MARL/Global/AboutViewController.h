//
//  AboutViewController.h
//  MARL
//
//  Created by Kevin Murphy on 10/21/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
{
    UITapGestureRecognizer *gesture;
    UIButton *linkButton;
}
@end
