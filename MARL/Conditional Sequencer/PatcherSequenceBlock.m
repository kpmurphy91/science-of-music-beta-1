//
//  PatcherSequenceBlock.m
//  MARL
//
//  Created by Kevin Murphy on 8/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "PatcherSequenceBlock.h"
#import "PatcherConditionalBlock.h"

@implementation PatcherSequenceBlock
@synthesize delegate;
@synthesize topPort, bottomPort;
@synthesize thisSequence;
@synthesize isPlaying;
@synthesize repeats;
@synthesize sequenceTag;

static int tagSeqCount = 0;
- (id) initWithCenter: (CGPoint) centerPoint {
    
    CGSize blockSize = CGSizeMake(140, 140);
    CGRect frame = CGRectMake(centerPoint.x-blockSize.width/2, centerPoint.y-blockSize.height/2, blockSize.width, blockSize.height);
    
    self = [super initWithFrame:frame];
    if (self) {
        self.sequenceTag = tagSeqCount++;
        
        self.alpha = 0;
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"SequenceBlock.png"]];
        
        doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap)];
        doubleTap.numberOfTapsRequired = 2;
        doubleTap.numberOfTouchesRequired = 1;
        [self addGestureRecognizer: doubleTap];
        
        singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [singleTap requireGestureRecognizerToFail:doubleTap];
        [self addGestureRecognizer: singleTap];
        
        sequenceLabel = [[UILabel alloc] initWithFrame:self.bounds];
        sequenceLabel.backgroundColor = [UIColor clearColor];
        sequenceLabel.text = [NSString stringWithFormat:@"Sequence %d", tagSeqCount];
        sequenceLabel.textAlignment = UITextAlignmentCenter;
        sequenceLabel.font = [UIFont boldSystemFontOfSize:17];
        sequenceLabel.textColor = [UIColor whiteColor];
        [self addSubview:sequenceLabel];
    }
    
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    CGPoint cen = [aDecoder decodeCGPointForKey:@"centerPoint"];
    self = [self initWithCenter:cen];
    self.thisSequence = [aDecoder decodeObjectForKey:@"sequence"];
    self.sequenceTag = [aDecoder decodeIntegerForKey:@"tag"];
    
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInt:sequenceTag forKey:@"tag"];
    [aCoder encodeCGRect:self.frame forKey:@"frame"];
    [aCoder encodeCGPoint:self.center forKey:@"centerPoint"];
    [aCoder encodeObject:thisSequence forKey:@"sequence"];
    //Check for cords
    PatcherConditionalBlock *otherSide = (PatcherConditionalBlock*)[[topPort.connectedCord connectToPortFrom:topPort] parent];
    if(otherSide!=nil) {
        [aCoder encodeInt:otherSide.conditionalTag forKey:@"topPortTag"];
    } else {
        [aCoder encodeInt:-1 forKey:@"topPortTag"];
    }
    
    PatcherConditionalBlock *bottomSide = (PatcherConditionalBlock*)[[bottomPort.connectedCord connectToPortFrom:bottomPort] parent];
    if(bottomSide!=nil) {
        [aCoder encodeInt:bottomSide.conditionalTag forKey:@"bottomPortTag"];
    } else {
        [aCoder encodeInt:-1 forKey:@"bottomPortTag"];
    }
}


- (void) setDelegate:(id<SequenceBlockDelegate,PatcherPortDelegate>)delegatee {
    delegate = delegatee;
    topPort = [[PatcherPortView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    topPort.center = CGPointMake(self.frame.size.width/2, topPort.frame.size.height/2);
    topPort.backgroundColor = [UIColor clearColor];
    topPort.delegate = delegate;
    topPort.parent = self;
    topPort.portLocation = KMDPatcherPort_TopPort;
    [self addSubview:topPort];
    
    bottomPort = [[PatcherPortView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    bottomPort.center = CGPointMake(self.frame.size.width/2, self.frame.size.height - bottomPort.frame.size.height/2);
    bottomPort.backgroundColor = [UIColor clearColor];
    bottomPort.delegate = delegate;
    bottomPort.parent = self;
    bottomPort.portLocation = KMDPatcherPort_BottomPort;
    [self addSubview:bottomPort];
}

- (void) animateIn {
    self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{self.transform = CGAffineTransformMakeScale(1.0, 1.0);self.alpha = 1;} completion:^(BOOL finished){}];
}

- (void) setThisSequence:(Sequence *)sequence {
    thisSequence = sequence;
    
    if(![sequence.title isEqualToString:@""]) {
        sequenceLabel.text = sequence.title;
    }
    
    thisSequence.delegate = self;
}

#pragma mark UIView Touches
#pragma mark -


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *thisTouch = [touches anyObject];
    CGPoint thisPoint = [thisTouch locationInView:self.superview];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *thisTouch = [touches anyObject];
    CGPoint thisPoint = [thisTouch locationInView:self.superview];
    
    [topPort superviewIsMoving];
    [bottomPort superviewIsMoving];
    
    self.center = thisPoint;
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *thisTouch = [touches anyObject];
    CGPoint thisPoint = [thisTouch locationInView:self.superview];
    
    [topPort superviewIsMoving];
    [bottomPort superviewIsMoving];
    
    self.center = thisPoint;
}

#pragma mark Patcher Port Delegate
#pragma mark -

- (void) patcherPortWasSelected:(PatcherPortView *)port {
    
}

- (void) patcherPortIsSelected:(PatcherPortView *)port andCordMovedToPoint:(CGPoint)newPoint {
    
}

- (void) patcherPortWasDropped:(PatcherPortView *)port andCordIsAt:(CGPoint)newPoint {
    
}

- (void) patcherPortRecievedPulse:(PatcherPortView *)port {
    [self playSequence];
    //NSLog(@"Sequence Received Pulse");
}

#pragma mark Transport Control
#pragma mark -

- (void) stop {
    if(thisSequence.isplaying != NO) {
    UIImageView *tempPlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nothing.png"]];
    tempPlay.frame = self.bounds;
    tempPlay.contentMode = UIViewContentModeScaleAspectFit;
    tempPlay.transform = CGAffineTransformMakeScale(0.25, 0.25);
    tempPlay.backgroundColor = [UIColor blackColor];
    [self addSubview:tempPlay];
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationCurveEaseOut animations:^{
        tempPlay.transform = CGAffineTransformMakeScale(1.3, 1.3);
        tempPlay.alpha = 0;
    } completion:^(BOOL finished){[tempPlay removeFromSuperview];}];
    }
    
    self.isPlaying = NO;
    [self.thisSequence stop];
    
    
}

- (void) playSequence {
    [delegate stopAllSequences];
    
    repeats = 0;
    [self.thisSequence play];
    
    [self.bottomPort.connectedCord firstResponderPulse:self.bottomPort];
    PatcherPortView *por = (PatcherPortView*) [self.bottomPort.connectedCord connectToPortFrom:self.bottomPort];
    PatcherConditionalBlock *cond = (PatcherConditionalBlock*) por.parent;
    [cond reset];
    [cond becomeFirstResponder];
    
    NSLog(@"Play Sequence");
    UIImageView *tempPlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play.png"]];
    tempPlay.frame = self.bounds;
    tempPlay.contentMode = UIViewContentModeScaleAspectFit;
    tempPlay.transform = CGAffineTransformMakeScale(0.25, 0.25);
    tempPlay.backgroundColor = [UIColor clearColor];
    [self addSubview:tempPlay];
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationCurveEaseOut animations:^{
        tempPlay.transform = CGAffineTransformMakeScale(1.5, 1.5);
        tempPlay.alpha = 0;
    } completion:^(BOOL finished){[tempPlay removeFromSuperview];}];
}

#pragma mark Gesture Recognizer Control
#pragma mark -

- (void) singleTap {    
    if(self.thisSequence.isplaying) {
        [self stop];
    } else {
        [self playSequence];
    }
    
    
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [singleTap requireGestureRecognizerToFail:doubleTap];
    
    [self addGestureRecognizer: singleTap];
}

- (void) doubleTap {
    [delegate sequenceWantsToEdit:self];
    
    doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap)];
    doubleTap.numberOfTapsRequired = 2;
    doubleTap.numberOfTouchesRequired = 1;

    [self addGestureRecognizer: doubleTap];
}

#pragma mark Repeats
#pragma mark -

- (void) notifyConditionalsOfRepeat {
    if(self.bottomPort.connectedCord != nil) {
        PatcherPortView *otherSide = [self.bottomPort.connectedCord connectToPortFrom:self.bottomPort];
        PatcherConditionalBlock *otherBlock = (PatcherConditionalBlock*) otherSide.parent;
        [otherBlock iterateRepeat:self.repeats];
        
        [otherBlock.leftConditional iterateRepeat:self.repeats];
        //[otherBlock.leftConditional.bottomConditional iterateRepeat:self.repeats];
        [otherBlock.rightConditional iterateRepeat:self.repeats];
        //[otherBlock.rightConditional.bottomConditional iterateRepeat:self.repeats];
        //[otherBlock.bottomConditional iterateRepeat:self.repeats];
        
    }
}

- (void) sequenceReachedEndOfSequence {
    repeats++;
    
    PatcherConditionalBlock *otherSide = (PatcherConditionalBlock*)[[self.bottomPort.connectedCord connectToPortFrom:self.bottomPort]parent];
    [otherSide iterateRepeat:repeats];
    [otherSide.leftConditional iterateRepeat:repeats];
    [otherSide.rightConditional iterateRepeat:repeats];
    [otherSide.bottomConditional iterateRepeat:repeats];
}

- (void) sequencePlayedNote {
    [UIView animateWithDuration:0.02 delay:0 options:UIViewAnimationOptionCurveEaseIn |UIViewAnimationOptionAllowUserInteraction animations:^{
            self.transform = CGAffineTransformMakeScale(1.15, 1.15);
        } completion:^(BOOL finish){
        
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction animations:^{self.transform = CGAffineTransformMakeScale(1.0, 1.0);} completion:^(BOOL finish){}];
    }];
}

- (void) removeFromSuperview {
    [super removeFromSuperview];
    
    [self stop];
    
    [topPort.connectedCord removeFromSuperview];
    [bottomPort.connectedCord removeFromSuperview];
}

- (void) prepareNextConditionals {
    PatcherConditionalBlock *otherSide = (PatcherConditionalBlock*)[[self.bottomPort.connectedCord connectToPortFrom:self.bottomPort] parent];
    
    [otherSide reset];
    
}


@end


