//
//  PatcherCord.h
//  MARL
//
//  Created by Kevin Murphy on 8/24/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PatcherPortView;

@interface PatcherCord : UIView
{
    CGPoint temporaryPoint;
    CGMutablePathRef pathOfCord;
    
    UIBezierPath *thisPath;
    
    CGPoint pointA, pointB;
}
@property (nonatomic, readwrite) PatcherPortView *fromPort, *toPort, *movingPort;
@property (nonatomic, readwrite) CGPoint pointA, pointB, movingPoint;
-(id) initWithFrame:(CGRect) frame fromPort:(PatcherPortView*) from;
- (id) initWithFrame:(CGRect)frame fromPort:(PatcherPortView *)from toPort:(PatcherPortView*) to;
- (void) moveToPoint: (CGPoint) newPoint;
- (void) snapToPort: (PatcherPortView*) destinationPort;
- (void) portMoved: (PatcherPortView*) movingPort;
- (PatcherPortView*) connectToPortFrom:(PatcherPortView*) originatingFromPort;
- (void) pulseFrom: (PatcherPortView*) originatingPort;
- (void) firstResponderPulse: (PatcherPortView*) originatingPort;

- (void) selfDestruct;
@end
