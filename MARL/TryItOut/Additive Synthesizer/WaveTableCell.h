//
//  WaveTableCell.h
//  MARL
//
//  Created by Kevin Murphy on 9/10/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SineWaveView.h"


@interface WaveTableCell : UITableViewCell
{
    UIView *dimmedView;
    
}
@property (nonatomic, readwrite) SineWaveView *waveView;
@property (nonatomic, readwrite) float frequency, amplitude;
@property (nonatomic, readwrite) float calibrationFrequency;
@property (nonatomic, readwrite) BOOL dimmed;

- (id) initWithFrequency:(float) freq andAmplitude:(float) amp;

@end
