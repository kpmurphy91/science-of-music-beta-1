//
//  SequencerNavViewController.m
//  MARL
//
//  Created by Kevin Murphy on 9/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "SequencerNavViewController.h"

@interface SequencerNavViewController ()

@end

@implementation SequencerNavViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationBar.tintColor = RGBA(36, 10, 119, 1);
    self.toolbar.tintColor = RGBA(165, 159, 221, 1);
	// Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationBar.frame = CGRectMake(0, 20-1, self.navigationBar.frame.size.width, self.navigationBar.frame.size.height+1);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL) shouldAutorotate {
    return YES;
}

@end
