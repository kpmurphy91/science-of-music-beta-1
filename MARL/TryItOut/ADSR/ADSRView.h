//
//  ADSRView.h
//  Science of Music
//
//  Created by Kevin Murphy on 6/8/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Grapher.h"
#import "CirclePoint.h"



struct ADSRStruct
{
    CGPoint start;
    CGPoint A;
    CGPoint D;
    CGPoint S;
    CGPoint R;
    float duration;
};


@protocol ADSRViewDelegate <NSObject>
@required
- (void) newADSRStruct: (struct ADSRStruct) newADSRStruct;
@end

@interface ADSRView : UIView <CirclePointDelegate, UITextFieldDelegate>
{
    UIView *wrapper;
    UIView *axis, *options;
    Grapher *graph;
    
    CirclePoint *startCircle, *attackCircle, *decayCircle, *sustainCircle, *releaseCircle;
    
    UITouch *TEMPthisTouch;
    
    UILabel *durationLabel;

    UIView *sustainWindow;
    UIImageView *sustainImage;
}
@property (nonatomic, assign) id<ADSRViewDelegate> delegate;
@property (assign) struct ADSRStruct adsrValues;
@end
