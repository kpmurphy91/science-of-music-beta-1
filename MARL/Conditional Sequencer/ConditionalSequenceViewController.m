//
//  ConditionalSequenceViewController.m
//  MARL
//
//  Created by Kevin Murphy on 9/12/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "ConditionalSequenceViewController.h"
#import "SoundFontArray.h"
#import "NavigationControlForAddBlock.h"
#import "OverlayMessage.h"

@interface ConditionalSequenceViewController ()

@end

@implementation ConditionalSequenceViewController
@synthesize conditionlPopover, sequencePopover, savedPopover;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark -
#pragma mark Lifecycle
    
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton* myInfoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [myInfoButton addTarget:self action:@selector(infoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *infoBarButton = [[UIBarButtonItem alloc] initWithCustomView:myInfoButton];
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismiss)];

    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:cancel, nil];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:infoBarButton, nil];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 40)];
    titleLabel.text = @"Sequencer";
    [titleLabel setUserInteractionEnabled:YES];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:22];
    titleLabel.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(titleTapped:)];
    gesture.numberOfTapsRequired = 1;
    [titleLabel addGestureRecognizer:gesture];
    
    
    self.navigationItem.titleView = titleLabel;
    
    [self setToolbarForPatcher];
    
    flipContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];

    
    patcher = [[PatcherView alloc] initWithFrame:CGRectMake(0, 0, 1024,768)];
    patcher.delegate = self;
    
    [flipContainer addSubview:patcher];
    currentOnTop = patcher;
    
    [self.view addSubview:flipContainer];    
}

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setToolbarHidden:NO];
    
}

- (void) viewDidAppear:(BOOL)animated {
    
    flipContainer.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    patcher.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
   
    
    if([currentOnTop isKindOfClass:[EditSequencerView class]]) {
        [self setToolbarForSequenceBuilder];
    } else {
        [self setToolbarForPatcher];
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    [self.navigationController setToolbarHidden:YES];
}

- (void) viewDidUnload {
    [patcher stop];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Orientation Rotation Methods

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL) shouldAutorotate {
    return YES;
}

#pragma mark -
#pragma mark Navigation Bar and ToolBar actions

- (void) infoButtonClicked:(id) sender {
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.navigationController.view cache:NO];
    
    HelpViewController *helpController = [HelpViewController helpControllerWithHTML:nil];
    
    [self.navigationController pushViewController:helpController animated:YES];
    [UIView commitAnimations];
}



- (void) presentAddPopover:(id)sender {
    if(patcher.isInEditMode) return;

    AddBlockPopoverViewController *addController = [[AddBlockPopoverViewController alloc] initWithNibName:nil bundle:nil];
    addController.delegate = self;
    
    NavigationControlForAddBlock *navContol = [[NavigationControlForAddBlock alloc] initWithRootViewController:addController];
    addController.contentSizeForViewInPopover = CGSizeMake(300, 280);
    
    addController.title = @"Add Block";
    addControlPopover = [[UIPopoverController alloc] initWithContentViewController:navContol];
    [addControlPopover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (void) deleteBulk:(id)sender {
    if(patcher.isInEditMode) {
        deleteBarButton.tintColor = prevTintColor;
        patcher.isInEditMode = NO;
    } else if(patcher.blocksOnScreen.count>0) {
        prevTintColor = deleteBarButton.tintColor;
        deleteBarButton.tintColor = [UIColor redColor];
        patcher.isInEditMode = YES;
    }
}

#pragma mark -
#pragma mark Conditional Life Cycle

- (void) createNewConditional: (id) sender {
    if(patcher.isInEditMode) return;
    
    if(conditionalPopover.isPopoverVisible)return;
    
    if(sequencePopover.isPopoverVisible) {
        [sequencePopover dismissPopoverAnimated:YES];
    }
    
    if(savedPopover.isPopoverVisible) {
        [savedPopover dismissPopoverAnimated:YES];
    }
    
    CreateConditionalViewController *al = [[CreateConditionalViewController alloc] initWithNibName:nil bundle:nil];
    al.delegate = self;
    al.navigationItem.title = @"New Conditional";

    UINavigationController *navContol = [[UINavigationController alloc] initWithRootViewController:al];
    
    conditionalPopover = [[UIPopoverController alloc] initWithContentViewController:navContol];
    conditionalPopover.popoverContentSize = CGSizeMake(275, 450);
    [conditionalPopover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    
}


- (void) popoverCreatedConditional:(PatcherConditionalBlock *)aConditional {
    [addControlPopover dismissPopoverAnimated:YES];
    [patcher createNewConditional:aConditional];
}

#pragma mark -
#pragma mark Sequence Life Cycle

- (void) createNewSequence: (id) sender {
    if(patcher.isInEditMode) return;

    if(sequencePopover.isPopoverVisible)return;

    if(conditionalPopover.isPopoverVisible) {
        [conditionalPopover dismissPopoverAnimated:YES];
    }
    if(savedPopover.isPopoverVisible) {
        [savedPopover dismissPopoverAnimated:YES];
    }
    
    CreateSequenceViewController *al = [[CreateSequenceViewController alloc] initWithNibName:nil bundle:nil];
    al.navigationItem.title = @"New Sequence";
    al.delegate = self;
    UINavigationController *navContol = [[UINavigationController alloc] initWithRootViewController:al];
    
    sequencePopover = [[UIPopoverController alloc] initWithContentViewController:navContol];
    sequencePopover.popoverContentSize = CGSizeMake(275, 450);
    [sequencePopover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (void) saveSequenceMaker {
    if(newEditSequencerView.editingBlock != nil) {
        
        newEditSequencerView.editingBlock.thisSequence = [newEditSequencerView closeSequence];
        saveSequence = nil;
    } else {
        saveSequence = [newEditSequencerView closeSequence];
        
    }
   
    //NSLog(@"Saved Builder %@", newEditSequencerView);
    [self dismissSequenceMaker];
    
    titleLabel.text = patcher.title;

}

- (void) dismissSequenceMaker {
    [newEditSequencerView.soundPopoverController dismissPopoverAnimated:YES];
    
    [newEditSequencerView stop];
    
    
    UIView *toView = currentOnBottom, *fromView = currentOnTop;
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    completion:^(BOOL finished){
                        if(saveSequence!=nil)
                            [patcher createNewSequence:saveSequence];
                        saveSequence = nil;
                    }];
    
    
    currentOnTop = toView;
    currentOnBottom = fromView;
    
    [self setToolbarForPatcher];
    
    
}


- (void) popoverCreatedSequence:(Sequence *)aSequence {
    [addControlPopover dismissPopoverAnimated:YES];
    
    [self showSequenceMakerWithSequence:aSequence orBlock:nil];
}


- (void) showSequenceMakerWithSequence:(Sequence*) aSequence orBlock:(PatcherSequenceBlock*) block {
    
    if(aSequence!=nil) {
        
    } else if(block!=nil) {
        
    }
    
    currentOnBottom = [[EditSequencerView alloc] initWithFrame:flipContainer.bounds andSequence:aSequence orBlock:block];
    newEditSequencerView = (EditSequencerView*) currentOnBottom;
    
    UIView *toView = currentOnBottom, *fromView = currentOnTop;

    [UIView transitionFromView:fromView
                        toView:toView
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    completion:^(BOOL finished){
                        
                    }];
    
    currentOnTop = toView;
    currentOnBottom = fromView;
    
    titleLabel.text = aSequence.title;
    [self setToolbarForSequenceBuilder];
    
}


- (void) sequenceWantsToEdit:(PatcherSequenceBlock *)aSequenceBlock {
    if(patcher.isInEditMode) return;
    [self showSequenceMakerWithSequence:nil orBlock:aSequenceBlock];
}

#pragma mark -
#pragma mark Toolbar Setup Methods

- (void) setToolbarForSequenceBuilder {
    titleLabel.text = newEditSequencerView.thisSequence.title;

    
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissSequenceMaker)];
    
    UIBarButtonItem *use = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(saveSequenceMaker)];
    [use setTitle:@"Use"];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    UIBarButtonItem *play = [[UIBarButtonItem alloc] initWithTitle:@"Play" style:UIBarButtonItemStyleBordered target:self action:@selector(play)];
    
    UIBarButtonItem *clear = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:newEditSequencerView action:@selector(clear)];
    
    UIBarButtonItem *editSound = [[UIBarButtonItem alloc] initWithTitle:@"Edit Sound" style:UIBarButtonItemStyleBordered target:newEditSequencerView action:@selector(presentEditor:)];
    
    [self.navigationController.toolbar setItems:[NSArray arrayWithObjects: cancel, clear, editSound, flex, play, flex, use, nil]];
    [patcher stop];
}

- (void) setToolbarForPatcher {
    titleLabel.text = patcher.title;
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Flop.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(saveThisSequence)];
    
    UIBarButtonItem *open = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(openSavedPopOver:)];
    UIBarButtonItem *newCond = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(makeNewPatch)];
    newCond.style = UIBarButtonItemStyleBordered;
    open.style = UIBarButtonItemStyleBordered;
    
    deleteBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"trash.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(deleteBulk:)];
    UIBarButtonItem *addEither = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(presentAddPopover:)];
    
    addEither.style = UIBarButtonItemStyleBordered;
   // UIBarButtonItem *clearScreen = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:self action:@selector(clear)];
    
    [self setToolbarItems:[NSArray arrayWithObjects:open, save, newCond, flex, deleteBarButton, addEither, nil] animated:YES];
}


- (void) dismiss {
    if(currentOnTop == newEditSequencerView) {
        [self dismissSequenceMaker];
    } else {
        if(patcher.isInEditMode) return;
        [self dismissModalViewControllerAnimated:YES];
    }
}

- (void) play {
    if(patcher.isInEditMode) return;
    [newEditSequencerView play];
}


- (void) clear {
    [patcher clearBlocks];
}

#pragma mark - Patcher Life Cycle -
//FIXME: In my rush to finish this project, I made a big mistake in terminology. I'm admittedly still considering what terminology I should use, but I'll put this blurb here to explain. I have called two different concepts a "sequence"... In one sense of the term I reference a whole PatcherView that has different blocks on screen that are all arranged. Then I also refer to it as the singular sequence block that you can open and edit. There should be some differentiation between the two. At one point I started calling the PatcherView a Patcher but that sounds strange.

- (void) openSavedPopOver: (id) sender {
    if(patcher.isInEditMode) return;
    if(savedPopover.isPopoverVisible) return;
    
    SavedConditionalSequencesPopViewController *savedSequencePopControl = [[SavedConditionalSequencesPopViewController alloc] initWithNibName:nil bundle:nil];
    savedSequencePopControl.delegate = self;
    savedPopover = [[UIPopoverController alloc] initWithContentViewController:savedSequencePopControl];
    
    [savedPopover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    
}

- (void) choseSequence:(NSString*) fileName {
    //CHECK TO SAVE
    //TODO: Implement some type of change tracker so that when a change is made to the patcher, the program knows to ask if you'd like to save it or not.
    choseFile = fileName;
    [savedPopover dismissPopoverAnimated:YES];
    
    UIAlertView *saveAlert = [[UIAlertView alloc] initWithTitle:@"Save Sequence" message:@"Would you like to save the current sequence before proceding?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Don't Save", @"Save", nil];
    saveAlert.tag = 2;
    [saveAlert.layer setValue:fileName forKey:@"chosenFileName"];
    [saveAlert show];
}

- (void) openSaved: (NSString*) nam {
    NSString *path = [@"~/Documents/Sequences/" stringByExpandingTildeInPath];
    path = [path stringByAppendingPathComponent:nam];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [patcher removeFromSuperview];
        [patcher stop];
        
        patcher = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        patcher.delegate = self;
        
        [flipContainer addSubview:patcher];
        currentOnTop = patcher;
        
        self.navigationItem.title = patcher.title;
    } else {

        UIAlertView *thisAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not find sequence in file system" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [thisAlert show];
    }
}

- (void) saveThisSequence {
    if([patcher.title isEqualToString:@""]) {
        [self presentTitleAlertSave:YES];
        
    } else {
        [self finishSaving];
    }
}

- (void) finishSaving {
    if(![[NSFileManager defaultManager] fileExistsAtPath:[@"~/Documents/Sequences" stringByExpandingTildeInPath]]) {
        NSError *anError = [[NSError alloc] init];
        [[NSFileManager defaultManager] createDirectoryAtPath:[@"~/Documents/Sequences/" stringByExpandingTildeInPath] withIntermediateDirectories:NO attributes:nil error:&anError];
    }
    
    NSString *pathToFile = [[@"~/Documents/Sequences" stringByExpandingTildeInPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.seq", patcher.title]];
    
    BOOL suceed = [NSKeyedArchiver archiveRootObject:patcher toFile:pathToFile];
    
    if(suceed) {
        NSLog(@"Saved %@", pathToFile);
        
        OverlayMessage *overlay = [OverlayMessage messageWithTitle:@"File Saved" andDuration:1.5];
        [self.navigationController.view addSubview:overlay];
        
        
    } else {
        NSLog(@"Failed to save");
    }
}

- (void) makeNewPatch {
    //check to save!!
    if(patcher.isInEditMode) return;
    
    [patcher stop];
    [savedPopover dismissPopoverAnimated:YES];

    
    UIAlertView *saveAlert = [[UIAlertView alloc] initWithTitle:@"Save Sequence" message:@"Would you like to save the current sequence before proceding?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Don't Save", @"Save", nil];
    saveAlert.tag = 1;
    [saveAlert show];
}

#pragma mark - UIAlertView Protocol -

- (void) presentTitleAlertSave:(BOOL) save {
    [patcher stop];
    UIAlertView *thisAlert = [[UIAlertView alloc] initWithTitle:@"Enter A Title" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Enter", nil];
    thisAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [thisAlert show];
    firstResp = [thisAlert textFieldAtIndex:0];
    firstResp.delegate = self;
    firstResp.placeholder = titleLabel.text;
    firstRespAlertView = thisAlert;
    
    if(save) {
        thisAlert.tag = 5;
    }
    
}

- (void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if([alertView.title isEqualToString:@"Enter A Title"]) {
        if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"]) {
            NSLog(@"Cancel");
            return;
        }

        if(currentOnTop==newEditSequencerView) {
            newEditSequencerView.thisSequence.title = [alertView textFieldAtIndex:0].text;
        } else {
            patcher.title = [alertView textFieldAtIndex:0].text;
        }
        
        titleLabel.text = [alertView textFieldAtIndex:0].text;
        
        
        
        if(alertView.tag == 5)[self finishSaving];
    } else if([alertView.title isEqualToString:@"Save Sequence"]) {
        if(buttonIndex==0) {
            
        } else if(buttonIndex == 1) {
            NSLog(@"DON'T SAVE");
            //[self saveThisSequence];
            if(alertView.tag == 1) {
                [patcher clearBlocks];
            } else if(alertView.tag == 2) {
                [patcher clearBlocks];
                [self openSaved:choseFile];
            }
        } else if(buttonIndex == 2) {
            NSLog(@"SAVED");
            [self saveThisSequence];
            if(alertView.tag == 1) {
                [patcher clearBlocks];
                [self presentTitleAlertSave:NO];
            } else if(alertView.tag == 2) {
                [patcher clearBlocks];
                [self openSaved:choseFile];
            }
        }
    }
}

- (void) titleTapped:(id) sender {
    if(patcher.isInEditMode) return;
    [self presentTitleAlertSave:YES];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [firstResp resignFirstResponder];
    
    [firstRespAlertView dismissWithClickedButtonIndex:1 animated:YES];
    
    return YES;
}

@end
