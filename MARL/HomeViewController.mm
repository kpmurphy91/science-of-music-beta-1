//
//  HomeViewController.m
//  MARL
//
//  Created by Kevin Murphy on 6/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "HomeViewController.h"
#import "VideoPickViewController.h"
#import "SequencerNavViewController.h"
#import "AboutViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *blah = [[UIImageView alloc] initWithFrame:self.view.bounds];
    blah.contentMode = UIViewContentModeScaleToFill;
    blah.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    blah.image = [UIImage imageNamed:@"HomeBackground.png"];
    [self.view addSubview:blah];
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"HomeBackground.png"]];
    
    backGround = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background.png"]];
    backGround.frame = CGRectMake(self.view.bounds.size.width-backGround.frame.size.width, 10, 1024, 66);
    backGround.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    [self.view addSubview:backGround];
    
    choicesTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 600, 400) style:UITableViewStyleGrouped];
    choicesTable.center = CGPointMake(400, self.view.frame.size.height/2);
    choicesTable.backgroundView = nil;
    choicesTable.backgroundColor = [UIColor clearColor];
    choicesTable.delegate = self;
    choicesTable.dataSource = self;
    choicesTable.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    
    //[self.view addSubview:choicesTable];
    
    
     
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    
}

- (void) viewDidAppear:(BOOL)animated {
    UIImage *stretchedImage = [[UIImage imageNamed:@"HomeScreenButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:13];
    
    
    presentTryItOut = [[UIButton alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width*0.9,210)];
    presentTryItOut.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2-230);
    [presentTryItOut setBackgroundImage:stretchedImage forState:UIControlStateNormal];
    [presentTryItOut setTitle:@"Try it out!" forState:UIControlStateNormal];
    [presentTryItOut addTarget:self action:@selector(presentTryItOut) forControlEvents:UIControlEventTouchUpInside];
    presentTryItOut.titleLabel.font = [UIFont boldSystemFontOfSize:28];
    [self.view addSubview:presentTryItOut];
    
    
    presentMakeMusic = [[UIButton alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width*0.9,210)];
    presentMakeMusic.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    //[presentMakeMusic setImage:[UIImage imageNamed:@"SequencerHome.png"] forState:UIControlStateNormal];
    [presentMakeMusic setBackgroundImage:stretchedImage forState:UIControlStateNormal];
    [presentMakeMusic setTitle:@"Conditional Sequencer" forState:UIControlStateNormal];
    [presentMakeMusic addTarget:self action:@selector(presentMakeMusicViewController) forControlEvents:UIControlEventTouchUpInside];
    presentMakeMusic.titleLabel.font = [UIFont boldSystemFontOfSize:28];
    [self.view addSubview:presentMakeMusic];

    
    videos = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width*0.9, 210)];
    videos.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2+230);
    //[videos setImage:[UIImage imageNamed:@"Videos.png"] forState:UIControlStateNormal];
    videos.titleLabel.font = [UIFont boldSystemFontOfSize:28];
    [videos setBackgroundImage:stretchedImage forState:UIControlStateNormal];
    [videos setTitle:@"Videos" forState:UIControlStateNormal];
    [videos addTarget:self action:@selector(presentVideoPicker) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:videos];
    
    
    presentAbout = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width*0.9, 60)];
    presentAbout.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height-80);
    [presentAbout setTitle:@"About" forState:UIControlStateNormal];
    [presentAbout setBackgroundImage:stretchedImage forState:UIControlStateNormal];
    [presentAbout addTarget:self action:@selector(presentAbout) forControlEvents:UIControlEventTouchUpInside];
    presentAbout.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [self.view addSubview:presentAbout];
    
    
    UIButton *presentFeedback = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width*0.25, 40)];
    presentFeedback.center = CGPointMake((self.view.bounds.size.width*1)/6+7, self.view.bounds.size.height-24);
    [presentFeedback setTitle:@"Submit Feedback" forState:UIControlStateNormal];
    [presentFeedback setBackgroundImage:stretchedImage forState:UIControlStateNormal];
    [presentFeedback addTarget:self action:@selector(launchFeedback) forControlEvents:UIControlEventTouchUpInside];
    presentFeedback.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [self.view addSubview:presentFeedback];
    
    
    if(UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        [self willRotateToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation] duration:0.3];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

#pragma mark -
#pragma mark Orientation Rotation Methods

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationMaskPortrait;
}

- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL) shouldAutorotate {
    return NO;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        [UIView animateWithDuration:duration delay:0 options:nil animations:^{
            //*presentTryItOut, *presentMakeMusic, *videos
            videos.center = CGPointMake(768/2, 980/2+270);
            presentMakeMusic.center = CGPointMake(768/2, 980/2);
            presentTryItOut.center = CGPointMake(768/2, 980/2-270);

        } completion:^(BOOL fin){}];
    } else {
        [UIView animateWithDuration:duration delay:0 options:nil animations:^{
            //*presentTryItOut, *presentMakeMusic, *videos
            videos.center = CGPointMake(1024/4-30, 740/2);
            presentMakeMusic.center = CGPointMake(1024/2, 740/2);
            presentTryItOut.center = CGPointMake(3*1024/4+30, 740/2);
            
        } completion:^(BOOL fin){}];
    }
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void) newADSRStruct:(struct ADSRStruct)newADSRStruct {
    
}

- (void) presentTryItOut {
    TryItOutViewController *presentThis = [[TryItOutViewController alloc] initWithNibName:nil bundle:nil];
    presentThis.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self.navigationController pushViewController:presentThis animated:YES];
}

- (void) presentMakeMusicViewController {
    ConditionalSequenceViewController *presentThis = [[ConditionalSequenceViewController alloc] initWithNibName:nil bundle:nil];
    SequencerNavViewController *presentNave = [[SequencerNavViewController alloc] initWithRootViewController:presentThis];
    presentThis.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.navigationController presentModalViewController:presentNave animated:YES];
}

- (void) presentVideoPicker {
    VideoPickViewController *presentThis = [[VideoPickViewController alloc] init];
    presentThis.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.navigationController pushViewController:presentThis animated:YES];
}

- (void) presentAbout {
    AboutViewController *thisThing = [[AboutViewController alloc] initWithNibName:nil bundle:nil];
    //thisThing.view.frame = CGRectMake(0, 0, 320, 480);
    //thisThing.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    UINavigationController *aboutNavBar = [[UINavigationController alloc] initWithRootViewController:thisThing];
    aboutNavBar.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentModalViewController:aboutNavBar animated:YES];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Choice"];
    cell.textLabel.textAlignment = UITextAlignmentCenter;
    
    if(indexPath.row == 0) {
        cell.textLabel.text = @"Try it out";
    } else if(indexPath.row == 1) {
        cell.textLabel.text = @"Conditional Sequencer";
    } else if(indexPath.row ==2) {
        cell.textLabel.text = @"Videos";
    }
    
    return  cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([indexPath row]==0)
        [self presentTryItOut];
    if([indexPath row] ==1) 
        [self presentMakeMusicViewController];
    if([indexPath row] == 2)
        [self presentVideoPicker];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (int) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return  0;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  100;
}

- (void)launchFeedback {
    [TestFlight openFeedbackView];
}


@end
