//
//  KMDKeyBoard.m
//  BishaiTextBook
//
//  Created by Kevin Murphy on 6/18/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "KMDKeyBoard.h"
#import <QuartzCore/QuartzCore.h>

@implementation KMDKeyBoard
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        float widthWhite = self.frame.size.width/7.0;
        float blackWidth = widthWhite*0.67;
        
        self.backgroundColor = [UIColor blueColor];
        c = [UIButton buttonWithType:UIButtonTypeCustom];
        c.frame = CGRectMake(0, 0, widthWhite, self.frame.size.height);
        [c setBackgroundImage:[[UIImage imageNamed:@"whiteKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview: c];
        c.backgroundColor = [UIColor whiteColor];
        c.tag = 0;
        
        c.layer.borderColor = [UIColor blackColor].CGColor;
        c.layer.borderWidth = 3.0f;

        
        d = [UIButton buttonWithType:UIButtonTypeCustom];
        d.frame = CGRectMake(c.frame.origin.x+c.frame.size.width, 0, widthWhite, self.frame.size.height);
        [d setBackgroundImage:[[UIImage imageNamed:@"whiteKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview: d];
        d.backgroundColor = [UIColor whiteColor];
        d.tag = 2;
        [d addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        
        d.layer.borderColor = [UIColor blackColor].CGColor;
        d.layer.borderWidth = 3.0f;
        
        
        e = [UIButton buttonWithType:UIButtonTypeCustom];
        e.frame = CGRectMake(d.frame.origin.x+d.frame.size.width, 0, widthWhite, self.frame.size.height);
        [e setBackgroundImage:[[UIImage imageNamed:@"whiteKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview: e];
        e.backgroundColor = [UIColor whiteColor];
        e.tag = 4;
        [e addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        e.layer.borderColor = [UIColor blackColor].CGColor;
        e.layer.borderWidth = 3.0f;
        
        f = [UIButton buttonWithType:UIButtonTypeCustom];
        f.frame = CGRectMake(e.frame.origin.x+e.frame.size.width, 0, widthWhite, self.frame.size.height);
        [f setBackgroundImage:[[UIImage imageNamed:@"whiteKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview: f];
        f.backgroundColor = [UIColor whiteColor];
        f.tag = 5;
        [f addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        f.layer.borderColor = [UIColor blackColor].CGColor;
        f.layer.borderWidth = 3.0f;
        
        g = [UIButton buttonWithType:UIButtonTypeCustom];
        g.frame = CGRectMake(f.frame.origin.x+f.frame.size.width, 0, widthWhite, self.frame.size.height);
        [g setBackgroundImage:[[UIImage imageNamed:@"whiteKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview: g];
        g.backgroundColor = [UIColor whiteColor];
        g.tag = 7;
        [g addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        g.layer.borderColor = [UIColor blackColor].CGColor;
        g.layer.borderWidth = 3.0f;
        
        a= [UIButton buttonWithType:UIButtonTypeCustom];
        a.frame = CGRectMake(g.frame.origin.x+g.frame.size.width, 0, widthWhite, self.frame.size.height);
        [a setBackgroundImage:[[UIImage imageNamed:@"whiteKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        a.backgroundColor = [UIColor whiteColor];
        [self addSubview: a];
        a.tag = 9;
        [a addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        a.layer.borderColor = [UIColor blackColor].CGColor;
        a.layer.borderWidth = 3.0f;
        
        b= [UIButton buttonWithType:UIButtonTypeCustom];
        b.frame = CGRectMake(a.frame.origin.x+a.frame.size.width, 0, widthWhite, self.frame.size.height);
        [b setBackgroundImage:[[UIImage imageNamed:@"whiteKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview: b];
        b.backgroundColor = [UIColor whiteColor];
        b.tag = 11;
        [b addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        b.layer.borderColor = [UIColor blackColor].CGColor;
        b.layer.borderWidth = 3.0f;
        
        db = [UIButton buttonWithType:UIButtonTypeCustom];
        db.frame = CGRectMake(c.frame.origin.x+0.67*c.frame.size.width, 0, blackWidth, 120);
        [db setBackgroundImage:[[UIImage imageNamed:@"blackKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview:db];
        db.backgroundColor = [UIColor blackColor];
        db.tag = 1;
        [db addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        db.layer.borderColor = [UIColor blackColor].CGColor;
        db.layer.borderWidth = 3.0f;
        
        eb = [UIButton buttonWithType:UIButtonTypeCustom];
        eb.frame = CGRectMake(d.frame.origin.x+0.67*d.frame.size.width, 0, blackWidth, 120);
        [eb setBackgroundImage:[[UIImage imageNamed:@"blackKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview:eb];
        eb.backgroundColor = [UIColor blackColor];
        eb.tag = 3;
        [eb addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        eb.layer.borderColor = [UIColor blackColor].CGColor;
        eb.layer.borderWidth = 3.0f;
        
        gb = [UIButton buttonWithType:UIButtonTypeCustom];
        gb.frame = CGRectMake(f.frame.origin.x+0.67*f.frame.size.width, 0, blackWidth, 120);
        [gb setBackgroundImage:[[UIImage imageNamed:@"blackKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview:gb];
        gb.backgroundColor = [UIColor blackColor];
        gb.tag = 6;
        [gb addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        gb.layer.borderColor = [UIColor blackColor].CGColor;
        gb.layer.borderWidth = 3.0f;
        
        ab = [UIButton buttonWithType:UIButtonTypeCustom];
        ab.frame = CGRectMake(g.frame.origin.x+0.67*g.frame.size.width, 0, blackWidth, 120);
        [ab setBackgroundImage:[[UIImage imageNamed:@"blackKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        ab.backgroundColor = [UIColor blackColor];
        [self addSubview:ab];
        ab.tag = 8;
        [ab addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        ab.layer.borderColor = [UIColor blackColor].CGColor;
        ab.layer.borderWidth = 3.0f;
        
        bb = [UIButton buttonWithType:UIButtonTypeCustom];
        bb.frame = CGRectMake(a.frame.origin.x+0.67*a.frame.size.width, 0, blackWidth, 120);
        [bb setBackgroundImage:[[UIImage imageNamed:@"blackKey.png"] stretchableImageWithLeftCapWidth:20.0 topCapHeight:0.0] forState:UIControlStateNormal];
        [self addSubview:bb];
        bb.backgroundColor = [UIColor blackColor];
        bb.tag = 10;
        [bb addTarget:self action:@selector(keyboardNotePressed:) forControlEvents:UIControlEventTouchDown];
        bb.layer.borderColor = [UIColor blackColor].CGColor;
        bb.layer.borderWidth = 3.0f;
        
        KMDKeyBoardOverlay *over = [[KMDKeyBoardOverlay alloc] initWithFrame:self.bounds];
        [self addSubview:over];
        
    }
    return self;
}

- (void) keyboardNotePressed: (id) sender {
    UIButton *pressed = sender;
    [self keyPressed:pressed.tag];
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *tou = [touches anyObject];
    CGPoint thisPoint = [tou locationInView:self];
    currentlyPressed = [self findKeyForPoint:thisPoint];
    
    [self keyPressed:currentlyPressed.tag];
    [self pressButton:currentlyPressed down:YES];

    // NSLog(@"%@", NSStringFromCGPoint([tou locationInView:self]));
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *tou = [touches anyObject];
    //NSLog(@"%@", NSStringFromCGPoint([tou locationInView:self]));
    [self pressButton:currentlyPressed down:NO];

    [self keyLifted:currentlyPressed.tag];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *tou = [touches anyObject];
    CGPoint loc = [tou locationInView:self];
    //NSLog(@"%@", NSStringFromCGPoint([tou locationInView:self]));
    
    if(currentlyPressed!=[self findKeyForPoint:loc] && currentlyPressed != nil) {
        if([self findKeyForPoint:loc] != nil) {
            [self pressButton:currentlyPressed down:NO];
            currentlyPressed = [self findKeyForPoint:loc];
            [self keySustainChangedNote:currentlyPressed.tag];
            [self pressButton:currentlyPressed down:YES];

        } else {
            [self keyLifted:currentlyPressed.tag];
            [self pressButton:currentlyPressed down:NO];
            currentlyPressed = nil;
        }
    } else if(currentlyPressed == nil && [self findKeyForPoint:loc]!=nil) {
        currentlyPressed = [self findKeyForPoint:loc];
        [self pressButton:currentlyPressed down:YES];

        [self keyPressed:currentlyPressed.tag];
    }
}

- (void) keyPressed:(int) note {
    [self.delegate keyPressed:note];
}

- (void) keyLifted:(int) note {
    [self.delegate keyLifted:note];
    
}

- (void) keySustainChangedNote:(int) note {
    [self.delegate keySustainChangedNote:note];
}

- (UIButton*) findKeyForPoint:(CGPoint) thisPoint {
    UIButton *press;
    
    if(CGRectContainsPoint(c.frame, thisPoint)) {
        press = c;
    } else if(CGRectContainsPoint(d.frame, thisPoint)) {
        press = d;
    } else if(CGRectContainsPoint(e.frame, thisPoint)) {
        press = e;
    } else if(CGRectContainsPoint(f.frame, thisPoint)) {
        press = f;
    } else if(CGRectContainsPoint(g.frame, thisPoint)) {
        press = g;
    } else if(CGRectContainsPoint(a.frame, thisPoint)) {
        press = a;
    } else if(CGRectContainsPoint(b.frame, thisPoint)) {
        press = b;
    }
    
    if(CGRectContainsPoint(db.frame, thisPoint)) {
        press = db;
    } else if(CGRectContainsPoint(eb.frame, thisPoint)) {
        press = eb;
    } else if(CGRectContainsPoint(gb.frame, thisPoint)) {
        press = gb;
    } else if(CGRectContainsPoint(ab.frame, thisPoint)) {
        press = ab;
    } else if(CGRectContainsPoint(bb.frame, thisPoint)) {
        press = bb;
    }
    
    return press;
}



- (void) pressButton:(UIButton*) aButton down:(BOOL) yes {
    if(yes) {
        if(aButton==c) {
            aButton.backgroundColor = [UIColor lightGrayColor];
        } else if(aButton==db) {
            aButton.backgroundColor = [UIColor darkGrayColor];
        } else if(aButton==d) {
            aButton.backgroundColor = [UIColor lightGrayColor];
        } else if(aButton==eb) {
            aButton.backgroundColor = [UIColor darkGrayColor];
        } else if(aButton==e) {
            aButton.backgroundColor = [UIColor lightGrayColor];
        } else if(aButton==f) {
            aButton.backgroundColor = [UIColor lightGrayColor];
        } else if(aButton==gb) {
            aButton.backgroundColor = [UIColor darkGrayColor];
        } else if(aButton==g) {
            aButton.backgroundColor = [UIColor lightGrayColor];
        } else if(aButton==ab) {
            aButton.backgroundColor = [UIColor darkGrayColor];
        }else if(aButton==a) {
            aButton.backgroundColor = [UIColor lightGrayColor];
        } else if(aButton==bb) {
            aButton.backgroundColor = [UIColor darkGrayColor];
        } else if(aButton==b) {
            aButton.backgroundColor = [UIColor lightGrayColor];
        }
    } else {
        if(aButton==c) {
            aButton.backgroundColor = [UIColor whiteColor];
        } else if(aButton==db) {
            aButton.backgroundColor = [UIColor blackColor];
        } else if(aButton==d) {
            aButton.backgroundColor = [UIColor whiteColor];
        } else if(aButton==eb) {
            aButton.backgroundColor = [UIColor blackColor];
        } else if(aButton==e) {
            aButton.backgroundColor = [UIColor whiteColor];
        } else if(aButton==f) {
            aButton.backgroundColor = [UIColor whiteColor];
        } else if(aButton==gb) {
            aButton.backgroundColor = [UIColor blackColor];
        } else if(aButton==g) {
            aButton.backgroundColor = [UIColor whiteColor];
        } else if(aButton==ab) {
            aButton.backgroundColor = [UIColor blackColor];
        }else if(aButton==a) {
            aButton.backgroundColor = [UIColor whiteColor];
        } else if(aButton==bb) {
            aButton.backgroundColor = [UIColor blackColor];
        } else if(aButton==b) {
            aButton.backgroundColor = [UIColor whiteColor];
        }
    }
}





@end
