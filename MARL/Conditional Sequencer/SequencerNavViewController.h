//
//  SequencerNavViewController.h
//  MARL
//
//  Created by Kevin Murphy on 9/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SequencerNavViewController : UINavigationController

@end
