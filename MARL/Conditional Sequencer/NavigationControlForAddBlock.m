//
//  NavigationControlForAddBlock.m
//  MARL
//
//  Created by Kevin Murphy on 10/8/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "NavigationControlForAddBlock.h"

@interface NavigationControlForAddBlock ()

@end

@implementation NavigationControlForAddBlock

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
