//
//  OverlayMessage.h
//  MARL
//
//  Created by Kevin Murphy on 10/21/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverlayMessage : UIView
{
    UILabel *message;
    float duration;
}
+ (OverlayMessage*) messageWithTitle:(NSString*) title andDuration:(float) duration;
@end
