//  PitchDetector.h
//  MusiciansKit
//  Created by Kevin Murphy on 2/16/12.
//  Copyright (c) 2012 Kevin P Murphy. All rights reserved.

#import <Foundation/Foundation.h>
#import "TPCircularBuffer.h"

@class AudioController;

@protocol PitchDetectorDelegate <NSObject>
- (void) foundPitch:(float) frequency;
@end

@interface PitchDetector : NSObject
{
    @public
    
    int hiBound;
    
    BOOL running, sprinting;
    
    TPCircularBuffer bufferPitch, samps;
    int bufferLength;
    float *hann;
    
    NSTimer *tim;
    
    SInt16 *look;
    double *result;
    BOOL isRunning;
    
    float sampleRate;
    
    int n;
}
@property (nonatomic) id delegate;
-(void) startPerforming;
-(void) stopPerforming;

- (id) initWithSampleRate:(float) sampleRateInit;
- (int) addSamples: (SInt16*) samples inLength:(int) inNumberFrames;

@end
