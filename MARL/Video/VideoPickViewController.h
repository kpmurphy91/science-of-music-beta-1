//
//  VideoViewController.h
//  MARL
//
//  Created by Kevin Patrick Murphy on 7/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Video.h"
#import <QuartzCore/QuartzCore.h>

@interface VideoPickViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
{
    NSMutableArray *listOfVideos, *plistModel;
    UITableView *tableOfVideos;
    UIView *activityView;
}


@end
