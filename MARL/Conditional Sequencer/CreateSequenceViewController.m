//
//  CreateSequenceViewController.m
//  MARL
//
//  Created by Kevin Murphy on 9/13/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "CreateSequenceViewController.h"
#import "SoundFontArray.h"

@interface CreateSequenceViewController ()

@end

@implementation CreateSequenceViewController
@synthesize delegate, arrayOfSoundFonts, title, editing, currentTempo;


- (id) initWithSequence:(Sequence *)aSequence {
    self = [super initWithNibName:nil bundle:nil];
    if(self) {
        currentTempo = aSequence.tempo;
        title = aSequence.title;
        chosenSoundFont = aSequence.sound;
        self.editing = YES;
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currentTempo = 120;
        arrayOfSoundFonts = [[SoundFontArray globalSoundFontArray] mutableCopy];
        chosenSoundFont = [arrayOfSoundFonts objectAtIndex:0];
        self.editing = NO;

        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.contentSizeForViewInPopover = CGSizeMake(300, 280);

    currentTitle = @"";
    
    thisTable = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    thisTable.delegate = self;
    thisTable.dataSource = self;
    
	// Do any additional setup after loading the view.
}
- (void) viewWillAppear:(BOOL)animated {

    tempoField.text = [NSString stringWithFormat:@"%d", currentTempo];
}

- (void) viewDidAppear:(BOOL)animated {
    thisTable.frame = self.view.bounds;
    
    self.navigationItem.title = @"Create Sequence";
    
    
    UIView *buttonsFrame = [[UIView alloc] initWithFrame:CGRectMake(25, 0, thisTable.frame.size.width-50, 40)];
    UIButton *create = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    create.frame = buttonsFrame.bounds;
    if(editing) {
        [create setTitle:@"Edit Sequence" forState:UIControlStateNormal];
    } else {
        [create setTitle:@"Create Sequence" forState:UIControlStateNormal];
    }
    
    [create addTarget:self action:@selector(createSequence) forControlEvents:UIControlEventTouchUpInside];
    [buttonsFrame addSubview:create];
    
    thisTable.tableFooterView = buttonsFrame;
    thisTable.tableFooterView.userInteractionEnabled = YES;
    [self.view addSubview:thisTable];
    
    NSLog(@"CURRENT TEMPO: %d", currentTempo);
    tempoField.text = [NSString stringWithFormat:@"%d", currentTempo];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

int titleTag2 = 100, titleTag1=50;

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        if (indexPath.section == 0) {
            
            // Add a UITextField
            UITextField *textField = [[UITextField alloc] init];
            // Set a unique tag on each text field
            textField.tag = titleTag2 + indexPath.row;
            // Add general UITextAttributes if necessary
            textField.enablesReturnKeyAutomatically = YES;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.placeholder = @"Tempo";
            textField.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:textField];
            
            UILabel *textLabel = [[UILabel alloc] init];
            textLabel.tag = titleTag1 + indexPath.row;
            textLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:textLabel];
        }
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void)configureCell:(UITableViewCell *)theCell atIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        // Get the text field using the tag
        UITextField *textField = (UITextField *)[theCell.contentView viewWithTag:titleTag2+indexPath.row];
        UILabel *textLabel = (UILabel*)[theCell.contentView viewWithTag:titleTag1+indexPath.row];
        
        // Position the text field within the cell bounds
        CGRect cellBounds = theCell.bounds;
        cellBounds = CGRectMake(theCell.bounds.size.width/2, 0, theCell.bounds.size.width/2, theCell.bounds.size.height);
        CGFloat textFieldBorder = 10.f;
        // Don't align the field exactly in the vertical middle, as the text
        // is not actually in the middle of the field.
        float width = theCell.bounds.size.width;
        
        textLabel.frame = CGRectMake(textFieldBorder, 9.f, width/4-(2*textFieldBorder), 31.f);
        textField.frame = CGRectMake(textLabel.frame.origin.x+textLabel.frame.size.width+textFieldBorder, 13.f, width-(4*textFieldBorder)- (textLabel.frame.size.width+textLabel.frame.origin.x)-11, 34.f);

        
        textField.adjustsFontSizeToFitWidth = YES;
        
        
        // Configure UITextAttributes for each field
        if(indexPath.row == 0) {
            textLabel.text = @"Title";
            textField.placeholder = @"Name";
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            titleField = textField;
            
            if(editing) {
                textField.text = title;
            }
            
            if(![currentTitle isEqualToString:@""]) {
                titleField.text = currentTitle;
            }
            
        } else if(indexPath.row == 1) {
            textLabel.text = @"Tempo";
            tempoField = textField;
            tempoField.delegate = self;
            
            textField.placeholder = @"Tempo (Beats Per Minute)";
            textField.returnKeyType = UIReturnKeyNext;
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            
            if(editing) {
                textField.text = [NSString stringWithFormat:@"%d" ,currentTempo];
            }
            
            if(currentTempo>1) {
                tempoField.text = [NSString stringWithFormat:@"%d", currentTempo];
            }
        } else if(indexPath.row == 2) {
            textLabel.text = @"Sound";
            [textField removeFromSuperview];
            
            soundChoice = [[UILabel alloc] initWithFrame:textField.frame];
            soundChoice.frame = CGRectMake(soundChoice.frame.origin.x, soundChoice.frame.origin.y-8, soundChoice.frame.size.width-20, soundChoice.frame.size.height);
            soundChoice.text = chosenSoundFont.title;
            soundChoice.backgroundColor = [UIColor clearColor];
            [theCell.contentView addSubview:soundChoice];
            soundChoice.textAlignment = UITextAlignmentRight;
            [theCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        } else {
        }
    
    }
}

- (void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==1) {
        NSLog(@"hello");
        [self.navigationController pushViewController:[[PopTableController alloc]initWithNibName:nil bundle:nil] animated:YES];
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==2) {
        currentTempo = [tempoField.text intValue];
        currentTitle = titleField.text;
        NSLog(@"TEMPO %d", currentTempo);
        PopTableController *present = [[PopTableController alloc] initWithNibName:nil bundle:nil andArray:[SoundFontArray globalSoundFontArray] andDefaultObject:chosenSoundFont];
        present.delegate = self;
        [self.navigationController pushViewController:present animated:YES];
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (int) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return  0;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  50;
}

- (void) createSequence {
    Sequence *newSeq = [[Sequence alloc] init];
    newSeq.sound = chosenSoundFont;
    [chosenSoundFont load];
    newSeq.tempo = [tempoField.text integerValue];
    if(newSeq.tempo == 0) {
        NSLog(@"Was 0, now resetting tempo to 120");
        newSeq.tempo = 120;
    }
    newSeq.title = titleField.text;
    printf("NEW TEMPO: %d\n", newSeq.tempo);
    [delegate popoverCreatedSequence:newSeq];
}

- (void) soundFontChooserChoseSoundFont:(SequencerSoundFont*) soundFont {
    chosenSoundFont = soundFont;
    soundChoice.text = soundFont.title;
    
    tempoField.text = [NSString stringWithFormat:@"%d", currentTempo];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    if([string length]==0){
        return YES;
    }
    
    //Validate Character Entry
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789,."];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        
        if ([myCharSet characterIsMember:c]) {
            //now check if string already has 1 decimal mark
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            if([sep count]>2) return NO;
            return YES;
            
        }
        
    }
    
    return NO;
}




@end
