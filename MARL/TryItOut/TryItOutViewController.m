//
//  TryItOutViewController.m
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "TryItOutViewController.h"

@interface TryItOutViewController ()

@end

@implementation TryItOutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    tableViewOfChoices = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 600, 600) style:UITableViewStyleGrouped];
    tableViewOfChoices.backgroundColor = [UIColor clearColor];
    tableViewOfChoices.backgroundView = nil;
    tableViewOfChoices.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    tableViewOfChoices.dataSource = self;
    tableViewOfChoices.delegate = self;
    tableViewOfChoices.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
    
    [self.view addSubview:tableViewOfChoices];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


- (void) presentADSRViewController {
    ADSRViewController *presentThis = [[ADSRViewController alloc] initWithNibName:nil bundle:nil];
    presentThis.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.navigationController pushViewController:presentThis animated:YES];

}

- (void) presentFFT {
    FFTGraphViewController *presentThis = [[FFTGraphViewController alloc] initWithNibName:nil bundle:nil];
    presentThis.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.navigationController pushViewController:presentThis animated:YES];
}

- (void) presentAddSynth {
    AdditiveSynthViewController *presentThis = [[AdditiveSynthViewController alloc] initWithNibName:nil bundle:nil];
    presentThis.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self.navigationController pushViewController:presentThis animated:YES];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Choice"];
    if(indexPath.row == 0) {
        cell.textLabel.text = @"ADSR Envelope";
    } else if(indexPath.row == 1) {
        cell.textLabel.text = @"FFT Graph";
    } else if(indexPath.row == 2) {
        cell.textLabel.text = @"Additive Synthesizer";
    }
    cell.textLabel.textAlignment = UITextAlignmentRight;

    return  cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([indexPath row]==0)
        [self presentADSRViewController];
    if([indexPath row] ==1) 
        [self presentFFT];
    if([indexPath row] ==2)
        [self presentAddSynth];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (int) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return  0;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  100;
}
@end
