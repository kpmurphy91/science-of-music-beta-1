//
//  SoundFontArray.m
//  MARL
//
//  Created by Kevin Murphy on 9/17/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "SoundFontArray.h"

@implementation SoundFontArray
+ (NSArray*) globalSoundFontArray {
    NSMutableArray *arrayOfSoundFonts = [[NSMutableArray alloc] initWithCapacity:1];
    
    SequencerSoundFont *aSoundF = [[SequencerSoundFont alloc] init];
    aSoundF.sf2Name = @"ff4sf2";
    aSoundF.noteOnTimeout = 4;
    [arrayOfSoundFonts addObject:aSoundF];
    aSoundF.title = @"Final Fantasy";
    
    aSoundF = [[SequencerSoundFont alloc] init];
    aSoundF.sf2Name = @"BasicWavforms";
    aSoundF.title = @"Basic Waveforms";
    aSoundF.noteOnTimeout = 0.4;
    [arrayOfSoundFonts addObject:aSoundF];
    
    aSoundF = [[SequencerSoundFont alloc] init];
    aSoundF.sf2Name = @"Piano";
    aSoundF.title = @"Piano";
    aSoundF.noteOnTimeout = 3;
    [arrayOfSoundFonts addObject:aSoundF];
    
    
    NSURL *presetURL;
    NSURL *ppresetURL = [[NSURL alloc] initFileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Vibraphone" ofType:@"aupreset"]];
    if (ppresetURL) {
        NSLog(@"Attempting to load preset '%@'\n", [presetURL description]);
        presetURL = ppresetURL;
        SequencerSoundFont *aSecondFont = [[SequencerSoundFont alloc] init];
        aSecondFont.presetURL = presetURL;
        aSecondFont.noteOnTimeout = 3;
        [arrayOfSoundFonts addObject:aSecondFont];
        aSecondFont.title = @"Vibraphone";
        //self.currentPresetLabel.text = @"Vibraphone";
    } else {
        NSLog(@"COULD NOT GET PRESET PATH!");
    }
    
    return [NSArray arrayWithArray:arrayOfSoundFonts];
}

@end
