//
//  CreateConditionalViewController.m
//  MARL
//
//  Created by Kevin Murphy on 9/13/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "CreateConditionalViewController.h"

@interface CreateConditionalViewController ()

@end

@implementation CreateConditionalViewController
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    [self forcePopoverSize];
}

- (void) viewDidAppear:(BOOL)animated {
    thisTable = [[UITableView alloc] initWithFrame:CGRectMake(0,0,300,280) style:UITableViewStyleGrouped];
    thisTable.delegate = self;
    thisTable.dataSource = self;
    [self.view addSubview:thisTable];
    
    self.contentSizeForViewInPopover = CGSizeMake(300, 280);
    
    self.navigationItem.title = @"Create Conditional";

    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Choice"];
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    cell.textLabel.text = @"hello";
    
    if(indexPath.row == 0) {
        cell.textLabel.text = @"Shake";
    } else if(indexPath.row == 1) {
        cell.textLabel.text = @"Tap and Hold";
    } else if(indexPath.row == 2) {
        cell.textLabel.text = @"Single Tap";
    } else if(indexPath.row == 3) {
        cell.textLabel.text = @"Double Tap";
    } else if(indexPath.row == 4) {
        cell.textLabel.text = @"Repeat  X";

        repeatsFied = [[UITextField alloc] initWithFrame:CGRectMake(100, 10, 170, 40)];
        repeatsFied.placeholder = @"Number Of Repeats";
        repeatsFied.delegate = self;
        repeatsFied.borderStyle = UITextBorderStyleRoundedRect;
        
        repeatsFied.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        [cell.contentView addSubview:repeatsFied];
    }
    
    return  cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row==0) {
        [delegate popoverCreatedConditional:[[PatcherConditionalBlock alloc] initWithCenter:CGPointMake(90, 90) andConditional:ConditionalType_ShakeGesture]];
    } else if(indexPath.row==1) {
        [delegate popoverCreatedConditional:[[PatcherConditionalBlock alloc] initWithCenter:CGPointMake(90, 90) andConditional:ConditionalType_TapAndHold]];
    } else if(indexPath.row==2) {
        [delegate popoverCreatedConditional:[[PatcherConditionalBlock alloc] initWithCenter:CGPointMake(90, 90) andConditional:ConditionalType_SingleTap]];
    } else if(indexPath.row==3) {
        [delegate popoverCreatedConditional:[[PatcherConditionalBlock alloc] initWithCenter:CGPointMake(90, 90) andConditional:ConditionalType_DoubleTap]];
    } else if(indexPath.row==4) {
        if([repeatsFied.text intValue]<1){
            repeatsFied.text = @"1";
        }
        [delegate popoverCreatedConditional:[[PatcherConditionalBlock alloc] initWithCenter:CGPointMake(90, 90) andRepeats:[repeatsFied.text intValue]]];
    }
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (int) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return  0;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  60;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    if([string length]==0){
        return YES;
    }
    
    //Validate Character Entry
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789,."];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        
        if ([myCharSet characterIsMember:c]) {
            
            //now check if string already has 1 decimal mark
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            if([sep count]>2) return NO;
            return YES;
            
        }
        
    }
    
    return NO;
}


- (void) forcePopoverSize {
    CGSize currentSetSizeForPopover = self.contentSizeForViewInPopover;
    CGSize fakeMomentarySize = CGSizeMake(currentSetSizeForPopover.width - 1.0f, currentSetSizeForPopover.height - 1.0f);
    self.contentSizeForViewInPopover = fakeMomentarySize;
}


@end

