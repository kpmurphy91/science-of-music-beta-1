//
//  EditSequencerView.h
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SequenceTouchInterceptor.h"
#import "TouchGrid.h"
#import "PopTableController.h"
#import "PatcherSequenceBlock.h"
#import "Sequence.h"
#import "CreateSequenceViewController.h"

@protocol SequenceBuilderDelegate <NSObject>
-(void) sequenceBuilderFinishedWithSequence:(Sequence*) aSequence;
@end

@interface EditSequencerView : UIView <SoundFontChooserDelegate, CreateSequenceDelegate>
{
    TouchGrid *sequenceButtonGridView;
    NSTimer *sequenceTimer;
    
    BOOL isplaying;
}
@property (nonatomic, readwrite) PatcherSequenceBlock *editingBlock;
@property (nonatomic) int columns;
@property (nonatomic) int rows;
@property (nonatomic, readwrite) UIPopoverController *soundPopoverController;
@property (nonatomic, readwrite) id<SequenceBuilderDelegate> delegate;
@property (nonatomic, readwrite) Sequence *thisSequence;
- (void) presentEditor:(id) sender;
- (void) clear;
- (void) play;
- (void) stop;

- (id) initWithFrame:(CGRect)frame andSequence: (Sequence*) thisSeq orBlock:(PatcherSequenceBlock*) block;
- (Sequence*) closeSequence;
@end
