//
//  ADSRView.m
//  Science of Music
//
//  Created by Kevin Murphy on 6/8/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "ADSRView.h"
#import "SynthDefs.h"

#define PADDING_SIDES 30
#define PADDING_TOP 10
#define CIRCLE_SIZE 70
@implementation ADSRView
@synthesize adsrValues, delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor clearColor];
    if (self) {
        
        wrapper = [[UIView alloc] initWithFrame:CGRectMake(PADDING_SIDES, PADDING_TOP, frame.size.width - (PADDING_SIDES *2), frame.size.height - (PADDING_SIDES*2))];
        wrapper.layer.cornerRadius = 5;
        wrapper.layer.masksToBounds = YES;
        wrapper.layer.borderColor = RGBA(0x55, 0x55, 0x55, 1).CGColor;
        wrapper.backgroundColor = RGBA(30, 40, 50, 0.3);//[UIColor whiteColor];
        wrapper.layer.borderWidth = 2;
        //wrapper.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        graph = [[Grapher alloc] initWithFrame:CGRectMake(2, 3, wrapper.frame.size.width-8, wrapper.frame.size.height-2)];
        graph.lineColor = RGBA(33, 45, 50, 1);
        graph.shouldFill = YES;
        
        UIView *backGround = [[UIView alloc] initWithFrame:graph.frame];
        backGround.backgroundColor = [UIColor whiteColor];
        
        axis = [[UIView alloc] initWithFrame:CGRectMake(0, graph.frame.origin.y + graph.frame.size.height, wrapper.frame.size.width, (wrapper.frame.size.height-graph.frame.size.height)/2)];
        axis.backgroundColor = [UIColor whiteColor];
        
        options = [[UIView alloc] initWithFrame:CGRectMake(0, axis.frame.origin.y + axis.frame.size.height, wrapper.frame.size.width, axis.frame.size.height)];
        options.backgroundColor = [UIColor whiteColor];
        
        struct ADSRStruct original;
        original.start = CGPointMake(0, 0);
        original.A = CGPointMake(0.1, 0.8);
        original.D = CGPointMake(0.3, 0.3);
        original.S = CGPointMake(0.8, original.D.y);
        original.R = CGPointMake(0.95, 0);
        original.duration = 0.1;
        
        self.adsrValues = original;
        
        startCircle = [[CirclePoint alloc] initWithFrame:CGRectMake((graph.frame.origin.x+ graph.frame.size.width*adsrValues.start.x), (graph.frame.origin.y + graph.frame.size.height) - (adsrValues.start.y*graph.frame.size.height), CIRCLE_SIZE, CIRCLE_SIZE)];
        startCircle.onFloor = YES;
        startCircle.center = startCircle.frame.origin;
        startCircle.graphFrame = graph.frame;
        startCircle.label = @"";
        startCircle.delegate = self;
        
        attackCircle = [[CirclePoint alloc] initWithFrame:CGRectMake((graph.frame.origin.x+ graph.frame.size.width*adsrValues.A.x), (graph.frame.origin.y + graph.frame.size.height) - (adsrValues.A.y*graph.frame.size.height), CIRCLE_SIZE, CIRCLE_SIZE)];
        attackCircle.center = attackCircle.frame.origin;
        attackCircle.graphFrame = graph.frame;
        attackCircle.label = @"A";
        attackCircle.delegate = self;
        
        decayCircle = [[CirclePoint alloc] initWithFrame:CGRectMake((graph.frame.origin.x+ graph.frame.size.width*adsrValues.D.x), (graph.frame.origin.y + graph.frame.size.height) - (adsrValues.D.y*graph.frame.size.height), CIRCLE_SIZE, CIRCLE_SIZE)];
        decayCircle.center = decayCircle.frame.origin;
        decayCircle.graphFrame = graph.frame;
        decayCircle.label = @"D";
        decayCircle.delegate = self;
        
        sustainCircle = [[CirclePoint alloc] initWithFrame:CGRectMake((graph.frame.origin.x+ graph.frame.size.width*adsrValues.S.x), (graph.frame.origin.y + graph.frame.size.height) - (adsrValues.S.y*graph.frame.size.height), CIRCLE_SIZE, CIRCLE_SIZE)];
        sustainCircle.center = sustainCircle.frame.origin;
        sustainCircle.graphFrame = graph.frame;
        sustainCircle.label = @"S";
        sustainCircle.delegate = self;
        
        releaseCircle = [[CirclePoint alloc] initWithFrame:CGRectMake((graph.frame.origin.x+ graph.frame.size.width*adsrValues.R.x), (graph.frame.origin.y + graph.frame.size.height) - (adsrValues.R.y*graph.frame.size.height), CIRCLE_SIZE, CIRCLE_SIZE)];
        releaseCircle.center = releaseCircle.frame.origin;
        releaseCircle.graphFrame = graph.frame;
        releaseCircle.label = @"R";
        releaseCircle.onFloor = YES;
        releaseCircle.delegate = self;
        
        decayCircle.sisterYPoint = sustainCircle;
        sustainCircle.sisterYPoint = decayCircle;
        
        attackCircle.lowerBoundSibling = decayCircle;
        decayCircle.upperBoundSibling = attackCircle;
        sustainCircle.upperBoundSibling = attackCircle;
        
        startCircle.rightCircle = attackCircle;
        attackCircle.leftCircle = startCircle;
        attackCircle.rightCircle = decayCircle;
        decayCircle.leftCircle = attackCircle;
        decayCircle.rightCircle = sustainCircle;
        sustainCircle.leftCircle = decayCircle;
        sustainCircle.rightCircle = releaseCircle;
        releaseCircle.leftCircle = sustainCircle;
        
        
        
        [wrapper addSubview:graph];
        [wrapper addSubview:axis];
        [wrapper addSubview:options];
        
        sustainWindow = [[UIView alloc] initWithFrame:CGRectMake(decayCircle.center.x, decayCircle.center.y, sustainCircle.center.x - decayCircle.center.x, wrapper.frame.size.height-sustainCircle.center.y)];
        sustainWindow.clipsToBounds = YES;
        sustainImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"diagonalLines.png"]];
        //sustainImage.frame = CGRectMake(0, 0, sustainWindow.frame.size.width, sustainWindow.frame.size.height);

        [sustainWindow addSubview:sustainImage];
        [wrapper addSubview:sustainWindow];
        
        //[wrapper addSubview:startCircle];
        [wrapper addSubview:releaseCircle];
        [wrapper addSubview:attackCircle];
        [wrapper addSubview:decayCircle];
        [wrapper addSubview:sustainCircle];
        
        
        [self addSubview:wrapper];
        
        [self pointsChangedPosition];
    }
    return self;
}

- (void) updateADSRValues {
    
    adsrValues.start = [startCircle scaledPosition];
    adsrValues.A = [attackCircle scaledPosition];
    adsrValues.D = [decayCircle scaledPosition];
    adsrValues.S = [sustainCircle scaledPosition];
    adsrValues.R = [releaseCircle scaledPosition];
    adsrValues.duration = 0.25;
    
    
    [delegate newADSRStruct:adsrValues];
    //NSLog(@"H");
}

- (void) pointsChangedPosition {
    [self updateADSRValues];
    
    float *pointsy = (float*) malloc(sizeof(float)*5);
    float *pointsx = (float*) malloc(sizeof(float)*5);

    pointsx[0] = adsrValues.start.x;
    pointsy[0] = 0;//adsrValues.start.y * graph.frame.size.height;
    pointsx[1] = adsrValues.A.x;
    pointsy[1] = adsrValues.A.y* graph.frame.size.height-4;
    pointsx[2] = adsrValues.D.x;
    pointsy[2] = adsrValues.D.y* graph.frame.size.height-4;
    pointsx[3] = adsrValues.S.x;
    pointsy[3] = adsrValues.S.y* graph.frame.size.height-4;
    pointsx[4] = adsrValues.R.x;
    pointsy[4] = 0;//adsrValues.R.y* graph.frame.size.height;
    
    [graph setPoints:pointsy withXorNil:pointsx numPuntos:5];
    
    sustainWindow.frame = CGRectMake(decayCircle.center.x, decayCircle.center.y, sustainCircle.center.x - decayCircle.center.x, wrapper.frame.size.height-sustainCircle.center.y);
    
    NSLog(@"%@  - %@", NSStringFromCGPoint(decayCircle.center),NSStringFromCGRect(sustainWindow.frame));
    free(pointsx);
    free(pointsy);
    //These are points that can need to be scaled, and then inserted to audio controller
}



#pragma mark UITextView Delegate
#pragma mark -

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return NO;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    if([string length]==0){
        return YES;
    }
    
    //Validate Character Entry
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789,."];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        
        if ([myCharSet characterIsMember:c]) {
            
            //now check if string already has 1 decimal mark
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            if([sep count]>2) return NO;
            return YES;
            
        }
        
    }
    
    return NO;
}

- (BOOL)isNumeric:(NSString *)input {
    for (int i = 0; i < [input length]; i++) {
        char c = [input characterAtIndex:i];
        // Allow a leading '-' for negative integers
        if (!((c == '-' && i == 0) || (c >= '0' && c <= '9'))) {
            return NO;
        }
    }
    return YES;
}


@end
