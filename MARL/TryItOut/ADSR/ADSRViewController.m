//
//  ADSRViewController.m
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "ADSRViewController.h"

@interface ADSRViewController ()

@end

@implementation ADSRViewController
@synthesize keyState;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [AudioController sharedAudioManager].delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton* myInfoButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [myInfoButton addTarget:self action:@selector(infoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:myInfoButton];
    
    float whatsmaller = (self.view.bounds.size.width>self.view.bounds.size.height)?self.view.bounds.size.height:self.view.bounds.size.width;
    whatsmaller-=20;
    
    graphView = [[ADSRView alloc] initWithFrame:CGRectMake(0, 0, whatsmaller, whatsmaller)];
    graphView.center = CGPointMake(self.view.bounds.size.width/2, graphView.center.y);
    graphView.delegate = self;
    graphView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin);
    [self.view addSubview:graphView];
    
    keyboardView = [[KMDKeyBoard alloc] initWithFrame:CGRectMake(graphView.frame.origin.x, graphView.frame.origin.y+graphView.frame.size.height-30, graphView.frame.size.width, 200+30)];
    keyboardView.delegate = self;
    keyboardView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:keyboardView];
    
    [self newADSRStruct:graphView.adsrValues];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void) viewDidAppear:(BOOL)animated {
    [AudioController sharedAudioManager].inputRunning = NO;
    [AudioController sharedAudioManager].envelopeDelegate = self;
    [AudioController sharedAudioManager].keyState = synthKeyStateNoTouch;
    
    float whatsmaller = (self.view.bounds.size.width>self.view.bounds.size.height)?self.view.bounds.size.height:self.view.bounds.size.width;
    whatsmaller-=20;
    
    graphView.frame = CGRectMake(0, 0, whatsmaller, whatsmaller);
    graphView.center = CGPointMake(self.view.bounds.size.width/2, graphView.center.y);
    
    keyboardView.frame = CGRectMake(graphView.frame.origin.x, graphView.frame.origin.y+graphView.frame.size.height-30, graphView.frame.size.width, 200+30);
}

- (void) viewWillDisappear:(BOOL)animated {
    [[AudioController sharedAudioManager] stopAudio];
    [AudioController sharedAudioManager].envelopeDelegate = nil;

}

- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (void) dismiss {
    [AudioController sharedAudioManager].envelopeDelegate = nil;
    [self dismissModalViewControllerAnimated:YES];
}

- (void) newADSRStruct:(struct ADSRStruct)newADSRStruct {
    struct ADSRStruct thisNewStruct;
    thisNewStruct = newADSRStruct;
    thisNewStruct.start.x = [self scaledToSampleCount:newADSRStruct.start.x];
    thisNewStruct.A.x = [self scaledToSampleCount:newADSRStruct.A.x-newADSRStruct.start.x];
    thisNewStruct.D.x = [self scaledToSampleCount:newADSRStruct.D.x-newADSRStruct.A.x];
    thisNewStruct.S.x = [self scaledToSampleCount:newADSRStruct.S.x-newADSRStruct.D.x];
    thisNewStruct.R.x = [self scaledToSampleCount:newADSRStruct.R.x-newADSRStruct.S.x];
    
    thisNewStruct.A.y=thisNewStruct.A.y*0.7+0.3;
    thisNewStruct.D.y=thisNewStruct.D.y*0.7+0.3;
    thisNewStruct.S.y=thisNewStruct.S.y*0.7+0.3;
    
    thisNewStruct.S.y = thisNewStruct.D.y;
    printf("%f, %f, %f, %f\n", thisNewStruct.A.y, thisNewStruct.D.y, thisNewStruct.S.y, thisNewStruct.R.y);
    
    [AudioController sharedAudioManager].sampleADSRStruct = thisNewStruct;
}

- (void) keyPressed:(int)chromaticNumber {
    printf("Note Number Pressed: %d\n", chromaticNumber+4*12);
    [AudioController sharedAudioManager]->chosenTable = chromaticNumber;
    [AudioController sharedAudioManager].keyState = synthKeyStateTouchBegan;
}

- (void) keySustainChangedNote:(int)chromaticNumber {
    [AudioController sharedAudioManager]->chosenTable = chromaticNumber;
    [AudioController sharedAudioManager].keyState = synthKeyStateTouchBegan;
}

- (void) keyLifted:(int)chromaticNumber {
    printf("Note Number Lifted: %d\n", chromaticNumber+4*12);

    [AudioController sharedAudioManager].keyState = synthKeyStateTouchLifted;

}

- (int) scaledToSampleCount:(float) inScale {
    int outSamples = 0;
    
    float duration = 0.3*inScale;
    outSamples = duration*[AudioController sharedAudioManager].audioFormat.mSampleRate;
    
    return outSamples;
}

- (void) infoButtonClicked:(id) sender {
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.navigationController.view cache:NO];
    
    NSData *htmlData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ADSRHelp" ofType:@"html"]];
    
    HelpViewController *helpController = [HelpViewController helpControllerWithHTML:htmlData];
    
    [self.navigationController pushViewController:helpController animated:YES];
    [UIView commitAnimations];
}


@end
