//
//  SavedConditionalSequencesPopViewController.m
//  MARL
//
//  Created by Kevin Murphy on 9/17/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//


#import "SavedConditionalSequencesPopViewController.h"

@implementation SavedConditionalSequencesPopViewController
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.contentSizeForViewInPopover = CGSizeMake(250, 300);
        
        paths = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:[@"~/Documents/Sequences/" stringByExpandingTildeInPath]  error:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated {
    choices = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    choices.delegate = self;
    choices.dataSource = self;
    [self.view addSubview:choices];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Choice"];
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    
    if(indexPath.row == 0) {
        cell.textLabel.textAlignment = UITextAlignmentCenter;
        cell.textLabel.text = @"Create New Sequence";
    } else {
        NSString *path = [@"~/Documents/Sequences" stringByAppendingPathComponent:[paths objectAtIndex:indexPath.row-1]];
        cell.textLabel.text = [[path lastPathComponent] stringByDeletingPathExtension];
    }
    
    return  cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == 0) {
        [delegate makeNewPatch];
    } else {
        [delegate choseSequence:[paths objectAtIndex:indexPath.row-1]];
    }
    
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return paths.count+1;
}

- (int) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return  0;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  60;
}


@end
