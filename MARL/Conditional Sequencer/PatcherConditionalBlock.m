//
//  PatcherConditionalBlock.m
//  MARL
//
//  Created by Kevin Murphy on 8/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "PatcherConditionalBlock.h"
#define degreesToRadians(x) (M_PI * x / 180.0)
@implementation PatcherConditionalBlock
@synthesize delegate;
@synthesize topPort, bottomPort, leftPort, rightPort, leftConditional, rightConditional, topConditional, bottomConditional, conditionalTag;
@synthesize typeOfConditional, completed, currentRepeat, finished;

static int tagCount = 0;

- (id) initWithCenter: (CGPoint) centerPoint andRepeats:(int) rep {
    self = [self initWithCenter:centerPoint andConditional:ConditionalType_Repeats];
    conditionalTag = tagCount++;
    
    conditionalRepeat = rep;
    
    
    titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
    titleLabel.text = [NSString stringWithFormat:@"Repeat X %d", rep];
    titleLabel.textAlignment = UITextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:titleLabel];
    
    return self;
}

- (id) initWithCenter: (CGPoint) centerPoint andConditional:(ConditionalType)type {
    
    CGSize blockSize = CGSizeMake(140, 140);
    CGRect frame = CGRectMake(centerPoint.x-blockSize.width/2, centerPoint.y-blockSize.height/2, blockSize.width, blockSize.height);
    
    self = [super initWithFrame:frame];
    if (self) {
        self.typeOfConditional = type;
        self.alpha = 1;
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ConditionalBlock.png"]];
        
        alreadyPulsed = NO;

        
        titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
        titleLabel.textAlignment = UITextAlignmentCenter;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:17];
        titleLabel.textColor = [UIColor whiteColor];
        [self addSubview:titleLabel];
        
        if(type == ConditionalType_TapAndHold) {
            UIGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPress:)];
            [self addGestureRecognizer:longPress];
            titleLabel.text = @"Tap and Hold";
        } else if(type == ConditionalType_SingleTap) {
            UIGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(singleTap:)];
            titleLabel.text = @"Single Tap";
            [self addGestureRecognizer:singleTap];
        } else if(type == ConditionalType_DoubleTap) {
            titleLabel.text = @"Double Tap";
            UIGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
            [self addGestureRecognizer:doubleTap];
        } else if(type == ConditionalType_ShakeGesture) {
            titleLabel.text = @"Shake";

        }
        
    }
    return self;
}


- (id) initWithCoder:(NSCoder *)aDecoder {
    //CGRect aFrame = [aDecoder decodeCGRectForKey:@""];
    CGPoint cenPoint = [aDecoder decodeCGPointForKey:@"centerPoint"];
    ConditionalType type = (ConditionalType)[aDecoder decodeIntForKey:@"typeOfConditional"];
    if(type==ConditionalType_Repeats) {
        int reps = [aDecoder decodeIntForKey:@"repeat"];
        self = [self initWithCenter:cenPoint andRepeats:reps];
    } else {
        self = [self initWithCenter:cenPoint andConditional:type];
    }
    
    self->bottomConditionalTag = [aDecoder decodeIntForKey:@"bottomConditional"];
    self->leftConditionalTag = [aDecoder decodeIntForKey:@"leftConditional"];
    self->rightConditionalTag = [aDecoder decodeIntForKey:@"rightConditional"];
    self->topConditionalTag = [aDecoder decodeIntForKey:@"topConditional"];
    self->topPortTag = [aDecoder decodeIntForKey:@"topPortTag"];
    self->bottomPortTag = [aDecoder decodeIntForKey:@"bottomPortTag"];
    
    return self;
}


- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInt:conditionalTag forKey:@"tag"];
    [aCoder encodeCGRect:self.frame forKey:@"frame"];
    [aCoder encodeCGPoint:self.center forKey:@"centerPoint"];
    [aCoder encodeInt:typeOfConditional forKey:@"typeOfConditional"];
    [aCoder encodeInt:conditionalRepeat forKey:@"repeat"];
    //Check for cords
    PatcherSequenceBlock *otherSide = (PatcherSequenceBlock*)[[topPort.connectedCord connectToPortFrom:topPort] parent];
    if(otherSide!=nil) {
        [aCoder encodeInt:otherSide.sequenceTag forKey:@"topPortTag"];
    } else {
        [aCoder encodeInt:-1 forKey:@"topPortTag"];
    }
    
    PatcherSequenceBlock *bottomSide = (PatcherSequenceBlock*)[[bottomPort.connectedCord connectToPortFrom:bottomPort] parent];
    if(bottomSide!=nil) {
        [aCoder encodeInt:bottomSide.sequenceTag forKey:@"bottomPortTag"];
    } else {
        [aCoder encodeInt:-1 forKey:@"bottomPortTag"];
    }
    //Check for friends
    
    if(leftConditional!=nil) {
        [aCoder encodeInt:leftConditional.conditionalTag forKey:@"leftConditional"];
    } else {
        [aCoder encodeInt:-1 forKey:@"leftConditional"];
    }
    
    if(rightConditional!=nil) {
        [aCoder encodeInt:rightConditional.conditionalTag forKey:@"rightConditional"];
    } else {
        [aCoder encodeInt:-1 forKey:@"rightConditional"];
    }
    

    if(bottomConditional!=nil) {
        [aCoder encodeInt:bottomConditional.conditionalTag forKey:@"bottomConditional"];
    } else {
        [aCoder encodeInt:-1 forKey:@"bottomConditional"];
    }
    
    
    if(topConditional!=nil) {
        [aCoder encodeInt:topConditional.conditionalTag forKey:@"topConditional"];
    } else {
        [aCoder encodeInt:-1 forKey:@"topConditional"];
    }
}


- (void) setDelegate:(UIView<ConditionalBlockDelegate,PatcherPortDelegate>*)delegatee {
    delegate = delegatee;
    topPort = [[PatcherPortView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    topPort.center = CGPointMake(self.frame.size.width/2, topPort.frame.size.height/2);
    topPort.backgroundColor = [UIColor clearColor];
    topPort.delegate = delegate;
    topPort.parent = self;
    topPort.portLocation = KMDPatcherPort_TopPort;
    [self addSubview:topPort];
    
    bottomPort = [[PatcherPortView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    bottomPort.center = CGPointMake(self.frame.size.width/2, self.frame.size.height - bottomPort.frame.size.height/2);
    bottomPort.backgroundColor = [UIColor clearColor];
    bottomPort.delegate = delegate;
    bottomPort.parent = self;
    bottomPort.portLocation = KMDPatcherPort_BottomPort;
    [self addSubview:bottomPort];
    
    leftPort = [[PatcherPortView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    leftPort.center = CGPointMake(15, self.frame.size.height/2);
    leftPort.backgroundColor = [UIColor clearColor];
    leftPort.delegate = delegate;
    leftPort.parent = self;
    leftPort.portLocation = KMDPatcherPort_LeftPort;
    [self addSubview:leftPort];
    
    
    rightPort = [[PatcherPortView alloc] initWithFrame:CGRectMake(self.frame.size.width-30, 15, 30, 30)];
    rightPort.center = CGPointMake(self.frame.size.width-15, self.frame.size.height/2);
    rightPort.backgroundColor = [UIColor clearColor];
    rightPort.delegate = delegate;
    rightPort.parent = self;
    rightPort.portLocation = KMDPatcherPort_RightPort;
    [self addSubview:rightPort];
    
    
    
}

- (void) animateIn {
    self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{self.transform = CGAffineTransformMakeScale(1.0, 1.0);self.alpha = 1;} completion:^(BOOL finished){}];
}

#pragma mark Touches
#pragma mark -

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //NSLog(@"SELF: %@", self);
    
    //NSLog(@"TOP: %@", topConditional);
    //NSLog(@"RIGHT: %@", rightConditional);
    //NSLog(@"BOTTOM: %@", bottomConditional);
    //NSLog(@"LEFT: %@", leftConditional);
    UITouch *thisTouch = [touches anyObject];
    CGPoint thisPoint = [thisTouch locationInView:self.superview];
    [self becomeFirstResponder];
    [delegate conditional:self WasSelectedAtPoint:thisPoint];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *thisTouch = [touches anyObject];
    CGPoint thisPoint = [thisTouch locationInView:self.superview];

    [topPort superviewIsMoving];
    [bottomPort superviewIsMoving];

    self.center = thisPoint;
    [delegate conditional:self WasMovedToPoint:thisPoint];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *thisTouch = [touches anyObject];
    CGPoint thisPoint = [thisTouch locationInView:self.superview];
    
    [topPort superviewIsMoving];
    [bottomPort superviewIsMoving];
    
    //self.center = thisPoint;
    [delegate conditional:self WasDroppedAtPoint:thisPoint];
}


#pragma mark Patcher Port Delegate
#pragma mark -


- (void) patcherPortWasSelected:(PatcherPortView *)port {
    
}

- (void) patcherPortIsSelected:(PatcherPortView *)port andCordMovedToPoint:(CGPoint)newPoint {
    
}

- (void) patcherPortWasDropped:(PatcherPortView *)port andCordIsAt:(CGPoint)newPoint {
    
}

#pragma mark -
#pragma mark Gesture Recognizer Control


- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    
    [self shakeFrom:self];

}

- (void) shakeFrom:(PatcherConditionalBlock *)from {
    if(self.typeOfConditional == ConditionalType_ShakeGesture) {
        self.completed = YES;
    }
    
    if(from != leftConditional && leftConditional.typeOfConditional == ConditionalType_ShakeGesture) {
        self.leftConditional.completed = YES;
        
    }
    
    if(from != rightConditional && rightConditional.typeOfConditional == ConditionalType_ShakeGesture) {
        self.rightConditional.completed = YES;
    }
    
    if(topConditional.typeOfConditional == ConditionalType_ShakeGesture)
        topConditional.completed = YES;
    
    if(topConditional.topConditional.typeOfConditional == ConditionalType_ShakeGesture) {
        topConditional.topConditional.completed = YES;
    }
    
    [bottomConditional shakeFrom:self];
    [leftConditional.bottomConditional shakeFrom:leftConditional];
    [rightConditional.bottomConditional shakeFrom:rightConditional];
}

-(void)handleLongPress:(UILongPressGestureRecognizer*)recognizer
{
        if(self.typeOfConditional!= ConditionalType_TapAndHold || !self.isFirstResponder) {
        return;
        }
    
    self.completed = YES;
}

- (void) singleTap:(UITapGestureRecognizer*)recognizer {
    

    if(self.typeOfConditional!=ConditionalType_SingleTap || !self.isFirstResponder) {
        return;
    }
    self.completed = YES;
}

- (void) doubleTap:(UITapGestureRecognizer*)recognizer {
    if(self.typeOfConditional!=ConditionalType_DoubleTap || !self.isFirstResponder) {
        return;
    }
    self.completed = YES;
}

- (void) patcherPortRecievedPulse:(PatcherPortView *)port {
    [self becomeFirstResponder];
    
    if(self.completed || leftConditional.completed || rightConditional.completed) {
        if(self.bottomConditional!=nil) {
            
        } else {
        }
        
    }
    
    NSLog(@"Conditional became first responder");
}

- (BOOL) canBecomeFirstResponder {
    return YES;
}


    
- (void) setCompleted:(BOOL)complet {
    completed = complet;
    finished = complet;
    (complet)? [self setBackgroundCompleted]:[self setBackgroundWaiting];
    NSLog(@"Completed!");
    
    if(complet==NO) return;
    PatcherConditionalBlock *ANDJunction;
    
    //look for ANDS
    bool fin = NO;

    if(bottomConditional != nil || topConditional != nil) ANDJunction = self;
    
    if(leftConditional.bottomConditional != nil || leftConditional.topConditional != nil)
        ANDJunction = leftConditional;
    
    if(rightConditional.bottomConditional != nil || rightConditional.topConditional != nil)
        ANDJunction = rightConditional;
    
    if(ANDJunction==nil) {
        fin = complet;
        
        NSLog(@"OR CLOSE");
        
    } else {
        NSLog(@"AND LOGIC");
        if(ANDJunction.bottomConditional!=nil) {
            NSLog(@"Bottom Conditional Exists");
            if(ANDJunction.bottomConditional.completed) {
                NSLog(@"MYself and Bottom are complete: jnc %@", ANDJunction->titleLabel.text);

                fin = YES;
                
            }
        } else if(ANDJunction.topConditional != nil) {
            NSLog(@"Top Conditional Exists");
            
            if(ANDJunction.topConditional.completed) {
                NSLog(@"MYself and Top are complete");
                
                fin = YES;
                
            }
        } else {
            NSLog(@"Not AND completed");
        }
    }
    
    if(fin) {
        //[ANDJunction shouldPulseChord];
        
        [bottomPort.connectedCord pulseFrom:self.bottomPort];
        [bottomConditional.bottomPort.connectedCord pulseFrom:self.bottomConditional.bottomPort];
        [bottomConditional.leftConditional.bottomPort.connectedCord pulseFrom:bottomConditional.leftConditional.bottomPort];
        [bottomConditional.rightConditional.bottomPort.connectedCord pulseFrom:bottomConditional.rightConditional.bottomPort];
        
        [leftConditional.bottomPort.connectedCord pulseFrom:leftConditional.bottomPort];
        [leftConditional.bottomConditional.bottomPort.connectedCord pulseFrom:leftConditional.bottomConditional.bottomPort];
        [rightConditional.bottomPort.connectedCord pulseFrom:rightConditional.bottomPort];
        [rightConditional.bottomConditional.bottomPort.connectedCord pulseFrom:rightConditional.bottomConditional.bottomPort];
        
        
        
        PatcherSequenceBlock *seqBlock = (PatcherSequenceBlock*)[[ANDJunction.topConditional.topPort.connectedCord connectToPortFrom:ANDJunction.topConditional.topPort] parent];
        [seqBlock stop];
        
        seqBlock = (PatcherSequenceBlock*)[[ANDJunction.topConditional.topConditional.topPort.connectedCord connectToPortFrom:ANDJunction.topConditional.topConditional.topPort] parent];
        [seqBlock stop];
        
        seqBlock = (PatcherSequenceBlock*)[[ANDJunction.topConditional.leftConditional.topPort.connectedCord connectToPortFrom:ANDJunction.topConditional.leftConditional.topPort] parent];
        [seqBlock stop];
        
        seqBlock = (PatcherSequenceBlock*)[[ANDJunction.topConditional.rightConditional.topPort.connectedCord connectToPortFrom:ANDJunction.topConditional.rightConditional.topPort] parent];
        [seqBlock stop];
        
        seqBlock = (PatcherSequenceBlock*)[[topPort.connectedCord connectToPortFrom:topPort] parent];
        [seqBlock stop];
        
        seqBlock = (PatcherSequenceBlock*)[[rightConditional.topPort.connectedCord connectToPortFrom:rightConditional.topPort] parent];
        [seqBlock stop];
        
        seqBlock = (PatcherSequenceBlock*)[[leftConditional.topPort.connectedCord connectToPortFrom:leftConditional.topPort] parent];
        [seqBlock stop];
        
    }
}


- (BOOL) completed {
    if(self.typeOfConditional == ConditionalType_Repeats) {
        if(connectedRepeat>=conditionalRepeat) {
            return YES;
        } else 
            return  NO;
    } else {
        if(leftConditional.finished || leftConditional.leftConditional.finished || rightConditional.finished || rightConditional.rightConditional.finished) {
            return YES;
        } else {
            return completed;
        }
    }
    
}

- (void) iterateRepeat: (int) value {
    currentRepeat = value;
    if(conditionalRepeat<=currentRepeat && self.typeOfConditional==ConditionalType_Repeats && !self.completed) {
        self.completed = YES;
    }
    
    [bottomConditional iterateRepeat:value];
}


- (void) numberInRow {
    int number = 1;
    
    PatcherConditionalBlock *leftCompare = self.leftConditional, *rightCompare = self.rightConditional;
    while(leftCompare.leftConditional!=nil) {
        number++;
        leftCompare=leftCompare.leftConditional;
    }
    
    while(rightCompare.rightConditional!=nil) {
        number++;
        rightCompare=rightCompare.rightConditional;
    }
    
}

- (void) notifyOthersOfRepeatsFrom:(PatcherConditionalBlock *)origin {
    
}

- (void) removeFromSuperview {
    [super removeFromSuperview];
    
    [topPort.connectedCord removeFromSuperview];
    [bottomPort.connectedCord removeFromSuperview];
}

- (BOOL) horizontalCheckFrom:(PatcherConditionalBlock *)origin {
    BOOL comp = NO;
    
    
    
    return comp;
}

- (void) shouldPulseChord {
    if(alreadyPulsed) {
        NSLog(@"alrad");
    }
    if(!alreadyPulsed) {
        NSLog(@"PULSED");
        [self.bottomPort.connectedCord pulseFrom:self.bottomPort];
        [self.leftConditional shouldPulseChord];
        [self.rightConditional shouldPulseChord];
        [self.bottomConditional shouldPulseChord];
        
        alreadyPulsed = YES;
    }
}

- (void) reset {
    alreadyPulsed = NO;
    
    self.completed = NO;
    self.finished = NO;
    
    leftConditional.completed = NO;
    leftConditional.finished = NO;
    leftConditional.leftConditional.completed = NO;
    leftConditional.leftConditional.finished = NO;
    
    rightConditional.completed = NO;
    rightConditional.finished = NO;
    rightConditional.rightConditional.completed = NO;
    rightConditional.rightConditional.finished = NO;
    
    [self.bottomConditional reset];
    [leftConditional.bottomConditional reset];
    [leftConditional.leftConditional.bottomConditional reset];
    [rightConditional.bottomConditional reset];
    [rightConditional.rightConditional.bottomConditional reset];

}

- (void) resolvePortsAndConditionals {
    if(leftConditionalTag!=-1 && leftConditional == nil) {
        self.leftConditional = [delegate conditionalForTagNumber:leftConditionalTag];
        self.leftConditional.rightConditional = self;
        leftConditionalTag=-1;
        [self.leftConditional resolvePortsAndConditionals];
    }
    
    if(rightConditionalTag!=-1 && rightConditional == nil) {
        self.rightConditional = [delegate conditionalForTagNumber:rightConditionalTag];
        self.rightConditional.leftConditional = self;
        rightConditionalTag=-1;
        [self.rightConditional resolvePortsAndConditionals];
    }
    
    if(topConditionalTag!=-1 && topConditional == nil) {
        self.topConditional = [delegate conditionalForTagNumber:topConditionalTag];
        self.topConditional.bottomConditional = self;
        topConditionalTag=-1;
        [self.topConditional resolvePortsAndConditionals];
    }
    
    if(bottomConditionalTag!=-1 && bottomConditional == nil) {
        self.bottomConditional = [delegate conditionalForTagNumber:bottomConditionalTag];
        self.bottomConditional.topConditional = self;
        bottomConditionalTag=-1;
        [self.bottomConditional resolvePortsAndConditionals];
    }
    
    if(bottomPortTag!=-1) {
        PatcherCord *newCord = [[PatcherCord alloc] initWithFrame:[delegate bounds] fromPort:bottomPort];
        newCord.toPort = [delegate sequenceForTagNumber:bottomPortTag].topPort;
        [delegate addSubview:newCord];
        newCord.toPort.connectedCord = newCord;
        newCord.fromPort.connectedCord = newCord;
        bottomPortTag=-1;
    }
    
    if(topPortTag!=-1) {
        PatcherCord *newCord = [[PatcherCord alloc] initWithFrame:[delegate bounds] fromPort:[delegate sequenceForTagNumber:topPortTag].bottomPort];
        NSLog(@"TopPortTag: %d", topPortTag);
        newCord.toPort = topPort;
        newCord.toPort.connectedCord = newCord;
        newCord.fromPort.connectedCord = newCord;
        
        [delegate addSubview:newCord];
        topPortTag = -1;
    }
}

- (void) setBackgroundCompleted {
    self.alpha = 0.5;
}

- (void) setBackgroundWaiting {
    self.alpha = 1;
}

- (void) setLeftConditional:(PatcherConditionalBlock *)left {
    leftConditional = left;
    leftConditionalTag = left.conditionalTag;
    
    if(left==nil) {
        leftConditionalTag = -1;
        [leftLink removeFromSuperview];
        leftLink = nil;
    } else if(leftLink==nil) {
        leftLink = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Link.png"]];
        leftLink.center = CGPointMake(0, self.bounds.size.height/2);
        [self addSubview:leftLink];
    }
    
    if(left==self) {
        leftConditionalTag = -1;
        leftConditional = nil;
    }
}


- (void) setRightConditional:(PatcherConditionalBlock *)left {
    rightConditional = left;
    rightConditionalTag = left.conditionalTag;
    
    
    if(left==nil) {
        rightConditionalTag = -1;
        [rightLink removeFromSuperview];
        rightLink = nil;
    } else if(rightLink==nil) {
        rightLink = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Link.png"]];
        rightLink.center = CGPointMake(self.bounds.size.width, self.bounds.size.height/2);
        [self addSubview:rightLink];
        
    }
    
    if(left==self) {
        rightConditionalTag = -1;
        rightConditional = nil;
    }
}


- (void) setTopConditional:(PatcherConditionalBlock *)left {
    topConditional = left;
    topConditionalTag = left.conditionalTag;
    
    
    
    
    if(left==nil) {
        NSLog(@"REMaOVED top");

        [topLink removeFromSuperview];
        topLink = nil;
        topConditionalTag = -1;
    } else if(topLink==nil) {
        NSLog(@"ADDED Top");
        topLink = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Link.png"]];
        topLink.center = CGPointMake(self.bounds.size.width/2, 0);
        topLink.transform = CGAffineTransformMakeRotation(degreesToRadians(90));
        [self addSubview:topLink];
    }
    
    if(left==self) {
        topConditionalTag = -1;
        topConditional = nil;
    }
}


- (void) setBottomConditional:(PatcherConditionalBlock *)left {
    bottomConditional = left;
    bottomConditionalTag = left.conditionalTag;
    
    if(left==nil) {
        NSLog(@"Removed Bottom");

        [bottomLink removeFromSuperview];
        bottomLink = nil;
        bottomConditionalTag = -1;
    }   else if(bottomLink==nil) {
        NSLog(@"Added Bottom");
        bottomLink = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Link.png"]];
        bottomLink.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height);
        bottomLink.transform = CGAffineTransformMakeRotation(degreesToRadians(90));
        [self addSubview:bottomLink];
        
    }
    
    if(left==self) {
        bottomConditionalTag = -1;
        bottomConditional = nil;
    }
}


@end
