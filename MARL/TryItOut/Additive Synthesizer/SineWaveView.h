//
//  SineWaveView.h
//  MARL
//
//  Created by Kevin Murphy on 9/9/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SineWaveView : UIView
@property (nonatomic, readwrite) float frequency;
@property (nonatomic, readwrite) float periodOfFrame;
- (id) initWithFrame:(CGRect)frame andFrequeny:(float) freq;

@end
