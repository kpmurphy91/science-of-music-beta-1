//
//  PatcherPortView.h
//  MARL
//
//  Created by Kevin Murphy on 8/24/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PatcherCord.h"
@class PatcherPortView;

typedef enum {
    KMDPatcherPort_TopPort,
    KMDPatcherPort_BottomPort,
    KMDPatcherPort_LeftPort,
    KMDPatcherPort_RightPort
} KMDPatcherPort_PortLocation;

@protocol PatcherPortDelegate <NSObject>
- (void) patcherPortWasSelected: (PatcherPortView*) port;
- (void) patcherPortIsSelected: (PatcherPortView*) port andCordMovedToPoint: (CGPoint) newPoint;
- (void) patcherPortWasDropped: (PatcherPortView*) port andCordIsAt:(CGPoint) newPoint;
@optional
- (void) patcherPortRecievedPulse: (PatcherPortView*) port;
@end

@interface PatcherPortView : UIView
@property (nonatomic,readwrite) KMDPatcherPort_PortLocation portLocation;
@property (nonatomic,readwrite) UIView<PatcherPortDelegate> *parent;
@property (nonatomic) id<PatcherPortDelegate> delegate;
@property (nonatomic) PatcherCord *connectedCord;

- (void) superviewIsMoving;
- (void) recievePulse;
- (void) recieveFirstResponderPulse;

@end