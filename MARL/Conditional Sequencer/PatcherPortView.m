//
//  PatcherPortView.m
//  MARL
//
//  Created by Kevin Murphy on 8/24/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "PatcherPortView.h"

@implementation PatcherPortView
@synthesize parent, delegate, portLocation, connectedCord;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setDelegate:(id<PatcherPortDelegate>)delegatee {
    delegate = delegatee;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //NSLog(@"sent To Delegate %@", [delegate description]);
    [delegate patcherPortWasSelected:self];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *thisTouch = [touches anyObject];
    CGPoint pointInPatcherView = [thisTouch locationInView:self.superview.superview];
    
    [delegate patcherPortIsSelected:self andCordMovedToPoint:pointInPatcherView];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *thisTouch = [touches anyObject];
    CGPoint pointInPatcherView = [thisTouch locationInView:self.superview.superview];
    [delegate patcherPortWasDropped:self andCordIsAt:pointInPatcherView];
}

- (void) superviewIsMoving {
    [self.connectedCord portMoved:self];
}

- (void) recievePulse {
    [parent patcherPortRecievedPulse:self];
}

- (void) recieveFirstResponderPulse {
    [(UIView*)parent becomeFirstResponder];
}

@end
