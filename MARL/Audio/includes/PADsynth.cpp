/*
    PADsynth implementation as ready-to-use C++ class.
    By: Nasca O. Paul, Tg. Mures, Romania
    This implementation and the algorithm are released under Public Domain
    Feel free to use it into your projects or your products ;-)

    This implementation is tested under GCC/Linux, but it's 
    very easy to port to other compiler/OS. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "PADsynth.h"
#include <Accelerate/Accelerate.h>


PADsynth::PADsynth(int N_, int samplerate_, int number_harmonics_){
    N=N_;
    samplerate=samplerate_;
    number_harmonics=number_harmonics_;
    A=new REALTYPE [number_harmonics];
    freq_values = new REALTYPE [number_harmonics_];
    for (int i=0;i<number_harmonics;i++) A[i]=0.0;
    A[0]=1.0;//default, the first harmonic has the amplitude 1.0

    freq_amp=new REALTYPE[N/2];
    
};

PADsynth::~PADsynth(){
    delete[] A;
    delete[] freq_values;
    delete[] freq_amp;
};

REALTYPE PADsynth::relF(int N){
    return N;
};

void PADsynth::setharmonic(int n,REALTYPE value, REALTYPE frequencyy){
    if ((n<0)||(n>=number_harmonics)) return;
    A[n]=value;
    freq_values[n] = frequencyy;
};

REALTYPE PADsynth::getharmonic(int n){
    if ((n<0)||(n>number_harmonics)) return 0.0;
    return A[n];
};

REALTYPE PADsynth::getharmonicFreq(int n) {
    if ((n<0)||(n>number_harmonics)) return 0.0;
    return freq_values[n];
}

REALTYPE PADsynth::profile(REALTYPE fi, REALTYPE bwi){
    REALTYPE x=fi/bwi;
    x*=x;
    //if (x>14712.80603) return 0.0;//this avoids computing the e^(-x^2) where it's results are very close to zero
    return exp(-x)/bwi;
};

int binForHz(float Hz, int length, float sampleRate) {    
    float thisFloat = (Hz/sampleRate)*length + 0.5;
    return (int) thisFloat;
}

//HOW TO CONVERT BIN TO FREQUENCY BIN/length multiplied by the samplerate (81.7/8192 * 44100)

void PADsynth::synth(REALTYPE f,REALTYPE bw,REALTYPE bwscale,REALTYPE *smp){
    int i,nh;
    for (i=0;i<N/2;i++) freq_amp[i]=0.0;//default, all the frequency amplitudes are zero

    //freq_amp[binForHz(f, N, 44100)] = 1;
    
    //printf("fundamental: %f\n", f);

    
    for (nh=0;nh<number_harmonics;nh++){//for each harmonic
        REALTYPE bw_Hz;//bandwidth of the current harmonic measured in Hz
        REALTYPE bwi;
        REALTYPE fi;
        REALTYPE rF=f*relF(nh);

        bw_Hz=(pow(2.0,bw/1200.0)-1)*f*pow(relF(nh),bwscale);
	
        bwi=bw_Hz/(2.0*samplerate);
        fi=rF/samplerate;  //period
        //printf("harmonic[%d]: %f\n", nh, getharmonicFreq(nh));

        freq_amp[binForHz(freq_values[nh]*f, N, 44100)] = A[nh];

        for (i=0;i<N/2;i++){//here you can optimize, by avoiding to compute the profile for the full frequency (usually it's zero or very close to zero)
        };
    };
    

    REALTYPE *freq_real=new REALTYPE[N/2];
    REALTYPE *freq_imaginary=new REALTYPE[N/2];

    //Convert the freq_amp array to complex array (real/imaginary) by making the phases random
    for (i=0;i<N/2;i++){
        REALTYPE phase=RND()*2.0*3.14159265358979;
        freq_real[i]=freq_amp[i]*cos(phase);
        freq_imaginary[i]=freq_amp[i]*sin(phase);
    };
    IFFT(freq_real,freq_imaginary,smp);
    delete [] freq_real;
    delete [] freq_imaginary;

    //normalize the output
    REALTYPE max=0.0;
    for (i=0;i<N;i++) if (fabs(smp[i])>max) max=fabs(smp[i]);
    if (max<1e-5) max=1e-5;
    for (i=0;i<N;i++) smp[i]/=max*1.4142;
    
};

REALTYPE PADsynth::RND(){
    return (rand()/(RAND_MAX+1.0));
};

void PADsynth::IFFT(REALTYPE *freq_real,REALTYPE *freq_imaginary,REALTYPE *smp) {
    for(int i = 0; i<N; i++) {
        //printf("freq_real: %f\n", freq_real[i]);
    }
    vDSP_Length log2n = log2f(N);
    FFTSetup fftSetup = vDSP_create_fftsetup(log2n, FFT_RADIX2);
    COMPLEX_SPLIT A;
    A.imagp = freq_imaginary;
    A.realp = freq_real;
    vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_INVERSE);
    vDSP_ztoc(&A, 1, (COMPLEX *) smp, 2, N/2);
}



