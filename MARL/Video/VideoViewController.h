//
//  VideoViewController.h
//  MARL
//
//  Created by Kevin Patrick Murphy on 7/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Video.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideoViewController : UIViewController
{
    MPMoviePlayerController *videoPlayer;
    UIView *playerWindow;
    UIScrollView *whatsThisContainer;
    UILabel *titleLabel, *shortDescription;
}
@property (nonatomic, readwrite) Video *thisVideo;

+ (VideoViewController*) videoAndExplanationFromVideoObject:(Video *)videoObject;

@end
