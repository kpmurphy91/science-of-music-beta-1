//
//  ADSRViewController.h
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioController.h"
#import "ADSRView.h"
#import "KMDKeyBoard.h"
#import "SynthDefs.h"
#import "HelpViewController.h"

@interface ADSRViewController : UIViewController<AudioManagerDelegate, AudioEnvelopeDelegate, ADSRViewDelegate, KMDKeyboardDelegate>
{
    ADSRView *graphView;
    KMDKeyBoard *keyboardView;
}
@property (nonatomic, readwrite) SynthKeyState keyState;
@end
