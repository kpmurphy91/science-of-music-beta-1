//
//  HomeViewController.h
//  MARL
//
//  Created by Kevin Murphy on 6/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADSRView.h"
#import "TryItOutViewController.h"
#import "ConditionalSequenceViewController.h"

@interface HomeViewController : UIViewController <ADSRViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    ADSRView *graphView;
    
    UIButton *presentTryItOut, *presentMakeMusic, *videos;
    UIButton *presentAbout;
    UITableView *choicesTable;
    
    UIImageView *backGround;
}

@end
