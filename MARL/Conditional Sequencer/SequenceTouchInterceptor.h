//
//  SequenceTouchInterceptor.h
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TouchInterceptorDelegate <NSObject>
- (void) startedInterceptingTouchInRow:(int)row andColumn:(int) col;
- (void) droppedTouch;
- (void) interceptedTouchMovedInRow:(int)row andColumn:(int)col;
@end

@interface SequenceTouchInterceptor : UIView
{
    int currentRow, currentCol;
}
@property (nonatomic) BOOL ooooThatTickles;
@property (nonatomic) id<TouchInterceptorDelegate> delegate;
@property (nonatomic) int rows, columns;

- (void) generateTouchGridWithRows:(int) rowCount andColumns: (int) columnCount;
- (float) getButtonHeight;
- (float) getButtonWidth;


@end
