//
//  FFTGraphViewController.h
//  MARL
//
//  Created by Kevin Murphy on 7/5/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioController.h"
#import "Grapher.h"
#import "FFTControl.h"
#import "TPCircularBuffer.h"
#import "HelpViewController.h"

@interface FFTGraphViewController : UIViewController <AudioManagerDelegate>
{
    FFTControl *spectrumAnalyzer;
    Grapher *spectrumGraphView;
    
    BOOL working;
    
    float *newSamples;
    int lengthOfSamples;
    
    TPCircularBuffer buffer;
    
    int windowSize;
    
    UIBarButtonItem *logButton, *linButton;
}
@property (nonatomic, readwrite) float *logFreqMatrix;
@property (nonatomic, readwrite) BOOL mode;
- (void) setPoints;
@end
