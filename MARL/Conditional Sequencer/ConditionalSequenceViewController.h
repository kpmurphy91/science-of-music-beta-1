//
//  ConditionalSequenceViewController.h
//  MARL
//
//  Created by Kevin Murphy on 9/12/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditSequencerView.h"
#import "PatcherView.h"
#import "CreateConditionalViewController.h"
#import "CreateSequenceViewController.h"
#import "SavedConditionalSequencesPopViewController.h"
#import "AddBlockPopoverViewController.h"
#import "HelpViewController.h"


@interface ConditionalSequenceViewController : UIViewController < CreateSequenceDelegate, CreateConditionalDelegate, PatcherViewDelegate, UIAlertViewDelegate, OpenSequencePopOver, UITextFieldDelegate>
{
    UIView *flipContainer, *currentOnTop, *currentOnBottom;
    PatcherView *patcher;
    UIPopoverController *conditionalPopover, *sequencePopover, *addControlPopover;
    Sequence *saveSequence;
    EditSequencerView *newEditSequencerView;
    
    NSString *choseFile;
    
    UILabel *titleLabel;
    
    UITextField *firstResp;
    UIAlertView *firstRespAlertView;
    
    UIBarButtonItem *deleteBarButton;
    UIColor *prevTintColor;
}
@property (nonatomic, readwrite) UIPopoverController *conditionlPopover, *sequencePopover, *savedPopover;

- (void) openSaved: (NSString*) nam;
@end
