//
//  PatcherCord.m
//  MARL
//
//  Created by Kevin Murphy on 8/24/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "PatcherCord.h"
#import "PatcherPortView.h"
#import "PatcherConditionalBlock.h"
#import "PatcherSequenceBlock.h"

@implementation PatcherCord
@synthesize fromPort, toPort, pointA, pointB, movingPoint;

- (id) initWithFrame:(CGRect)frame fromPort:(PatcherPortView *)from toPort:(PatcherPortView*) to {
    self = [self initWithFrame:frame fromPort:from];
    self.toPort = to;
    
    return self;
}

- (id)initWithFrame:(CGRect)frame fromPort:(PatcherPortView *)from
{

    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        fromPort = from;
        pathOfCord = CGPathCreateMutable();
        CGPathMoveToPoint(pathOfCord, NULL, from.center.x, from.center.y);
        
        //NSLog(@"%@", from);
        
        pointA = CGPointMake(from.center.x+from.superview.frame.origin.x, from.center.y+from.superview.frame.origin.y);
        
        
        self.clearsContextBeforeDrawing = YES;
        // Initialization code
        
        thisPath = [[UIBezierPath alloc] init];
        [thisPath moveToPoint:pointA];
        
        int difference;
        if(from.portLocation == KMDPatcherPort_TopPort) {
            difference = 20;
        } else {
            difference = -20;
        }
        
        [thisPath addLineToPoint:CGPointMake(pointA.x, pointA.y+difference)];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context    = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [UIColor greenColor].CGColor);

    // Draw them with a 2.0 stroke width so they are a bit more visible.
    CGContextSetLineWidth(context, 2.0);
    CGContextAddPath(context, thisPath.CGPath);
    
    
    if(temporaryPoint.x == 0 && temporaryPoint.y == 0) {
        
    } else {
    // and now draw the Path!
        CGContextStrokePath(context);
    }
}

- (void) setPointA:(CGPoint)pointAa {
    pointA = pointAa;
    [self setNeedsDisplay];
}

- (void) setPointB:(CGPoint)pointBb {
    pointB = pointBb;
    [self setNeedsDisplay];
}


- (void) setToPort:(PatcherPortView *)toPortt {
    if(toPortt.connectedCord != nil) return;
    toPort = toPortt;
    if(toPortt!=nil) {
        pointB = toPortt.center;
        [self moveToPoint:CGPointMake(toPortt.center.x + toPortt.superview.frame.origin.x, toPortt.center.y + toPortt.superview.frame.origin.y)];
    }
    
    [self setNeedsDisplay];
}

- (void) moveToPoint:(CGPoint)newPoint {
    temporaryPoint = newPoint;
    
    pointB = newPoint;
    
    [thisPath removeAllPoints];
    [thisPath moveToPoint:pointA];
    
    float distance = sqrtf((pointA.x-pointB.x)*(pointA.x-pointB.x)+(pointA.y-pointB.y)*(pointA.y-pointB.y));
    float distanceY = fabsf(pointA.y-pointB.y);
    float distanceX = fabsf(pointA.x-pointB.x)-100;
    
    //if(distanceY<0) distanceY = 0;
    //if(distanceX<0) distanceX = 0;
    
    float differenceY;
    float differenceX = distanceX*0.4;
    if(fromPort.portLocation == KMDPatcherPort_BottomPort) {
        differenceY = 100+distanceY*0.4+distanceX*0.33;
    } else {
        differenceY = -100-distanceY*0.4-distanceX*0.33;
    }
    
    
    
    if((pointA.x-pointB.x)<0) {
        //A then B
        [thisPath addCurveToPoint:newPoint controlPoint1:CGPointMake(pointA.x+differenceX, pointA.y+differenceY) controlPoint2:CGPointMake(newPoint.x-differenceX, newPoint.y-differenceY)];
    } else {
        
        [thisPath addCurveToPoint:newPoint controlPoint1:CGPointMake(pointA.x-differenceX, pointA.y+differenceY) controlPoint2:CGPointMake(newPoint.x+differenceX, newPoint.y-differenceY)];
    }
    
    
    [self setNeedsDisplay];
}

- (void) snapToPort:(PatcherPortView *)destinationPort {
    self.toPort = destinationPort;
}


- (CGRect) translateFrame:(CGRect) childFrame intoParentFrame:(CGRect) parentFrame {
    CGRect result = CGRectMake(parentFrame.origin.x+childFrame.origin.x, parentFrame.origin.y+childFrame.origin.y, childFrame.size.width, childFrame.size.height);
    return result;
}

- (CGPoint) translatePoint:(CGPoint) childPoint intoSuperview: (CGRect) parentFrame {
    return CGPointMake(childPoint.x+parentFrame.origin.x, childPoint.y+parentFrame.origin.y);
}

- (void) portMoved:(PatcherPortView *)movingPort {
    if(movingPort==self.fromPort) {
        pointA = [self translatePoint:movingPort.center intoSuperview:movingPort.superview.frame];
        [self moveToPoint:pointB];
    } else {
        pointB = [self translatePoint:movingPort.center intoSuperview:movingPort.superview.frame];
        [self moveToPoint:pointB];
    }
    [self setNeedsDisplay];
}

- (PatcherPortView*) connectToPortFrom:(PatcherPortView *)originatingFromPort {
    if(self.toPort == originatingFromPort) {
        return self.fromPort;
    } else {
        return self.toPort;
    }
}

- (void) pulseFrom:(PatcherPortView *)originatingPort {
    if(self.toPort == originatingPort) {
        [self.fromPort recievePulse];
    } else {
        [self.toPort recievePulse];
    }
}

- (void) firstResponderPulse:(PatcherPortView *)originatingPort {
    if(self.toPort == originatingPort) {
        if([self.fromPort.parent isKindOfClass:[PatcherConditionalBlock class]])
            [self.fromPort becomeFirstResponder];
    } else {
        if([self.toPort.parent isKindOfClass:[PatcherConditionalBlock class]])
            [self.toPort becomeFirstResponder];
    }
}

- (void) selfDestruct {
    toPort.connectedCord = nil;
    fromPort.connectedCord = nil;
    [self removeFromSuperview];
}

@end
