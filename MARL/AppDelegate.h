//
//  AppDelegate.h
//  MARL
//
//  Created by Kevin Murphy on 6/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioController.h"
#import "PitchDetector.h"
#import "FFTControl.h"

@class HomeViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, PitchDetectorDelegate>
{
    PitchDetector *temp;
    FFTControl *newFFT;
}
@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) AudioController *audioManager;

@end
