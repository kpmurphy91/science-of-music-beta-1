//
//  AddBlockPopoverViewController.h
//  MARL
//
//  Created by Kevin Murphy on 10/8/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateConditionalViewController.h"
#import "CreateSequenceViewController.h"

@interface AddBlockPopoverViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *thisTable;
}
@property (nonatomic, readwrite) id<CreateConditionalDelegate, CreateSequenceDelegate> delegate;
@end
