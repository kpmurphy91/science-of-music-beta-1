//
//  TouchGrid.h
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "SequenceTouchInterceptor.h"

@interface TouchGrid : UIView <TouchInterceptorDelegate>
{
    SequenceTouchInterceptor *touchInterceptor;
    NSMutableArray *beatContentArray;
    NSMutableArray *rowToFrequencyArray;
}
@property (readonly) NSMutableArray *beatContentArray, *rowToFrequencyArray;
@property (nonatomic, readwrite) float highLightDecayInterval;
@property (nonatomic) int columns;
@property (nonatomic) int rows;
- (void) reloadButtonsWithArray:(NSArray*) beatsArray;
- (void) clearSelection;
- (void) animateColumn:(int) col;
@end
