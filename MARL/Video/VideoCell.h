//
//  VideoCell.h
//  MARL
//
//  Created by Kevin Patrick Murphy on 7/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Video.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideoCell : UITableViewCell
{
    UIImageView *thumbImageView;
    MPMoviePlayerController *videoPlayer;
}
@property (nonatomic, readwrite) UIImage *thumb;
@property (nonatomic, readwrite) Video *thisCellsVideo;
@property (nonatomic, readwrite) UILabel *titleLabel, *descriptionLabel, *durationLabel;


@end
