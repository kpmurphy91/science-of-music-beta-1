//
//  PatcherSequenceBlock.h
//  MARL
//
//  Created by Kevin Murphy on 8/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PatcherPortView.h"
#import "Sequence.h"

@class PatcherSequenceBlock;

@protocol SequenceBlockDelegate <NSObject>
    - (void) sequenceWasTouched:(PatcherSequenceBlock*) aSequenceBlock;
    - (void) sequenceWantsToEdit: (PatcherSequenceBlock*) aSequenceBlock;
    - (void) stopAllSequences;
@end


@interface PatcherSequenceBlock : UIView <PatcherPortDelegate, UIGestureRecognizerDelegate, SequenceRepeatDelegate>
{
    UIView *intersectingView;
    int repeats;
    UITapGestureRecognizer *singleTap, *doubleTap;
    UILabel *sequenceLabel;
}

@property (nonatomic, readwrite) int sequenceTag;
@property (nonatomic, readwrite) PatcherPortView *topPort, *bottomPort;
@property (nonatomic, readwrite) id<SequenceBlockDelegate, PatcherPortDelegate> delegate;
@property (nonatomic, readwrite) Sequence *thisSequence;
@property (nonatomic, readwrite) BOOL isPlaying;
@property (nonatomic, readwrite) int repeats;

- (id) initWithCenter: (CGPoint) centerPoint;
- (void) animateIn;
- (void) motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event;
- (BOOL) canBecomeFirstResponder;
- (void) playSequence;
- (void) stop;
@end



