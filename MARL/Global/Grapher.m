//
//  Grapher.m
//  Science of Music
//
//  Created by Kevin Murphy on 6/7/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "Grapher.h"


@implementation Grapher
@synthesize  shouldFill, lineColor;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        lineColor = [UIColor colorWithWhite:1 alpha:0.7];
    }
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}



- (void) setPoints: (float*) puntos withXorNil:(float *)xPuntos numPuntos: (int) numOfPoints
{
    //printf("hey1");

    int n = numOfPoints;
    CGPoint *points = (CGPoint *)malloc(sizeof(CGPoint)*n);
    
    if(xPuntos==nil) {
        for (int i =0; i<n; i++) {
            points[i] = CGPointMake(((float)i/n)*self.frame.size.width, 30*log(puntos[i]*puntos[i])+10);
        }
    } else {
        
        for (int i =0; i<n; i++) {
            points[i] = CGPointMake (xPuntos[i]*self.frame.size.width, self.frame.origin.y + puntos[i]);
        }
    }
    
    //printf("hey2");

    //CGPathRelease(line);
    line = CGPathCreateMutable();
    
    float height = self.frame.size.height;
    
    CGPathMoveToPoint(line, NULL, 0, height-1);
    for (int i = 0; i<n; i++) {
        //NSLog(@"%@", NSStringFromCGPoint(points[i]));
        CGPathAddLineToPoint(line, NULL, points[i].x, height - (points[i].y));
        //printf("%d - (%f, %f)\n", i, points[i].x, points[i].y);
    }
    
    //printf("hey3");

    //free(points);
    //printf("hey4");

    [self setNeedsDisplay];
    
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //printf("DRAWWWWWWWWW\n\n\n");
    CGContextClearRect(UIGraphicsGetCurrentContext(), self.bounds);
    CGContextRef context = UIGraphicsGetCurrentContext(); 
    CGContextSetStrokeColorWithColor(context, lineColor.CGColor);
    
    CGContextSetLineWidth(context, 2);
    CGContextBeginPath(context);
    CGPathRef thisOne = CGPathCreateCopy(line);
    CGContextAddPath(context, thisOne);
    CGContextStrokePath(context);
    
    if(shouldFill) {
        CGMutablePathRef fillPath = CGPathCreateMutableCopy(thisOne);
        CGContextBeginPath(context);
        CGPathAddLineToPoint(fillPath, NULL, self.frame.size.width, self.frame.size.height);
        CGPathAddLineToPoint(fillPath, NULL, 0,  self.frame.size.height);
        CGContextSetFillColorWithColor(context, lineColor.CGColor);
        CGContextAddPath(context, fillPath);
        CGContextFillPath(context);
        CGPathRelease(fillPath);
    }
    
    CGPathRelease(thisOne);
}


@end
