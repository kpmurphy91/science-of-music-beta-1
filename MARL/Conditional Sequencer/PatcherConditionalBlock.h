//
//  PatcherConditionalBlock.h
//  MARL
//
//  Created by Kevin Murphy on 8/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PatcherPortView.h"
#import "PatcherSequenceBlock.h"

typedef enum {
    ConditionalType_SingleTap,
    ConditionalType_DoubleTap,
    ConditionalType_TapAndHold,
    ConditionalType_ShakeGesture,
    ConditionalType_Repeats,
} ConditionalType;

@class PatcherConditionalBlock;
@protocol ConditionalBlockDelegate <NSObject>

- (void) conditional:(PatcherConditionalBlock*) patcherConditional WasSelectedAtPoint:(CGPoint) pointInFrame;
- (void) conditional:(PatcherConditionalBlock*) patcherConditional WasMovedToPoint:(CGPoint) pointInFrame;
- (void) conditional:(PatcherConditionalBlock*) patcherConditional WasDroppedAtPoint:(CGPoint) pointInFrame;
- (PatcherConditionalBlock*) conditionalForTagNumber:(int) num;
- (PatcherSequenceBlock*) sequenceForTagNumber:(int) num;

- (NSMutableArray*) blocksOnScreen;
- (CGRect) bounds;
@end

@interface PatcherConditionalBlock : UIView <PatcherPortDelegate>
{
    UIView *intersectingView;
    int connectedRepeat;
    int conditionalRepeat;
    
    BOOL alreadyPulsed;

    @public
    UILabel *titleLabel;
    BOOL finished;
    
    int leftConditionalTag, rightConditionalTag, bottomConditionalTag, topConditionalTag, topPortTag, bottomPortTag;
    
    UIImageView *leftLink, *rightLink, *topLink, *bottomLink;
}
@property (nonatomic, readwrite) int currentRepeat;
@property (nonatomic, readwrite) int conditionalTag;
@property (nonatomic, readwrite) BOOL completed, finished;
@property (nonatomic, readwrite) ConditionalType typeOfConditional;
@property (nonatomic, readwrite) PatcherPortView *topPort, *bottomPort, *leftPort, *rightPort;
@property (nonatomic, readwrite) PatcherConditionalBlock *leftConditional, *rightConditional, *topConditional, *bottomConditional;
@property (nonatomic, readwrite) UIView<ConditionalBlockDelegate, PatcherPortDelegate> *delegate;

- (id) initWithCenter: (CGPoint) centerPoint andRepeats:(int) rep;
- (id) initWithCenter: (CGPoint) centerPoint andConditional: (ConditionalType) type;
- (void) resolvePortsAndConditionals;

- (void) animateIn;
- (void) iterateRepeat:(int) value;
- (BOOL) horizontalCheckFrom:(PatcherConditionalBlock*) origin;
- (void) shakeFrom: (PatcherConditionalBlock*) from;
- (void) shouldPulseChord;
- (void) reset;
@end
