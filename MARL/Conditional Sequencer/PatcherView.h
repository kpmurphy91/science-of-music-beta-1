//
//  PatcherView.h
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PatcherConditionalBlock.h"
#import "PatcherSequenceBlock.h"
#import "PatcherPortView.h"
#import "PatcherCord.h"
#import "Sequence.h"
#import <QuartzCore/QuartzCore.h>

@protocol PatcherViewDelegate <NSObject>
- (void) sequenceWantsToEdit:(PatcherSequenceBlock *)aSequenceBlock;
- (UINavigationItem*) navigationItem;
@end

@interface PatcherView : UIView <ConditionalBlockDelegate, PatcherPortDelegate, SequenceBlockDelegate, UIAlertViewDelegate, CAMediaTiming>
{
    PatcherConditionalBlock *newestConditional;
    PatcherCord *newPatcherCord;
    NSMutableArray *blocksOnScreen;
    
    UIView *placeholder;
    
    UIButton *startButton;
}
@property (nonatomic, readwrite) NSString *title;
@property (nonatomic, readonly) BOOL isPlaying;
@property (nonatomic, readwrite) NSMutableArray *blocksOnScreen;
@property (nonatomic, readwrite) id<PatcherViewDelegate> delegate;

@property (nonatomic, readwrite) BOOL isInEditMode;

- (void) createNewSequence: (Sequence*) sequence;
- (void) createNewConditional: (PatcherConditionalBlock*) aConditional;
- (void) clearBlocks;
- (void) deleteConditional:(PatcherConditionalBlock*) aConditional;
- (void) deleteSequence: (PatcherSequenceBlock*) aSequenceBlock;
- (PatcherConditionalBlock*) conditionalForTagNumber:(int) num;
- (PatcherSequenceBlock*) sequenceForTagNumber:(int) num;

- (void) stop;


@end
