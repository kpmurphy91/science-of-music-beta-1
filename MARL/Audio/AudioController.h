/*
 Copyright (c) Kevin P Murphy June 2012
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "ADSRView.h"
#import "SynthDefs.h"
#import "PADsynth.h"
#import "AdditiveWaveTable.h"

@class SequencerSoundFont;

@protocol AudioManagerDelegate
@required
- (void) receivedAudioSamples:(SInt16*) samples length:(int) len;
@end

@protocol AudioEnvelopeDelegate <NSObject>
- (float) nextEnvelopeCoefficient;
@end

@interface AudioController : NSObject
{
    SequencerSoundFont *currentFont;

    @public
    SInt16 **matrixOfSamples;
    AudioBufferList bufferList;
    int chosenTable;
    ADSRStruct sampleADSRStruct;
    NSMutableDictionary *dictionaryOfAliveNotes;
    
    
}
@property (readwrite) BOOL inputRunning;
@property (nonatomic, assign) AudioStreamBasicDescription audioFormat;
@property (nonatomic, assign) AudioUnit rioUnit;
@property (nonatomic, assign) id<AudioManagerDelegate> delegate;
@property (nonatomic, assign) id<AudioEnvelopeDelegate> envelopeDelegate;
@property (nonatomic, readwrite) struct ADSRStruct sampleADSRStruct;
@property (nonatomic, readwrite) SynthKeyState keyState;
@property (nonatomic, readwrite) AdditiveWaveTable *synthWaveTable;
@property (nonatomic, readwrite) NSArray *arrayOfSynthTables;
@property (nonatomic, readonly) SequencerSoundFont *currentFont;
+ (AudioController*) sharedAudioManager;
- (void) startAudio;
- (void) stopAudio;
- (void) playNote:(int) notenum;
- (void) stopNote:(int) notenum;
- (float) nextEnvelopeCoefficient: (void*) context;
- (void) loadSequencerSoundFont:(SequencerSoundFont*) soundFont;

@end




