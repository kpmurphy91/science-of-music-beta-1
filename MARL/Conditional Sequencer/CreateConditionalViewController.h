//
//  CreateConditionalViewController.h
//  MARL
//
//  Created by Kevin Murphy on 9/13/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PatcherConditionalBlock.h"

@protocol CreateConditionalDelegate <NSObject>
- (void) popoverCreatedConditional: (PatcherConditionalBlock*) aConditional;
@end

@interface CreateConditionalViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    UITableView *thisTable;
    UITextField *repeatsFied;
}
@property (nonatomic, readwrite) id<CreateConditionalDelegate> delegate;

@end
