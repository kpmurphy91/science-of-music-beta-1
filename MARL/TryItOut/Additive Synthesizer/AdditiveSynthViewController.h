//
//  AdditiveSynthViewController.h
//  MARL
//
//  Created by Kevin Murphy on 8/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PADsynth.h"
#import "AdditiveWaveTable.h"
#import "SineWaveView.h"
#import "WaveTableCell.h"
#import "ComplexWaveView.h"
#import "HelpViewController.h"
#import "NoteChooserViewController.h"

@interface AdditiveSynthViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, NoteChooserDelegate>
{
    UITableView *thisTable;
    UITableViewCell *addWaveCell, *clearCell;
    UITextField *addFreqField, *addAmpField;
    UISlider *frequencySlider, *amplitudeSlider;
    
    float referenceFrequency;
    
    ComplexWaveView *complexWave;
    
    NSTimer *minimumTimer;
}
@property (nonatomic, readwrite) NSMutableArray *arrayOfWaves;
@property (nonatomic, readwrite) NSMutableArray *arrayOfCells;

@property (nonatomic, readwrite) UIPopoverController *popNotePicker;
@end
