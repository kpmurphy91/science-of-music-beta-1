//
//  AppDelegate.m
//  MARL
//
//  Created by Kevin Murphy on 6/25/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "HomeNaivgationController.h"
#import "TestFlight.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize audioManager;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    //audioManager = [AudioController sharedAudioManager];
    [AudioController sharedAudioManager];
    temp = [[PitchDetector alloc] initWithSampleRate:audioManager.audioFormat.mSampleRate];
    temp.delegate = self;
    [temp startPerforming];
    
    newFFT = [[FFTControl alloc] initWithLength:1024];
    
    HomeViewController *aController =  [[HomeViewController alloc] initWithNibName:nil bundle:nil];
    
    self.navController = [[HomeNaivgationController alloc] initWithRootViewController:aController];
    [self.window setRootViewController:self.navController];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
    [TestFlight takeOff:@"58633792a592af3aa4ad64d24346cbf5_ODczNzEyMDEyLTExLTE0IDEyOjEwOjA0LjI2MTE1NA"];

    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) receivedAudioInputSamples:(TPCircularBuffer *)circleBuffer {
    
    int availableBytes = 0;
    SInt16 *buffer = (SInt16*) TPCircularBufferTail(circleBuffer, &availableBytes);
    if(availableBytes==0) return;
    
    SInt16 *target = (SInt16*) malloc(availableBytes);
    memcpy(target, buffer, availableBytes);
    TPCircularBufferConsume(circleBuffer, availableBytes);
    
    int numSamples = availableBytes/sizeof(SInt16);
    
    //WORK WITH SAMPLES!
    [temp addSamples:target inLength:availableBytes/2];
    
    float *amp = (float*) malloc(sizeof(float)*1024);
    float *phase = (float*) malloc(sizeof(float)*1024);

    [newFFT doFFTReal:target amplitude:amp phase:phase length:1024];
    
    float greatest = 0;
    int greatestIndex = 0;
    for(int i = 0; i<1024/2; i++) {
        if(abs(amp[i])>greatest) {
            greatest =abs( amp[i] );
            greatestIndex = i;
        }
    }
    
   //printf("FFT %f\n", ((float)greatestIndex/512.0)*22050.0);
    
    
    
    
    free(amp);
    free(phase);
    free(target);
}

- (void) foundPitch:(float)frequency {
    //printf("Frequency: %f\n", frequency);
}

@end
