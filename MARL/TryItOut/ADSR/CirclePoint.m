//
//  CirclePoint.m
//  Science of Music
//
//  Created by Kevin Murphy on 6/8/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "CirclePoint.h"

@implementation CirclePoint
@synthesize delegate, scaledPosition, isMoving, sisterYPoint, leftCircle, rightCircle, graphFrame, lowerBoundSibling, upperBoundSibling, onFloor, label;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.scaledPosition = CGPointMake(frame.origin.x/self.superview.frame.size.width, frame.origin.y/self.superview.frame.size.height);
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = NO;
        self.onFloor = NO;
        
        charLabel = [[UILabel alloc] initWithFrame:self.bounds];
        charLabel.backgroundColor = [UIColor clearColor];
        charLabel.textAlignment = NSTextAlignmentCenter;
        charLabel.text = @"";
        charLabel.textColor = [UIColor redColor];
        charLabel.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:20];
        [self addSubview:charLabel];
        
    }
    return self;
}

- (void) setLabel:(NSString *)_label {
    if([_label isEqualToString:@"R"]) {
        charLabel.text = @"R";
        charLabel.transform = CGAffineTransformMakeTranslation(0, -13);
        [charLabel sizeToFit];
    }
    
    charLabel.text = _label;
    label = _label;
}


- (void)drawRect:(CGRect)rect
{
    float padScale = 0.9;
    rect = CGRectApplyAffineTransform(rect, CGAffineTransformMakeScale(padScale, padScale));
    rect = CGRectMake((self.frame.size.width/2)-(rect.size.width/2), (self.frame.size.height/2)-(rect.size.height/2), rect.size.width, rect.size.height);
    
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    // Set the border width
    CGContextSetLineWidth(contextRef, 2.0);
    CGContextSetFillColorWithColor(contextRef, RGBA(255, 255, 255, 0.7).CGColor);//RGBA(70, 170, 200, 0.3).CGColor);
    CGContextSetStrokeColorWithColor(contextRef, RGBA(255, 255, 255, 0.97).CGColor);//RGBA(70, 170, 200, 0.9).CGColor);
    
    // Fill the circle with the fill color
    CGContextFillEllipseInRect(contextRef, rect);
    
    // Draw the circle border
    CGContextStrokeEllipseInRect(contextRef, rect);
}

CGPoint original;
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
        original = [[touches anyObject] locationInView:self.superview];
    
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint inParent = [[touches anyObject] locationInView:self.superview];
    
    if(leftCircle!=nil) {
        if(inParent.x<leftCircle.center.x) {
            inParent.x = leftCircle.center.x+10;
        }
    }
    
    if(rightCircle!=nil) {
        if(inParent.x>rightCircle.center.x) {
            inParent.x = rightCircle.center.x-10;
        }
    }
    
    
    if(inParent.x<0) {
        inParent.x = 0;
    } else if(inParent.x>self.graphFrame.size.width) {
        inParent.x = self.graphFrame.size.width;
    }
    
    
    if(inParent.y<0) {
        printf("to top %f", inParent.y);
        inParent.y=0;
    } else if(inParent.y>self.graphFrame.size.height) {
        inParent.y = self.graphFrame.size.height;
    }
    
    if(self.lowerBoundSibling != nil) {
        if(inParent.y>lowerBoundSibling.center.y) {
            inParent.y = lowerBoundSibling.center.y;
        }
    }
    
    if(self.upperBoundSibling != nil) {
        if(inParent.y<upperBoundSibling.center.y) {
            inParent.y = upperBoundSibling.center.y;
        }
    }
    
    if(self.onFloor) {
        inParent.y = graphFrame.size.height;
    }
    
    self.center = inParent;
    
    if(sisterYPoint!=nil) {
        sisterYPoint.center = CGPointMake(sisterYPoint.center.x, self.center.y);
    }
    
    [delegate pointsChangedPosition];
    
    
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [delegate pointsChangedPosition];
}


- (CGPoint) scaledPosition {
    CGPoint returnPoint;
    returnPoint.x = (self.center.x-self.graphFrame.origin.x)/self.graphFrame.size.width;
    returnPoint.y = ((self.graphFrame.size.height+self.graphFrame.origin.y)-self.center.y)/self.graphFrame.size.height;
    printf("(%f, %f)\n", returnPoint.x, returnPoint.y);
    return returnPoint;
}


@end
