//
//  NoteChooserViewController.m
//  MARL
//
//  Created by Kevin Murphy on 5/14/13.
//  Copyright (c) 2013 New York University. All rights reserved.
//

#import "NoteChooserViewController.h"

@interface NoteChooserViewController ()

@end

@implementation NoteChooserViewController
@synthesize delegate;
- (id) initWithDelegate:(id) __delegate {
    self = [self initWithNibName:@"NoteChooserViewController" bundle:nil];
    if(self) {
        self.delegate = __delegate;
    }
    
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidAppear:(BOOL)animated {
    [self.view sizeToFit];
    self.contentSizeForViewInPopover = self.view.frame.size;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction) userSelectedCancel:(id)sender {
    [delegate notePickerCanceled];
}

- (IBAction) userSelectedDone:(id)sender {
    printf("Chosend first: %d, chosen second: %d\n", [aPickerView selectedRowInComponent:0], [aPickerView selectedRowInComponent:1]);
    NSLog(@"apickerView: %@", aPickerView);
    int octaveNote = [aPickerView selectedRowInComponent:1]-3;
    if(octaveNote<0) octaveNote+=12;
    [delegate notePickerChoseNote:([aPickerView selectedRowInComponent:0]+1)*12+octaveNote];
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}


- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(component == 1) return 12;
    else return 8;
}

- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSArray *arrayOfNoteStrings = [NSArray arrayWithObjects:@"A", @"Bb", @"B", @"C", @"C#", @"D", @"Eb", @"E", @"F", @"F#", @"G", @"G#", nil];
    if(component== 1) {
        return arrayOfNoteStrings[row];
    } else {
        return [NSString stringWithFormat:@"%d", row];
    }
}


@end
