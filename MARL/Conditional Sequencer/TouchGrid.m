//
//  TouchGrid.m
//  MARL
//
//  Created by Kevin Murphy on 6/27/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "TouchGrid.h"

@implementation TouchGrid
@synthesize columns, rows, beatContentArray, rowToFrequencyArray, highLightDecayInterval;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        touchInterceptor = [[SequenceTouchInterceptor alloc] initWithFrame:self.bounds];
        self.rows = 12;
        self.columns = 16;
        [touchInterceptor generateTouchGridWithRows:self.rows andColumns:self.columns];
        touchInterceptor.delegate = self;
        
        self.highLightDecayInterval = 0.8;
        beatContentArray = [[NSMutableArray alloc] initWithCapacity:self.columns];
        
        NSMutableArray *tempArray;
        for(int i = 0; i<self.columns; i++) {
           
            tempArray = [[NSMutableArray alloc] initWithCapacity:self.rows];
            
            for(int j = 0; j<self.rows; j++) {
                
                UIButton *tempButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                tempButton.alpha = 0.5;
                float originx = [touchInterceptor getButtonWidth]*i;
                float originy = [touchInterceptor getButtonHeight]*j;
               
                tempButton.frame = CGRectMake(originx, originy, [touchInterceptor getButtonWidth], [touchInterceptor getButtonHeight]);
                [tempButton setTag:j];
               
                [tempArray addObject:tempButton];
                [self addSubview:tempButton];
            }
            [beatContentArray addObject:tempArray];
        }
        
        rowToFrequencyArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:48], [NSNumber numberWithInt:50], [NSNumber numberWithInt:52], [NSNumber numberWithInt:55], [NSNumber numberWithInt:57], [NSNumber numberWithInt:60], [NSNumber numberWithInt:62], [NSNumber numberWithInt:64], [NSNumber numberWithInt:67], [NSNumber numberWithInt:69], [NSNumber numberWithInt:72], [NSNumber numberWithInt:74], nil];
        
        
        //must be last presented to view in order to recieve all touch events.
        [self addSubview:touchInterceptor];
        // Initialization code
    }
    return self;
}


- (void) startedInterceptingTouchInRow:(int)row andColumn:(int)col {
    
    UIButton *thisButton = [[beatContentArray objectAtIndex:col] objectAtIndex:row];
    [thisButton setSelected:!thisButton.selected];
    [thisButton setHighlighted:thisButton.selected];
    
    
}

- (void) interceptedTouchMovedInRow:(int)row andColumn:(int)col {
    
    UIButton *thisButton = [[beatContentArray objectAtIndex:col] objectAtIndex:row];
    [thisButton setSelected:!thisButton.selected];
    [thisButton setHighlighted:thisButton.selected];
}

- (void) droppedTouch {
}

- (void) clearSelection {
    for(int i = 0; i<beatContentArray.count; i++) {
        NSMutableArray *tempArray = [beatContentArray objectAtIndex:i];
        for(int j = 0; j<tempArray.count; j++) {
            UIButton *tempButton = [tempArray objectAtIndex:j];
            tempButton.selected = NO;
            tempButton.highlighted = NO;
        }
    }
}

- (void) reloadButtonsWithArray:(NSArray *)beatssArray {
    for(int i = 0; i<beatssArray.count; i++) {
        //NSLog(@"beats Array: %d", i);

        NSArray *thisArray = [beatssArray objectAtIndex:i];
        NSMutableArray *toArray = [beatContentArray objectAtIndex:i];
        for(int n = 0; n<thisArray.count; n++) {

            UIButton *thisButton = [toArray objectAtIndex:n];
            if([[thisArray objectAtIndex:n] boolValue]) {
                //NSLog(@"Column: %d, Row: %d", i, n);
                [thisButton setHighlighted:YES];
            }
        }
        
    }
}

- (void) animateColumn:(int)col {
    NSArray *theseButtons = [beatContentArray objectAtIndex:col];
    for(int i = 0; i<theseButtons.count; i++) {
        UIButton *thisButton = [theseButtons objectAtIndex:i];
        if(thisButton.selected) {
            [[theseButtons objectAtIndex:i] setAlpha:1];
            [UIView animateWithDuration:self.highLightDecayInterval/2 animations:^{[[theseButtons objectAtIndex:i] setAlpha:0.5];}];
        };
    }
}

@end