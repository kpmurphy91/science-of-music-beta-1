//
//  PopTableController.m
//  MARL
//
//  Created by Kevin Murphy on 8/28/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "PopTableController.h"

@interface PopTableController ()

@end

@implementation PopTableController
@synthesize arrayOfSoundFonts, delegate, tabView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andArray: (NSArray*) array andDefaultObject:(SequencerSoundFont*) defaul
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.arrayOfSoundFonts = array;
        
        for(int i = 0; i<array.count; i++) {
            SequencerSoundFont *temp = [array objectAtIndex:i];
            if([temp.title isEqualToString:defaul.title]) {
                checkedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            }
        }
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.contentSizeForViewInPopover = CGSizeMake(300, 280);
    
    tabView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStyleGrouped];
    tabView.backgroundColor = [UIColor clearColor];
    tabView.backgroundView = nil;
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    
    [self.view addSubview:tabView];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Choice"];
    cell.textLabel.textAlignment = UITextAlignmentCenter;
    cell.textLabel.text = [[arrayOfSoundFonts objectAtIndex:indexPath.row] title];
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    
    if(indexPath.row == checkedIndexPath.row) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return  cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SequencerSoundFont *aSound = [arrayOfSoundFonts objectAtIndex:indexPath.row];
    [delegate soundFontChooserChoseSoundFont:aSound];
    
    checkedIndexPath = indexPath;
    
    [tableView reloadData];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayOfSoundFonts.count;
}

- (int) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return  0;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  40;
}


@end
