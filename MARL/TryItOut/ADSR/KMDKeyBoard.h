//
//  KMDKeyBoard.h
//  BishaiTextBook
//
//  Created by Kevin Murphy on 6/18/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMDKeyBoardOverlay.h"

@protocol KMDKeyboardDelegate <NSObject>
- (void) keyPressed: (int) chromaticNumber;
- (void) keySustainChangedNote: (int) chromaticNumber;
- (void) keyLifted: (int) chromaticNumber;
@end

@interface KMDKeyBoard : UIView
{
    UIButton *a, *bb, *b, *c, *db, *d, *eb, *e, *f, *gb, *g, *ab, *currentlyPressed;
}
@property (nonatomic, readwrite) id<KMDKeyboardDelegate> delegate;
@end
