//
//  SequencerSoundFont.m
//  MARL
//
//  Created by Kevin Murphy on 9/13/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "SequencerSoundFont.h"

@implementation SequencerSoundFont
@synthesize sf2Name, presetURL, title, noteOnTimeout;

- (id) init {
    self = [super init];
    if(self) {
        self.sf2Name = nil;
        self.presetURL = nil;
        self.title = nil;
        self.noteOnTimeout = 1.5;
    }
    
    return self;
}

- (void) load {
    [[AudioController sharedAudioManager] loadSequencerSoundFont:self];
}

@end
