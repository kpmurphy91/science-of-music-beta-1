//
//  Video.m
//  MARL
//
//  Created by Kevin Patrick Murphy on 7/23/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "Video.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@implementation Video
@synthesize title, deck, description, path, thumbImage, urlToVideo, youtubeID, duration;

+ (Video*) videoObjectWithFileName:(NSString*) inPath title:(NSString*) inTitle andDescription:(NSString*) inDescription {
    Video *thisVideo = [[Video alloc] init];
    
    thisVideo.path = [[NSBundle mainBundle] pathForResource:inPath ofType:nil];
    thisVideo.youtubeID = nil;
    thisVideo.urlToVideo = nil;
    thisVideo.title = inTitle;
    thisVideo.description = inDescription;
    
    [thisVideo finishThumbnail];
    
    return thisVideo;
}


+ (Video*) videoObjectWithYoutubeID:(NSString*) idd title:(NSString*) inTitle andDescription:(NSString*) inDescription {
    Video *thisVideo = [[Video alloc] init];
    
    thisVideo.path = nil;
    thisVideo.urlToVideo = nil;
    thisVideo.youtubeID = idd;
    thisVideo.title = inTitle;
    thisVideo.description = inDescription;
    
    [thisVideo finishThumbnail];
    
    return thisVideo;
}

- (void) finishThumbnail {
    if(self.path!=nil) {
        AVURLAsset *asset=[[AVURLAsset alloc] initWithURL:[NSURL URLWithString:self.path] options:nil];
        AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generator.appliesPreferredTrackTransform=TRUE;
        CMTime thumbTime = CMTimeMakeWithSeconds(0,30);
        
        AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
            if (result != AVAssetImageGeneratorSucceeded) {
                NSLog(@"couldn't generate thumbnail, error:%@", error);
            }
            //[button setImage:[UIImage imageWithCGImage:im] forState:UIControlStateNormal];
            self.thumbImage =[UIImage imageWithCGImage:im];
        };
        
        CGSize maxSize = CGSizeMake(320, 180);
        generator.maximumSize = maxSize;
        [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
    } else if(youtubeID!=nil) {
        //retreive the thumb from a stream
        NSURL *imgRequestURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg", self.youtubeID]];
        NSData *thisData = [NSData dataWithContentsOfURL:imgRequestURL];
        self.thumbImage = [UIImage imageWithData:thisData];
    }
}

- (BOOL) isYoutube {
    if(self.youtubeID != nil) {
        return true;
    } else {
        return false;
    }
}
@end
