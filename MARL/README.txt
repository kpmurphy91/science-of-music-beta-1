I'm going to try to make some documentation for the objects I made for future readers...

ADSRView:

This view is a graph for an ADSR envelope. It has 5 points (beginning of the envelope, Attack, Decay, beginning of release (Sustain?), and the end of the envelope). It imports the Grapher.h class which fills in the area occupied by the ADSR envelope.
When the user makes a new enveloper, the ADSRView calls [delegate newADSRStruct: (struct ADSRStruct) newADSRStruct], which let's the delegate there's new envelope information to be used from now on. 



echo-nest

grace-notes

google

sound cloud