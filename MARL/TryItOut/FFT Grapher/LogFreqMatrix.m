//
//  LogFreqMatrix.m
//  MARL
//
//  Created by Kevin Murphy on 10/6/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import "LogFreqMatrix.h"

@implementation LogFreqMatrix


float * logFreqMatrixColumnCreate(float centerFrequency, float lowerFrequency, float upperFrequency, int FFTLengthOver2, float sampleRate, float scale) {
    int heightOfColumn = FFTLengthOver2;
    float spanOfOneRow = (sampleRate/2)/heightOfColumn;
    
    float d = centerFrequency-lowerFrequency;
    float centerHeight = (2*scale)/d;
    
    float *thisColumn = (float*) malloc(sizeof(float)*heightOfColumn);
    
    for (int i = 0; i<heightOfColumn; i++) {
        float thisBinsFrequency = spanOfOneRow*i;
        float thisValue = 0;
        if(thisBinsFrequency>=lowerFrequency && thisBinsFrequency<centerFrequency) {
            thisValue = (thisBinsFrequency-lowerFrequency)*centerHeight/(centerFrequency-lowerFrequency);
            if(lowerFrequency==0) thisValue = 0;
        } else if(thisBinsFrequency == centerFrequency) {
            thisValue = centerHeight;
        } else if(thisBinsFrequency > centerFrequency && thisBinsFrequency <=upperFrequency) {
            thisValue = (upperFrequency - thisBinsFrequency)*centerHeight/(upperFrequency-centerFrequency);
            if(upperFrequency==0) thisValue = 0;
        }
        
        thisColumn[i] = thisValue;
        //printf("Freq: %f   %f\n", thisBinsFrequency, thisValue);
    }
    return  thisColumn;
}

float freqFromMidi(float midi) {
    return 440.0*pow(2.0, ((double)midi-57)/12.0);
}

float * logspace(int midiLow, int midiHi, int binsPerNote) {
    int difference = midiHi-midiLow;
    int n = (difference)*binsPerNote;
    float res = ((float)difference)/n;
    
    float *returnFloats = (float*) malloc(sizeof(float)*n);
    
    for(int i = 0;i<n;i++) {
        returnFloats[i] = freqFromMidi((res*i)+midiLow);
        //printf("%f  %4.1f\n", (res*i)+midiLow, returnFloats[i]);
    }
    
    //logFreqMatrixColumnCreate(returnFloats[50], returnFloats[49], returnFloats[51], 8192, 44100, 10);
    
    return returnFloats;
}

float** multiplyMatrix(float **A, int A_Width, int A_Height, float **B, int B_Width, int B_Height) {
    
    int p = A_Width, n = A_Height, m = B_Width;
    
    float **c = (float **) malloc(sizeof(float*)*n);
    for(int i = 0; i<n; i++) {
        c[i] = (float*) malloc(sizeof(float)*m);
    }
    
    float **a = A;
    float **b = B;
    
    int i , j , k;
    for( i = 0 ; i < n ; i++){
        for( j = 0 ; j < p ; j++){
            a[i][j] = 1;
            //printf("%3d ", a[i][j]);
        }
        printf("\n");
    }
    
    
    for( i = 0 ; i < p ; i++) {
        for( j = 0 ; j < m ; j++) {
            b[i][j] = j;
            //printf("%3f ", b[i][j]);
        }
        printf("\n");
    }
    
    for( i = 0 ; i < n ; i++) {
        for( j = 0 ; j < m ; j++)
        {
            c[i][j] = 0;
            for( k = 0 ;k < p ; k++) {
                float left = a[i][k];
                float right = b[k][j];
                c[i][j] += left*right;
            }
        }
    }
    return c;
}


void printMatrix(int rows, int cols, float **arr) {
    
    for( int i = 0 ; i < rows ; i++) {
        for( int j = 0 ; j < cols ; j++) {
            printf("%4.1f ", arr[i][j]);
        }
        printf("\n");
    }
}

float* createFreqLogMatrix(int windowSize, int loMidi, int hiMidi, int binsPerMidi) {
    
    
    int fN = (hiMidi-loMidi)*binsPerMidi;
    
    float *resultMatrix = (float*) malloc(sizeof(float)*windowSize*fN);
    
    float *logFrequencies = (float*) malloc(sizeof(float)*fN);
    logFrequencies = logspace(loMidi, hiMidi, binsPerMidi);
    
    for (int i = 0; i<fN; i++) {
        float centerFreq = logFrequencies[i];
        float leftFreq = 0;
        if(i>=1) leftFreq = logFrequencies[i-1];
        float rightFreq = 0;
        if(i<fN-1) rightFreq = logFrequencies[i+1];
        
        float *thisColumn = logFreqMatrixColumnCreate(centerFreq, leftFreq, rightFreq, windowSize, 44100, 1);
        
        for (int j =0; j<windowSize; j++) {
            resultMatrix[j*windowSize+i] = thisColumn[j];
        }
        
    }
    
   // printMatrix(windowSize, fN, resultMatrix);
    
    return resultMatrix;
}

+ (float*) matrixWithHeight: (int) height sampleRate: (float) sampleRate lowMidi: (int) lowerMidi higherMidi: (int) upperMidi binsPerMidi: (int) binsPerMidi {
    return createFreqLogMatrix(height, lowerMidi, upperMidi, binsPerMidi);
}


@end
