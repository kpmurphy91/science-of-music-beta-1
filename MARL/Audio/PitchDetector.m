//  PitchDetector.m
//  MusiciansKit
//  Created by Kevin Murphy on 2/16/12.
//  Copyright (c) 2012 Kevin P Murphy. All rights reserved.
#import "PitchDetector.h"
#import "AudioController.h"
#import "math.h"
#import "AppDelegate.h"
#import <Accelerate/Accelerate.h>

float TMP=0.0;

@implementation PitchDetector
@synthesize delegate;
- (id) initWithSampleRate:(float) sampleRateInit {
    sampleRate = sampleRateInit;
    isRunning = false;
    bufferLength = sampleRate/25;    
    hiBound = 4500;
    
    hann = (float*) malloc(sizeof(float)*bufferLength);
    vDSP_hann_window(hann, bufferLength, vDSP_HANN_NORM);
    
    TPCircularBufferInit(&bufferPitch, bufferLength*4);
    
    TPCircularBufferInit(&samps, sizeof(SInt16)*bufferLength);
    sprinting = false;
    
    result = (double*) malloc(sizeof(double)*bufferLength);
    n = bufferLength; 

    
    
    return self;
}

-(void) perform;
{
    isRunning = true;
    float freq = 0;
    BOOL perf = true;
    
    int32_t availableBytes;
    SInt16 *samples = (SInt16*) TPCircularBufferTail(&bufferPitch, &availableBytes);
    
    if(availableBytes/2<n) {
        perf = false;
    } else if(availableBytes/2>n) {
        samples += availableBytes/2-n;
    }
    
    if(perf) {
        TPCircularBufferProduceBytes(&samps, samples, n*sizeof(SInt16));
        TPCircularBufferConsume(&bufferPitch, availableBytes);
        
        int returnIndex = 0;
        float sum;
        bool goingUp = false;
        float normalize = 0;
        
        BOOL breakMeOff = false;
        
        for(int i = 0; i<n; i++) {
            sum = 0;
            
            for(int j = 0; j<n; j++) {
                sum += (samples[j]*samples[j+i])*hann[j];
            }
            
            if(i ==0 ) normalize = sum;
            result[i] = sum/normalize;
            //printf("%d - %3.1f  \n", i, result[i]);
            
            if(result[i]>result[i-1] && goingUp == false && i >1) {
                
                //local min at i-1
                
                goingUp = true;
                
            } else if(goingUp == true && result[i]<result[i-1]) {
                
                //local max at i-1
                
                if(result[i-1]>result[0]*0.8) {
                    //printf("Local Max at frame: %d   ==  %f\n", i-1, result[i-1]);
                    returnIndex = i-1;
                    breakMeOff = TRUE;
                    break;
                } else if(result[i-1]>result[0]*0.85) {
                    //printf("Local Max at frame: %d   ==  %f\n", i-1, result[i-1]);
                }
                
                
            }
            if(breakMeOff) break;
        }
        
        
        freq = sampleRate/interp(result[returnIndex-1], result[returnIndex], result[returnIndex+1], returnIndex);
        TPCircularBufferConsume(&samps, sizeof(SInt16)*n);
    }
    
    isRunning = false;
    if(freq >= 27.5 && freq <= 4500.0) {
        
        dispatch_async(dispatch_get_main_queue(), ^{[delegate foundPitch:freq];});
        
    }
    
    sprinting = false;
    
    return;
}

-(void) startPerforming {
    
}

-(void) stopPerforming {
    printf("invalidate\n");
}


double interp(double y1, double y2, double y3, int k);
double interp(double y1, double y2, double y3, int k) {
    double d, kp;

    d = (y3 - y1) / (2 * (2 * y2 - y1 - y3));
    kp  =  k + d;
    return kp;
}


- (int) addSamples: (SInt16*) samples inLength:(int) inNumberFrames {
    int copied =  TPCircularBufferProduceBytes(&bufferPitch, samples, inNumberFrames*sizeof(SInt16));
    [self performSelectorInBackground:@selector(perform) withObject:nil];
    return copied;
}

@end

