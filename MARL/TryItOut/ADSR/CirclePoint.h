//
//  CirclePoint.h
//  Science of Music
//
//  Created by Kevin Murphy on 6/8/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Grapher.h"

@class CirclePoint;
@protocol CirclePointDelegate <NSObject>
- (void) pointsChangedPosition;
@end


@interface CirclePoint : UIView
{
    UILabel *charLabel;
}
@property (nonatomic, readwrite) NSString *label;


@property (nonatomic, assign) id<CirclePointDelegate> delegate;
@property (nonatomic) BOOL isMoving;
@property (nonatomic) BOOL onFloor;
@property (nonatomic, assign) CGRect graphFrame;
@property (nonatomic, assign) CGPoint scaledPosition;
@property (nonatomic, assign) CirclePoint *sisterYPoint;
@property (nonatomic, assign) CirclePoint *leftCircle;
@property (nonatomic, assign) CirclePoint *rightCircle;
@property (nonatomic, assign) CirclePoint *lowerBoundSibling;
@property (nonatomic, assign) CirclePoint *upperBoundSibling;




@end


