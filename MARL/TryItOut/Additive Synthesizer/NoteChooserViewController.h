//
//  NoteChooserViewController.h
//  MARL
//
//  Created by Kevin Murphy on 5/14/13.
//  Copyright (c) 2013 New York University. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NoteChooserDelegate <NSObject>

- (void) notePickerChoseNote:(int) note;
- (void) notePickerCanceled;

@end

@interface NoteChooserViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet UIPickerView *aPickerView;
}
@property (nonatomic, readwrite) id<NoteChooserDelegate> delegate;
- (id) initWithDelegate:(id) delegate;

- (IBAction) userSelectedCancel:(id)sender;
- (IBAction) userSelectedDone:(id)sender;

@end
