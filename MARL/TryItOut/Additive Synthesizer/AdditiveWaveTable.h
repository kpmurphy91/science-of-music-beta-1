//
//  AdditiveWaveTable.h
//  MARL
//
//  Created by Kevin Murphy on 9/9/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WaveTableCell.h"
#import "PADsynth.h"

@interface AdditiveWaveTable : NSObject
{
    int index;
}
@property (nonatomic, readwrite) int length;
@property (nonatomic, readwrite) SInt16 *values;
- (SInt16) getNextSample;
- (void) regenerateWithArrayOfWaves:(NSArray*) array;
- (void) regenerateWithArrayOfNumbers:(NSArray*)array;
@end
