//
//  SoundFontArray.h
//  MARL
//
//  Created by Kevin Murphy on 9/17/12.
//  Copyright (c) 2012 New York University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SequencerSoundFont.h"

@interface SoundFontArray : NSArray
+ (NSArray*) globalSoundFontArray;
@end
